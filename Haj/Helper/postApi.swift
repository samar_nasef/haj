//
//  postApi.swift
//  Haj
//
//  Created by elsaid yousif on 12/1/19.
//  Copyright © 2019 ITRoots. All rights reserved.
//

import Foundation
import UIKit

class ApiService
{
    static func getPostString(params:[String:Any]) -> String
    {
        var data = [String]()
        for(key, value) in params
        {
            data.append(key + "=\(value)")
        }
        return data.map { String($0) }.joined(separator: "&")
    }
    static func getPostSecString(params:[String:Any]) -> String
    {
        var data = [String]()
        for(key, value) in params
        {
            data.append(key + ":\(value)")
        }
        return data.map { String($0) }.joined(separator: ",")
    }
    static func callPost(url:URL, params:[String:Any], finish: @escaping ((message:String, data:Data?)) -> Void)
    {
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
            print(jsonData)
            //let theJSONText = String(data: jsonData,
            //       encoding: .utf8)
            // let encryptedPostString = userStatus.shared.encryptData(data: theJSONText!)
            //  let EncryptedObject = ["data": encryptedPostString]
            // let finalEncryptedObject = self.getPostString(params: params)
            
            if let savedPerson = UserDefaults.standard.object(forKey: "HajUserDetails") as? Data {
                              let decoder = JSONDecoder()
                              if let loadedPerson = try? decoder.decode(userDetails2.self, from: savedPerson) {
                  
                                 let token =  loadedPerson.Token!
//                }
//            }
//
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            
            request.httpBody = jsonData
            var result:(message:String, data:Data?) = (message: "Fail", data: nil)
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                                if let returnedData = String(data: data!, encoding: .utf8) {
                                   print(returnedData)
                                }
                
                if let response = response as? HTTPURLResponse {
                           print("post api statusCode: \(response.statusCode)")
                    if response.statusCode == 401{
                        self.loginAgainToUpdateToken()
                        print("bbb1")
//                        return
                        
                    }
                }
                
               
                if(error != nil)
                {
                    result.message = "Fail Error not null : \(error.debugDescription)"
                }
                else
                {
                    result.message = "Success"
                    result.data = data
                }
                
                finish(result)
            }
            task.resume()
                                

                                    }
                                }
            
        }
        catch {
            print(error)
        }
        
        
        //let postString = self.getPostSecString(params: params)
        
    }

    static func callPost3(url:URL, params:[String:Any], finish: @escaping ((message:String, data:Data?)) -> Void)
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
                print(jsonData)
                //let theJSONText = String(data: jsonData,
                //       encoding: .utf8)
                // let encryptedPostString = userStatus.shared.encryptData(data: theJSONText!)
                //  let EncryptedObject = ["data": encryptedPostString]
                // let finalEncryptedObject = self.getPostString(params: params)
                
//                if let savedPerson = UserDefaults.standard.object(forKey: "HajUserDetails") as? Data {
//                                  let decoder = JSONDecoder()
//                                  if let loadedPerson = try? decoder.decode(userDetails2.self, from: savedPerson) {
//
//                                     let token =  loadedPerson.Token!
    //                }
    //            }
    //
                request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
//                request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
                
                request.httpBody = jsonData
                var result:(message:String, data:Data?) = (message: "Fail", data: nil)
                let task = URLSession.shared.dataTask(with: request) { data, response, error in
                                    if let returnedData = String(data: data!, encoding: .utf8) {
                                       print(returnedData)
                                    }
                    
//                    if let response = response as? HTTPURLResponse {
//                               print("post api statusCode: \(response.statusCode)")
//                        if response.statusCode == 401{
//                            self.loginAgainToUpdateToken()
//                            print("bbb1")
//    //                        return
//
//                        }
//                    }
                    
                   
                    if(error != nil)
                    {
                        result.message = "Fail Error not null : \(error.debugDescription)"
                    }
                    else
                    {
                        result.message = "Success"
                        result.data = data
                    }
                    
                    finish(result)
                }
                task.resume()
                                    

//                                        }
//                                    }
//
            }
            catch {
                print(error)
            }
            
            
            //let postString = self.getPostSecString(params: params)
            
        }
    
    static func callPost2(url:URL, params: [Any] ,finish: @escaping ((message:String, data:Data?)) -> Void)
       {
           var request = URLRequest(url: url)
           request.httpMethod = "POST"
           do {
               let jsonData = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
               print(jsonData)
               //let theJSONText = String(data: jsonData,
               //       encoding: .utf8)
               // let encryptedPostString = userStatus.shared.encryptData(data: theJSONText!)
               //  let EncryptedObject = ["data": encryptedPostString]
               // let finalEncryptedObject = self.getPostString(params: params)
            
            if let savedPerson = UserDefaults.standard.object(forKey: "HajUserDetails") as? Data {
                                         let decoder = JSONDecoder()
                                         if let loadedPerson = try? decoder.decode(userDetails2.self, from: savedPerson) {
                             
                                            let token =  loadedPerson.Token!
                                            
               request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
                                            
                                             request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
                                            
               request.httpBody = jsonData
               var result:(message:String, data:Data?) = (message: "Fail", data: nil)
               let task = URLSession.shared.dataTask(with: request) { data, response, error in
                                   if let returnedData = String(data: data!, encoding: .utf8) {
                                      print(returnedData)
                                   }
                
                
                 if let response = response as? HTTPURLResponse {
                                           print("post api statusCode: \(response.statusCode)")
                                    if response.statusCode == 401{
                                        self.loginAgainToUpdateToken()
                                        print("back 22")
                //                        return
                                        
                                    }
                                }
                
                
                   if(error != nil)
                   {
                       result.message = "Fail Error not null : \(error.debugDescription)"
                   }
                   else
                   {
                       result.message = "Success"
                       result.data = data
                   }

                   finish(result)
               }
               task.resume()
                }}
            
           }
           catch {
               print(error)
           }
           //let postString = self.getPostSecString(params: params)

       }

    

     static func loginAgainToUpdateToken()  {
            
            print("loginAgainToUpdateToken")
             if let savedPerson = UserDefaults.standard.object(forKey: "HajUserDetails") as? Data {
                        let decoder = JSONDecoder()
                        if let loadedPerson = try? decoder.decode(userDetails2.self, from: savedPerson) {
            
                            
                           let userName =  loadedPerson.Username!
                           let userPass =  loadedPerson.password!
                            
  
            
                        if Reachability.isConnectedToNetwork() {
//                            sv = UIViewController.displaySpinner(onView: self.view)
         
                            
                            let jsonUrlString = loginEndpoint+userName.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!+"&password="+userPass
                            
                            print("login json url : \(jsonUrlString) ")
            
                            guard let url = URL(string: jsonUrlString) else { return }
                            print("url : \(url)")
                            URLSession.shared.dataTask(with: url) { (data, response, err) in
//                                UIViewController.removeSpinner(spinner: self.sv!)
                                if (err != nil){
//                                    self.view.presentToast(msg: serverError )
                                    
                                    return
                                }
                                if let status = response as? HTTPURLResponse {
                                    print("request status \(status.statusCode)")
                                    if status.statusCode != 200{
//                                        self.view.presentToast(msg: serverError)
                                        return
                                    }
                                }
                                guard let data = data else { return }
                                do {
                                    print(data)
                                    let response = try JSONDecoder().decode(loginApiRes.self, from: data)
                                    print("login response.Status = \(response.Status)")
           
                                    if response.Status == 1 {
                                        
                                        if response.UserDetails.IsVerified == true {
                                        DispatchQueue.main.async {
   
                                            UserDefaults.standard.set(response.UserDetails.IsVerified, forKey: "HajIsVerifiedStatus")
                                            UserDefaults.standard.set(response.UserDetails.SID, forKey: "HajUserID")
                                            
                                            let x = userDetails2(SID: response.UserDetails.SID, FullName: response.UserDetails.FullName, Username: response.UserDetails.Username, Phone: response.UserDetails.Phone, Email: response.UserDetails.Email, LastSeenDate: response.UserDetails.LastSeenDate, ProfileImage: response.UserDetails.ProfileImage, NotificationEnabled: response.UserDetails.NotificationEnabled, VerificationCode: response.UserDetails.VerificationCode, IsVerified: response.UserDetails.IsVerified, UserType: response.UserDetails.UserType, CreatedBy: response.UserDetails.CreatedBy, Status: response.UserDetails.Status, LastUpdatedDate: response.UserDetails.LastUpdatedDate, LastUpdatedBy: response.UserDetails.LastUpdatedBy,PositionName: response.UserDetails.PositionName,OrgOrDeptName: response.UserDetails.OrgOrDeptName,Token:response.UserDetails.Token,password:userPass)
                                            let encoder = JSONEncoder()
                                            if let encoded = try? encoder.encode(x) {
                                                let defaults = UserDefaults.standard
                                                defaults.set(encoded, forKey: "HajUserDetails")
                                            }
                                            print("new token ")
                                               NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getnewtoken"), object: nil)
            
                                        
                                      
                                            }
                                        }
                                     
                                    }
                                    else{
    //                                    self.view.presentToast(msg: response.Message)
                                    }
                                }
                                    
                                catch let jsonErr {
                                    print("Error serializing json:", jsonErr)
    //                                self.view.presentToast(msg: serverError)
                                }
                                }.resume()
                        }
                        else{
    //                        self.view.presentToast(msg: netError)
                        }
                            
                            
                            
                                            }
                                        }
            
            
        }
    
    
    
    
}
