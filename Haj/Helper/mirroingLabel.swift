//
//  mirroingLabel.swift
//  Haj
//
//  Created by elsaid yousif on 1/2/20.
//  Copyright © 2020 ITRoots. All rights reserved.
//

import Foundation


import UIKit

class MirroringLabel: UILabel {
    override func layoutSubviews() {
        if self.tag < 0 {
            if UIApplication.isRTL()  {
                if self.textAlignment == .right {
                    return
                }
            } else {
                if self.textAlignment == .left {
                    return
                }
            }
        }
        if self.tag <= 0 {
            if UIApplication.isRTL()  {
//                self.textAlignment = .right
                 self.textAlignment = .left
                
            } else {
//                self.textAlignment = .left
                  self.textAlignment = .right
            }
        }
    }

}
