//
//  serviceConstants.swift
//  Haj
//
//  Created by elsaid yousif on 12/1/19.
//  Copyright © 2019 ITRoots. All rights reserved.
//

import Foundation




let serverError = "حدث خطأ ما! ، يرجى المحاولة مرة أخرى."
let netError = "خطأ في الإتصال، يرجى التأكد من الانترنت وإعادة المحاولة"
let APIEndpointURL = "http://datacollectorapi.itrootsdemos2.com/"
let loginEndpoint = APIEndpointURL + "api/User/Login?username="
let SignUpEndpoint = APIEndpointURL + "api/User/Register"
let activationCodeEndpoint = APIEndpointURL + "api/User/Verify?verificationCode="
let listUserVisitsEndpoint = APIEndpointURL + "api/Visit/ListUserVisits?user="
let newhomeEndpoint = APIEndpointURL + "api/Visit/ListUserVisits"
let listOrgsEndpoint = APIEndpointURL + "api/Organization/List?user="
let createVisitEndpoint = APIEndpointURL + "api/Visit/Create"
let getOrgDetailsByIdEndpoint = APIEndpointURL + "api/Organization/GetByID?user="
let updateOrgDatatEndpoint = APIEndpointURL + "api/Organization/Update"
let listTemplateAttrEndpoint = APIEndpointURL + "api/Visit/ListTemplateAttributes?template="
let orgContactPersonListEndpoint = APIEndpointURL + "api/OrgContactPerson/List?user="
let visitNotesEndpoint = APIEndpointURL + "api/Visit/ListVisitComments?user="
let aboutAppEndpoint = APIEndpointURL + "api/General/GetAbout?user="
let visitaddReceivedDataEndpoint = APIEndpointURL + "api/Visit/AddReceivedData"
let getVisitDetailsEndpoint = APIEndpointURL + "api/Visit/GetVisitDetails?visit="
let updateReceivedDataEndpoint = APIEndpointURL + "api/Visit/UpdateReceivedData"
let saveVisitAtrrEndpoint = APIEndpointURL + "api/Visit/SaveVisitAttributes"
let updateUserProfileImgEndpoint = APIEndpointURL + "api/User/ImageUpdate"
let changePasswordEndpoint = APIEndpointURL + "api/User/ChangePassword"
let forgetPasswordEndpoint = APIEndpointURL + "api/User/ForgetPassword?userMail="
let changePassForForgetEndpoint = APIEndpointURL + "api/User/ChangePasswordForForget"
let updateProfileEndpoint = APIEndpointURL + "api/User/ProfileUpdate"
let insertOrgContactEndpoint = APIEndpointURL + "api/OrgContactPerson/Insert"
let editOrgContactEndpoint = APIEndpointURL + "api/OrgContactPerson/Update"
let removeOrgContactEndpoint = APIEndpointURL + "api/OrgContactPerson/Delete?user="
let resendactivationCodeEndpoint = APIEndpointURL + "api/User/ResendCode?user="
let listLandMarksEndpoint = APIEndpointURL+"api/Landmark/ListLandmarksByUser?user="
let removeLandMarkEndpoint = APIEndpointURL + "api/Landmark/LandmarkDelete?user="
let listLayersEndpoint = APIEndpointURL + "api/Landmark/ListLayers?user="
let insertLandMarkEndpoint = APIEndpointURL + "api/Landmark/LandmarkInsert"
let updateLandMarkEndpoint = APIEndpointURL + "api/Landmark/LandmarkUpdate"
let notificationEndpoint = APIEndpointURL + "api/User/ListNotifications?user="
let orgbranchListEndpoint = APIEndpointURL + "api/Organization/GetBranches?user="
let scheduleEndpoint = APIEndpointURL + "api/Visit/GetScheduledVisits?visitsDate="
