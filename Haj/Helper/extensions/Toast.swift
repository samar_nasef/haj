//
//  File.swift
//  Haj
//
//  Created by elsaid yousif on 12/1/19.
//  Copyright © 2019 ITRoots. All rights reserved.
//

import Foundation
import UIKit
import Toast_Swift

extension UIView {
    func presentToast(msg: String?) {
        
//        top
        
        
        var style = ToastStyle()
        style.messageFont = UIFont(name: "Cairo SemiBold", size: 14)!
        DispatchQueue.main.async {
            self.makeToast(msg, duration: 3.0, position: .center, style: style)
        }
    }
    func presentToast2(msg: String?) {
           var style = ToastStyle()
           style.messageFont = UIFont(name: "Cairo SemiBold", size: 14)!
           DispatchQueue.main.async {
               self.makeToast(msg, duration: 3.0, position: .center, style: style)
           }
       }
    func presentCenterToast(msg: String?) {
        var style = ToastStyle()
        style.messageFont = UIFont(name: "Cairo SemiBold", size: 14)!
        DispatchQueue.main.async {
            self.makeToast(msg, duration: 3.0, position: .top, style: style)
        }
    }
}

