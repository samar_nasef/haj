//
//  helper.swift
//  Haj
//
//  Created by elsaid yousif on 12/1/19.
//  Copyright © 2019 ITRoots. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration

public class Reachability {
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        /* Only Working for WIFI
         let isReachable = flags == .reachable
         let needsConnection = flags == .connectionRequired
         
         return isReachable && !needsConnection
         */
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
}


let APPLE_LANGUAGE_KEY = "AppleLanguages"
/// L102Language
class L102Language {
    /// get current Apple language
    class func currentAppleLanguage() -> String{
        let userdef = UserDefaults.standard
        let langArray = userdef.object(forKey: APPLE_LANGUAGE_KEY) as! NSArray
        let current = langArray.firstObject as! String
        let endIndex = current.startIndex
        let currentWithoutLocale = current.substring(to: current.index(endIndex, offsetBy: 2))
        return currentWithoutLocale
    }
    
    class func currentAppleLanguageFull() -> String{
        let userdef = UserDefaults.standard
        let langArray = userdef.object(forKey: APPLE_LANGUAGE_KEY) as! NSArray
        let current = langArray.firstObject as! String
        return current
    }
    
    /// set @lang to be the first in Applelanguages list
    class func setAppleLAnguageTo(lang: String) {
        let userdef = UserDefaults.standard
        userdef.set([lang,currentAppleLanguage()], forKey: APPLE_LANGUAGE_KEY)
        userdef.synchronize()
    }
    
    class var isRTL: Bool {
        return L102Language.currentAppleLanguage() == "ar"
    }
}

extension Bundle {
    @objc func specialLocalizedStringForKey(_ key: String, value: String?, table tableName: String?) -> String {
        if self == Bundle.main {
            let currentLanguage = L102Language.currentAppleLanguage()
            
//            if currentLanguage == "en" {
//                currentLanguage = "ar-SA"
//            }
            var bundle = Bundle();
            if let _path = Bundle.main.path(forResource: L102Language.currentAppleLanguageFull(), ofType: "lproj") {
                bundle = Bundle(path: _path)!
            }else
                if let _path = Bundle.main.path(forResource: currentLanguage, ofType: "lproj") {
                    bundle = Bundle(path: _path)!
                } else {
                    let _path = Bundle.main.path(forResource: "Base", ofType: "lproj")!
                    bundle = Bundle(path: _path)!
            }
            return (bundle.specialLocalizedStringForKey(key, value: value, table: tableName))
        } else {
            return (self.specialLocalizedStringForKey(key, value: value, table: tableName))
        }
    }
}


import Foundation
extension UIApplication {
    class func isRTL() -> Bool{
//        return UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft
          return UIApplication.shared.userInterfaceLayoutDirection == .leftToRight
    }
}

class L102Localizer: NSObject {
    class func DoTheMagic() {
        
        MethodSwizzleGivenClassName(cls: Bundle.self, originalSelector: #selector(Bundle.localizedString(forKey:value:table:)), overrideSelector: #selector(Bundle.specialLocalizedStringForKey(_:value:table:)))
        MethodSwizzleGivenClassName(cls: UIApplication.self, originalSelector: #selector(getter: UIApplication.userInterfaceLayoutDirection), overrideSelector: #selector(getter: UIApplication.cstm_userInterfaceLayoutDirection))
        MethodSwizzleGivenClassName(cls: UITextView.self, originalSelector: #selector(UITextView.layoutSubviews), overrideSelector: #selector(UITextView.cstmlayoutSubviews))
        MethodSwizzleGivenClassName(cls: UITextField.self, originalSelector: #selector(UITextField.layoutSubviews), overrideSelector: #selector(UITextField.cstmlayoutSubviews))
        MethodSwizzleGivenClassName(cls: UILabel.self, originalSelector: #selector(UILabel.layoutSubviews), overrideSelector: #selector(UILabel.cstmlayoutSubviews))
        
        
    }
}

func disableMethodSwizzling() {
    
}


/// Exchange the implementation of two methods of the same Class
func MethodSwizzleGivenClassName(cls: AnyClass, originalSelector: Selector, overrideSelector: Selector) {
    let origMethod: Method = class_getInstanceMethod(cls, originalSelector)!;
    let overrideMethod: Method = class_getInstanceMethod(cls, overrideSelector)!;
    if (class_addMethod(cls, originalSelector, method_getImplementation(overrideMethod), method_getTypeEncoding(overrideMethod))) {
        class_replaceMethod(cls, overrideSelector, method_getImplementation(origMethod), method_getTypeEncoding(origMethod));
    } else {
        method_exchangeImplementations(origMethod, overrideMethod);
    }
}
extension UITextField {
    @objc public func cstmlayoutSubviews() {
        self.cstmlayoutSubviews()
        if self.tag <= 0 {
//            if UIApplication.isRTL()  {
//                           if self.textAlignment == .right { return }
//                           self.textAlignment = .right
//            
//
//                       } else {
//                           if self.textAlignment == .left { return }
//                           self.textAlignment = .left
//                       }
        }
    }
}

extension UITextView {
    @objc public func cstmlayoutSubviews() {
        self.cstmlayoutSubviews()
        if self.tag <= 0 {
//            if UIApplication.isRTL()  {
//                if self.textAlignment == .right { return }
//                self.textAlignment = .right
//            } else {
//                if self.textAlignment == .left { return }
//                self.textAlignment = .left
//            }
        }
    }
}

//extension UITextView {
//    @objc public func cstmlayoutSubviews() {
//        self.cstmlayoutSubviews()
//        if self.tag <= 0 {
//            if UIApplication.isRTL()  {
////                if self.textAlignment == .right { return }
////                self.textAlignment = .right
//                 if self.textAlignment == .left { return }
//                  self.textAlignment = .left
//            } else {
////                if self.textAlignment == .left { return }
////                self.textAlignment = .left
//                 if self.textAlignment == .right { return }
//                    self.textAlignment = .right
//            }
//        }
//    }
//}
var numberoftimes = 0
extension UIApplication {
    @objc var cstm_userInterfaceLayoutDirection : UIUserInterfaceLayoutDirection {
        get {
            var direction = UIUserInterfaceLayoutDirection.leftToRight
            if L102Language.currentAppleLanguage() == "ar" {
//                direction = .rightToLeft
                 direction = .leftToRight
            }
            return direction
        }
    }
}
extension UILabel {
    @objc public func cstmlayoutSubviews() {
        self.cstmlayoutSubviews()
        if self.isKind(of: NSClassFromString("UITextFieldLabel")!) {
            return // handle special case with uitextfields
        }
        if self.tag <= 0  {
            if UIApplication.isRTL()  {
                if self.textAlignment == .right {
                    return
                }
            } else {
                if self.textAlignment == .left {
                    return
                }
            }
        }
        if self.tag <= 0 {
//            print("UIApplication.isRTL() : \(UIApplication.isRTL())")
            if UIApplication.isRTL() {
//                print("true left")
//                self.textAlignment = .right
                 self.textAlignment = .left
            } else {
//                    print("false left")
//                self.textAlignment = .left
                self.textAlignment = .right
                
            }
        }
    }
}




class Language {

  static var isArabicLanguage : Bool {
    get {
           return L102Language.currentAppleLanguage() == "ar"
     }
   }
}


extension UIViewController {

//    func loopThroughSubViewAndFlipTheImageIfItsAUIImageView(subviews: [UIView]) {
//           if subviews.count > 0 {
//               for subView in subviews {
//                   if (subView is UIImageView) && subView.tag < 0 {
//                       let toRightArrow = subView as! UIImageView
//                       if let _img = toRightArrow.image {
//                           toRightArrow.image = UIImage(cgImage: _img.cgImage!, scale:_img.scale , orientation: UIImage.Orientation.upMirrored)
//                       }
//                   }
//                   loopThroughSubViewAndFlipTheImageIfItsAUIImageView(subviews: subView.subviews)
//               }
//           }
//       }
    
    
//Align Textfield Text

func loopThroughSubViewAndAlignTextfieldText(subviews: [UIView]) {
if subviews.count > 0 {
    for subView in subviews {
        if subView is UITextField && subView.tag <= 0{
            let textField = subView as! UITextField
            textField.textAlignment = Language.isArabicLanguage ? .right: .left
        } else if subView is UITextView && subView.tag <= 0{
            let textView = subView as! UITextView
            textView.textAlignment = Language.isArabicLanguage ? .right: .left

        }

        loopThroughSubViewAndAlignTextfieldText(subviews: subView.subviews)
    }
  }
}


//Align Label Text
func loopThroughSubViewAndAlignLabelText(subviews: [UIView]) {
if subviews.count > 0 {
    for subView in subviews {
        if subView is UILabel && subView.tag <= 0 {
            let label = subView as! UILabel
            label.textAlignment = Language.isArabicLanguage ? .right : .left
        }
        loopThroughSubViewAndAlignLabelText(subviews: subView.subviews)
    }
   }
  }
    
    func loopThroughSubViewAndAlignLabelTextView(subviews: [UIView]) {
    if subviews.count > 0 {
        for subView in subviews {
            if subView is UITextField && subView.tag >= 0{
                let textField = subView as! UITextField
                textField.textAlignment = Language.isArabicLanguage ? .right: .left
            } else if subView is UITextView && subView.tag <= 0{
                let textView = subView as! UITextView
                textView.textAlignment = Language.isArabicLanguage ? .right: .left

            }else  if subView is UILabel && subView.tag <= 0 {
                       let label = subView as! UILabel
                       label.textAlignment = Language.isArabicLanguage ? .right : .left
                   }

            loopThroughSubViewAndAlignLabelTextView(subviews: subView.subviews)
        }
      }
    }
    
}


class LocalizerViewController: UIViewController {

   override func viewDidLoad() {
      super.viewDidLoad()

      self.loopThroughSubViewAndAlignTextfieldText(subviews: self.view.subviews)
      self.loopThroughSubViewAndAlignLabelText(subviews: self.view.subviews)
//    self.loopThroughSubViewAndFlipTheImageIfItsAUIImageView(subviews: self.view.subviews)
   
  }
}




import UIKit

class LoadingCell : UITableViewCell {
    var activityIndicator: UIActivityIndicatorView!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    private func setupSubviews() {
        let indicator = UIActivityIndicatorView()
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.style = .gray
        indicator.hidesWhenStopped = true
        
        contentView.addSubview(indicator)
        
        NSLayoutConstraint.activate([
            indicator.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            indicator.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10),
            indicator.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
        ])
        
        indicator.startAnimating()
    }
}

