//
//  settings.swift
//  Haj
//
//  Created by elsaid yousif on 12/11/19.
//  Copyright © 2019 ITRoots. All rights reserved.
//

import Foundation
struct aboutapi:Decodable {
    let Status:Int
    let AboutDetails:aboutobj
    let Message:String
    
}

struct aboutobj:Decodable {
//    "SID": 1,
    let AboutText:String
//    "CreationDate": "2019-12-01T00:00:00",
//    "Status": 1,
//    "LastUpdatedDate": null,
//    "LastUpdatedBy": null
}

struct changePassRes:Decodable {
    let Status:Int
    let Message:String
}

struct changePassForForgetRes:Decodable {
    let Status:Int
      let Message:String
}
