//
//  File.swift
//  Haj
//
//  Created by elsaid yousif on 12/18/19.
//  Copyright © 2019 ITRoots. All rights reserved.
//

import Foundation
struct updateprofileImg:Decodable {
    let Status:Int
    let ImageLink:String
    let Message:String
    
}

struct editPerson:Decodable {
    var SID:Int
//    "OrgSID": 1,
    var Name:String
    var Phone:String
    var gender:Int
    var job:String
    var org:String
//    "CreationDate": "2019-11-28T21:12:43.867",
//    "CreatedBy": 4
}
struct updateRes:Decodable {
    let Status:Int
    let Message:String
}

