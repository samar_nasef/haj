//
//  File.swift
//  Haj
//
//  Created by elsaid yousif on 12/10/19.
//  Copyright © 2019 ITRoots. All rights reserved.
//

import Foundation

struct listOrgContacts:Decodable {
    let Status:Int
    let OrgContactPerson:[contactPerson]
    let Message:String
}

struct contactPerson:Decodable {
    var SID:Int
//    "OrgSID": 1,
    var PersonName:String
    var PhoneNumber:String
    var PositionName:String
//    "CreationDate": "2019-11-28T21:12:43.867",
//    "CreatedBy": 4
}

struct insertRes:Decodable {
    let Status:Int
    let Message:String
}
