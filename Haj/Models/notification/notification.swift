//
//  File.swift
//  Haj
//
//  Created by elsaid yousif on 1/15/20.
//  Copyright © 2020 ITRoots. All rights reserved.
//

import Foundation

struct notificationobj:Decodable {
    let SID:Int
   
//    "SystemUserSID": 35,
//    "NotificationType": 0,
//    "Title": "test",
    let Message:String
    let CreationDate:String
//    "Status": 1
}
struct notificationistRes:Decodable {
    let Status:Int
    let NotificationsList:[notificationobj]?
    let Message:String
}
