//
//  activation.swift
//  Haj
//
//  Created by elsaid yousif on 12/2/19.
//  Copyright © 2019 ITRoots. All rights reserved.
//

import Foundation

struct activationApiRes: Decodable{

    let Status :Int
    let Message:String
   
}

struct forgetPassRes:Decodable {
    let Status:Int
    let UserID:Int?
    let VerificationCode:Int?
    let Message:String
}

struct resendActivationRes:Decodable {
    let Status:Int
    let Verificationcode:Int?
    let Message:String
    
}
