//
//  loginres.swift
//  Haj
//
//  Created by elsaid yousif on 12/1/19.
//  Copyright © 2019 ITRoots. All rights reserved.
//

import Foundation


struct userDetails2:Codable {
    
    let SID :Int
    var FullName:String?
    var Username:String?
    var Phone:String?
    var Email:String?
    let LastSeenDate:String?
    var ProfileImage:String?
    let NotificationEnabled:Bool
    let VerificationCode:Int?
    let IsVerified : Bool?
    var UserType:Int?
    //    let CreationDate:Data?
    let CreatedBy:Int
    let Status:Int
    let LastUpdatedDate:String?
    let LastUpdatedBy:Int?
    var PositionName:String?
    var OrgOrDeptName:String?
    var Token:String?
    var password:String?
    
    
}


struct userDetails:Decodable {

    let SID :Int
    let FullName:String?
    let Username:String?
    let Phone:String?
    let Email:String?
    let LastSeenDate:String?
    let ProfileImage:String?
    let NotificationEnabled:Bool
    let VerificationCode:Int?
    let IsVerified : Bool?
    let UserType:Int?
//    let CreationDate:Data?
    let CreatedBy:Int
    let Status:Int
    let LastUpdatedDate:String?
    let LastUpdatedBy:Int?
    let PositionName:String?
       let OrgOrDeptName:String?
    var Token:String?
       var password:String?
}
struct loginApiRes: Decodable{
    let Status: Int
    var UserDetails: userDetails2
    let Message:String
}

