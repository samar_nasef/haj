//
//  File.swift
//  Haj
//
//  Created by elsaid yousif on 12/30/19.
//  Copyright © 2019 ITRoots. All rights reserved.
//

import Foundation

struct listuserLandMarks:Decodable {
    let Status:Int
    let LandmarksList:[landMark]
    let Message:String
}

struct landMark:Decodable {
    let SID:Int
    let LayerSID:Int
    let LayerName:String
    let Latitude:Double?
    let Longtitude:Double?
    let PointTitle:String?
    let Notes:String?
    let SubFormNo:String?
    let WCNo:Int?
    let PointStatus:Int?
    let SubType:String?
    let StarsNo:Int?
    let PointNo:Int?
    let NearestLandmark:String?
    let WorkDuringNormalDays:Bool
    let WorkDuringRamadan:Bool
    let WorkDuringFriday:Bool
    let WorkDuringPilgrimage:Bool
    let CreationDate:String
//               "CreatedBy": 4,
    let UploadedImage:String
}
struct  removeLandMarkRes:Decodable {
    let Status:Int
    let Message:String
}

struct listLayersRes:Decodable {
    let Status:Int
    let LayersList:[layerObj]
    let Message:String
}
struct layerObj:Decodable {
    let SID:Int
    let LayerName:String
    let LayerURL:String?
    let SubFormNo:Bool
               let WCNo:Bool
               let PointStatus:Bool
               let SubType:Bool
               let StarsNo:Bool
    
               let PointNo:Bool
               let NearestLandmark:Bool
               let WorkDuringNormalDays:Bool
               let WorkDuringRamadan:Bool
               let WorkDuringFriday:Bool
               let WorkDuringPilgrimage:Bool
}

struct insertLandMarkRes:Decodable {
    let Status:Int
    let LandmarkID:Int?
    let Message:String
}
