//
//  schedulevisit.swift
//  Haj
//
//  Created by elsaid yousif on 2/17/20.
//  Copyright © 2020 ITRoots. All rights reserved.
//

import Foundation


struct scheduleVisitObj: Decodable {
    let SID:Int
//               "MobileUserSID": 4,
//               "OrgSID": 10,
//               "OrgBranchSID": 10,
    let VisitDate:String
//    "2020-01-29T00:00:00",
    let Notes:String
//               "IsDone": false,
//               "CreationDate": "2020-01-29T00:00:00",
//               "CreatedBy": 4,
//               "Status": 1,
    let OrgTitle:String
    let AdminName:String
    
//               "BranchTitle": "الإدارة العامة"
}

struct scheduleDataRes:Decodable {
    let Status:Int
    let VisitsList:[scheduleVisitObj]
    let Message:String
}
