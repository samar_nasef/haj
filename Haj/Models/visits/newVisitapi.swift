//
//  newVisitapi.swift
//  Haj
//
//  Created by elsaid yousif on 12/5/19.
//  Copyright © 2019 ITRoots. All rights reserved.
//

import Foundation

struct listorgs: Decodable{
    let Status:Int
    let Organizations:[organization]
    let Message:String
}

struct organization: Decodable{
    let SID:Int
    let Title:String
    let FormTemplateSID:Int
    
}
struct createVisitApiRes: Decodable{
    let VisitID:Int?
    let Status:Int
    let Message:String
}

struct getOrgDeatils:Decodable {
    let Status:Int
    let Details:orgDetails
    let Message:String
}

struct orgDetails:Decodable {
    let SID:Int
    let Title:String
    let ManagerName:String
    let ContactOfficer:String
    let ContactOfficerPhone:String?
    let OrgAddress:String
    let Lat:Double
    let Long:Double
    
}
struct updateOrgRes:Decodable {
    let Status:Int
    let Message:String
}

struct listtemplatearrtRes:Decodable {
    let Status:Int
    let AttributesList:[attrList?]
    let Message:String
}
struct attrList:Decodable {
    let SID:Int
//    "FormTemplateSID": 1,
    let Title:String?
    let ControlDataTypeSID:Int
    let TextboxDatatype:Int
    let Length:Int
    let Optional:Bool
//    "CreationDate": "2019-12-02T09:05:16.05",
//    "CreatedBy": 5,
//    "Status": 1,
//    "LastUpdatedDate": "2019-12-02T09:18:56.023"?,
//    "LastUpdatedBy": 5
    
}

struct visitAddReceievedDataRes:Decodable {
    
    let Status:Int
    let ReceivedDataID:Int?
    let Message:String
}

struct getvisitDetailsRes:Decodable {
    let Status:Int
    let VisitDetails:viditdetails
    let OrgDetails:orgdetail
    let ReceivedDataDetails:RecevedDetils?
    let DynamicAttributes:[dynamicDetails]?
    let Message:String
    
           
}
struct  viditdetails:Decodable {
    let SID:Int
    let FormTempateSID:Int
//       "SystemUserSID": 19,
    let OrgSID:Int
    let Title:String
    let VisitDate:String
    let VisitNumber:Double
    let Lat:Double
    let Long:Double
//       "IsCompleted": true,
    let CreationDate:String
//       "CreatedBy": 19,
//       "Status": 1,
    let LastUpdatedDate:String?
//       "LastUpdatedBy": 5
}
struct orgdetail:Decodable {
    let SID:Int
    let FormTemplateSID:Int
    let Title:String
    let ManagerName:String
    let ContactOfficer:String
    let ContactOfficerPhone:String?
    let Description:String
    let Lat:Double
    let Long:Double
    let OrgAddress:String
//        "OrgOrder": 1,
//        "CreationDate": "2019-11-27T00:00:00",
//        "CreatedBy": 4,
//        "Status": 1,
//        "LastUpdatedDate": "2019-12-11T06:24:58.647",
//        "LastUpdatedBy": 19
    
}
struct RecevedDetils:Decodable {
    let SID:Int
//    "OrgSID": 1,
//    "VisitSID": 13,
    let ReceivedDataFilesID:String
    let ReceivedDataDescription:String
    let SpatialAccuracy:String
    let DataCreationDate:String
    let DataLastUpdatedDate:String
//    "CreationDate": "2019-12-04T13:52:48.29",
//    "CreatedBy": 19,
//    "Status": 1,
//    "LastUpdatedDate": "2019-12-11T06:10:21.62",
//    "LastUpdatedBy": 4
}

struct dynamicDetails:Decodable {
    let SID:Int
//    "SystemUserSID": 19,
//    "VisitSID": 13,
    let DynamicAttributeSID:Int
    let DynamicAttributeValue:String
//    "CreationDate": "2019-12-05T10:11:58.537",
//    "CreatedBy": 19,
//    "Status": 1,
    let AttributeTitle:String
}

struct saveattrObj:Decodable {
    let UserID:Int
    let VisitID:Int
    let AttributeID:Int
    let AttributeValue:String
}


struct saveattrObj2: Codable {
    let UserID:Int
       let VisitID:Int
       let AttributeID:Int
       let AttributeValue:String
    
  
    init( UserID : Int, VisitID : Int, AttributeID: Int, AttributeValue:String) {
        self.UserID  = UserID
        self.VisitID = VisitID
        self.AttributeID = AttributeID
        self.AttributeValue = AttributeValue
    }
    func toJSON() -> [String:Any] {
        var dictionary: [String : Any] = [:]
        
        dictionary["UserID"] = self.UserID
        dictionary["VisitID"] = self.VisitID
        dictionary["AttributeID"] = self.AttributeID
        dictionary["AttributeValue"] = self.AttributeValue
        
        return dictionary
    }
    
    
}



struct orgbranchList:Decodable {
    let Status:Int
    let OrganizationBranches:[branchObj]
    let Message:String
}

struct branchObj:Decodable {
    let SID:Int
    let Title:String
    let FormTemplate_SID:Int
}
