//
//  Visits.swift
//  Haj
//
//  Created by elsaid yousif on 12/3/19.
//  Copyright © 2019 ITRoots. All rights reserved.
//

import Foundation
struct visit: Decodable{
    let SID:Int
    let Title:String?
//    let FormTempateSID:Int
//    let SystemUserSID:Int
//    let OrgSID:Int
    let VisitDate:String
//    let VisitNumber:Double
//    let Lat:Double?
//    let Long:Double?
//    let CreationDate:Date
//    let CreatedBy:Int
    let Status:Int
    let OrgName:String
    
}
struct listUserVisits: Decodable{
    let Status:Int?
    let NotificationCount:Int?
    let VisitsList:[visit]?
    let Message:String
}



