//
//  notes.swift
//  Haj
//
//  Created by elsaid yousif on 12/11/19.
//  Copyright © 2019 ITRoots. All rights reserved.
//

import Foundation

struct notesList:Decodable {
    let Status:Int
    let CommentsList:[comment]
    let Message:String
    
}
struct comment:Decodable {
    let SID:Int
    let AdminFullName:String
    let CommentText:String
    let CreationDate:String
   
}
