//
//  AppDelegate.swift
//
//
//
//  Copyright © 2019 ITRoots. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps

import Firebase
import FirebaseMessaging


let googleApiKey = "AIzaSyCLs49DFM0TzhWmSBQljnAPh8IZqPOBZbI" //haj created on dahyan gmail account 
var langForApi:Int = 0

  let gcmMessageIDKey = "gcm.message_id"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate ,UNUserNotificationCenterDelegate, MessagingDelegate{

    var window: UIWindow?

var enableAllOrientation = false
  
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }
        
         GMSServices.provideAPIKey(googleApiKey)
           L102Localizer.DoTheMagic()
         IQKeyboardManager.shared.enable = true
        
        
      UITabBar.appearance().unselectedItemTintColor = UIColor.white
    UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Cairo SemiBold", size: 9)!], for: UIControl.State.normal)
    UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Cairo SemiBold", size: 9)!], for: UIControl.State.selected)
       
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: UIFont(name: "Cairo SemiBold", size: 9)!], for: .normal)
         UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: UIFont(name: "Cairo SemiBold", size: 9)!], for: .selected)
        

        let verification_status = UserDefaults.standard.bool(forKey: "HajIsVerifiedStatus")

        
        
        var _: UIView.AnimationOptions = .transitionFlipFromLeft
        if L102Language.currentAppleLanguage() == "ar"{

                       langForApi =  1
                   
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            
        }else{

            langForApi =  2
                           
UIView.appearance().semanticContentAttribute = .forceRightToLeft
            
        }
        
        if verification_status == true  {
            
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let mainViewController = mainStoryBoard.instantiateViewController(withIdentifier: "mainViewController") as! UITabBarController
       
mainViewController.selectedIndex = 3
            
            self.window?.rootViewController = mainViewController
        }
        else {
           
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let mainViewController = mainStoryBoard.instantiateViewController(withIdentifier: "rootnav") as! ViewController
          

            self.window?.rootViewController = mainViewController
        }
        
        
        FirebaseApp.configure()
        if #available(iOS 10.0, *) {
            
            UNUserNotificationCenter.current().delegate = self

            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
        let token = Messaging.messaging().fcmToken
        let defaults = UserDefaults.standard
        defaults.set(token, forKey: "userRegID")
        print("FCM token: \(token ?? "")")
        
        
        _ = UINavigationBar.appearance()
        
        
      return true
    }

    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        UserDefaults.standard.setValue(fcmToken, forKey: "firebase_token")
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        UserDefaults.standard.set(NSDate(), forKey: "mostRecentPushNotification")
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        var NotificationTitle: String? = NSLocalizedString("NotificationRecieved", comment: "")
        var NotificationMessage: String? = NSLocalizedString("NotificationRecievedMsg", comment: "")
        if let aps = userInfo[AnyHashable("aps")] as? NSDictionary {
            if let alert = aps["alert"] as? NSDictionary {
                if let body = alert["body"] as? String {
                    NotificationMessage = body
                    print(body)
                }
                if let title = alert["title"] as? String {
                    NotificationTitle = title
                    print(title)
                }
            }
            else{
                if let body = aps["alert"] as? String {
                    NotificationMessage = body
                    print(body)
                }
            }
            
        }
        if let _ = userInfo["gcm.notification.orderId"] as? String {
            
            if  application.applicationState != UIApplication.State.background &&  application.applicationState != UIApplication.State.active {
                
                print("User Tap on notification Started the App")

            }
            else if application.applicationState != UIApplication.State.background  {
                let topWindow = UIWindow(frame: UIScreen.main.bounds)
                topWindow.rootViewController = UIViewController()
                topWindow.windowLevel = UIWindow.Level.alert + 1
                
                let alert = UIAlertController(title: NotificationTitle, message: NotificationMessage!, preferredStyle: .alert)
                
                  let paragraphStyle = NSMutableParagraphStyle()

                     if L102Language.currentAppleLanguage() == "ar"{
                        print("ar alert")
                        paragraphStyle.alignment = .right
                     }else{
                          paragraphStyle.alignment = .left
                         print("en alert")
                    }
                
                        
                    let messageText = NSMutableAttributedString(
                        string: NotificationMessage!,
                        attributes: [
                            NSAttributedString.Key.paragraphStyle: paragraphStyle,
                            NSAttributedString.Key.font : UIFont.preferredFont(forTextStyle: .body),
                            NSAttributedString.Key.foregroundColor : UIColor.black
                        ]
                    )

                    alert.setValue(messageText, forKey: "attributedMessage")
                
                let TitleText = NSMutableAttributedString(
                    string: NotificationTitle!,
                               attributes: [
                                   NSAttributedString.Key.paragraphStyle: paragraphStyle,
                                   NSAttributedString.Key.font : UIFont.preferredFont(forTextStyle: .body),
                                   NSAttributedString.Key.foregroundColor : UIColor.black
                               ]
                           )

                alert.setValue(TitleText, forKey: "attributedTitle")
                
                alert.addAction(UIAlertAction(title: NSLocalizedString(NSLocalizedString("Ok", comment: ""), comment: "confirm"), style: .default, handler: {(_ action: UIAlertAction) -> Void in

                    completionHandler(UIBackgroundFetchResult.newData)
                    topWindow.isHidden = true
                }))
                alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .default, handler: { (_ action: UIAlertAction) -> Void in
                    print("user cancel to show notification")
                    completionHandler(UIBackgroundFetchResult.newData)
                }))
                topWindow.makeKeyAndVisible()
                topWindow.rootViewController?.present(alert, animated: true, completion: nil)
            }
            
            
        }
        else{
            if application.applicationState != UIApplication.State.background  {
                let topWindow = UIWindow(frame: UIScreen.main.bounds)
                topWindow.rootViewController = UIViewController()
                topWindow.windowLevel = UIWindow.Level.alert + 1
                let alert = UIAlertController(title: NotificationTitle, message: NotificationMessage, preferredStyle: .alert)
                
                
                  let paragraphStyle = NSMutableParagraphStyle()

                     if L102Language.currentAppleLanguage() == "ar"{
                        print("ar alert")
                        paragraphStyle.alignment = .right
                     }else{
                          paragraphStyle.alignment = .left
                         print("en alert")
                    }
                
                        
                    let messageText = NSMutableAttributedString(
                        string: NotificationMessage!,
                        attributes: [
                            NSAttributedString.Key.paragraphStyle: paragraphStyle,
                            NSAttributedString.Key.font : UIFont.preferredFont(forTextStyle: .body),
                            NSAttributedString.Key.foregroundColor : UIColor.black
                        ]
                    )

                    alert.setValue(messageText, forKey: "attributedMessage")
                
                let TitleText = NSMutableAttributedString(
                    string: NotificationTitle!,
                               attributes: [
                                   NSAttributedString.Key.paragraphStyle: paragraphStyle,
                                   NSAttributedString.Key.font : UIFont.preferredFont(forTextStyle: .body),
                                   NSAttributedString.Key.foregroundColor : UIColor.black
                               ]
                           )

                alert.setValue(TitleText, forKey: "attributedTitle")
                
                
                alert.addAction(UIAlertAction(title: NSLocalizedString(NSLocalizedString("Ok", comment: ""), comment: "confirm"), style: .default, handler: {(_ action: UIAlertAction) -> Void in
                    
                    completionHandler(UIBackgroundFetchResult.newData)
                    topWindow.isHidden = true
                }))
                topWindow.makeKeyAndVisible()
                topWindow.rootViewController?.present(alert, animated: true, completion: nil)
            }
            
        }
        completionHandler(UIBackgroundFetchResult.newData)
        
    }
    // [END receive_message]

    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        print("rotate")
        if (enableAllOrientation == true){
            return UIInterfaceOrientationMask.allButUpsideDown
        }
        return UIInterfaceOrientationMask.portrait
    }
    

}

