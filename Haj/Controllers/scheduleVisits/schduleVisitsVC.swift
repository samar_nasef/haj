//
//  schduleVisitsVC.swift
//
// The scheduling of visits screen shows you visits based on date
//
//  Copyright © 2020 ITRoots. All rights reserved.
//

import UIKit

class scheduleVisitTableViewCell2: UITableViewCell {
    
    @IBOutlet weak var notes: UILabel!
    @IBOutlet weak var orgName: UILabel!
    @IBOutlet weak var sysManager: UILabel!
    @IBOutlet weak var date: UILabel!
    
}
class schduleVisitsVC: LocalizerViewController,UITableViewDelegate,UITableViewDataSource
,FSCalendarDelegate,FSCalendarDataSource{
    
    fileprivate weak var calendar1: FSCalendar!
    
    @IBOutlet weak var scheduleTabelView: UITableView!
    
    var label:UILabel = UILabel()
    var sv: UIView?
    var scheduleVsitsData:[scheduleVisitObj]=[]
    var schduledate :String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-dd-yyyy"
        let currentDate = formatter.string(from: date)
        schduledate = currentDate
        schedulVisit()
        
        
        label =  UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
        
        
        drawNotificationBell()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.drawNotificationBell),
                                               name: NSNotification.Name(rawValue: "reloadnotificationCount"),
                                               object: nil)
        
        if L102Language.currentAppleLanguage() == "ar"{
            langForApi =  1
        }else{
            langForApi =  2
        }
        
        
        scheduleTabelView.delegate = self
        scheduleTabelView.dataSource = self
        scheduleTabelView.tableFooterView = UIView()
        
        
        
        let calendar1 = FSCalendar(frame: CGRect(x: 10, y: 80, width: UIScreen.main.bounds.width, height: 200))
        calendar1.dataSource = self
        calendar1.delegate = self
        calendar1.appearance.selectionColor = #colorLiteral(red: 0.5058823824, green: 0.3372549117, blue: 0.06666667014, alpha: 1)
        calendar1.appearance.todayColor = #colorLiteral(red: 0.6610268354, green: 0.08241639286, blue: 0.007120576221, alpha: 1)
        calendar1.appearance.titleTodayColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        calendar1.appearance.titleSelectionColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        calendar1.appearance.headerTitleColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        calendar1.appearance.weekdayTextColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        if L102Language.currentAppleLanguage() == "ar"{
            
            calendar1.locale = NSLocale(localeIdentifier: "ar_AR") as Locale
        }else{
            calendar1.locale = NSLocale(localeIdentifier: "en_EN") as Locale
        }
        
        view.addSubview(calendar1)
        self.calendar1 = calendar1
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scheduleVsitsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "scheduleCell", for: indexPath) as! scheduleVisitTableViewCell2
        
        let item = self.scheduleVsitsData[indexPath.row]
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatterGet.timeZone = TimeZone.current
        let estimatedDate = dateFormatterGet.date(from: item.VisitDate)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let estimatedDateString = dateFormatter.string(from: estimatedDate!)
        cell.date.text = estimatedDateString
        cell.notes.text = item.Notes
        cell.orgName.text = item.OrgTitle
        cell.sysManager.text = item.AdminName
        
        return cell
        
    }
    
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss +SSSS"
        dateFormatterGet.timeZone = TimeZone.current
        let myString = dateFormatterGet.string(from: date)
        let estimatedDate = dateFormatterGet.date(from: myString)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let estimatedDateString = dateFormatter.string(from: estimatedDate!)
        schduledate = estimatedDateString
        schedulVisit()
        
    }
    
    @objc func drawNotificationBell(){
        let notificationButton = SSBadgeButton()
        notificationButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        notificationButton.setImage(UIImage(named: "notificationsButton-1")?.withRenderingMode(.alwaysTemplate), for: .normal)
        notificationButton.tintColor = .white
        
        if UserDefaults.standard.integer(forKey: "notificationCount") > 0 {
            
            notificationButton.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
            notificationButton.badge = String(UserDefaults.standard.integer(forKey: "notificationCount"))
        }
        
        notificationButton.addTarget(self, action: #selector(openNotification(sender:)), for: .touchUpInside)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: notificationButton)
    }
    
    
    @objc func openNotification(sender: UIBarButtonItem){
        
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "notificationVc") as! notificationsViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
        
    }
    
    
    func schedulVisit() {
        if Reachability.isConnectedToNetwork() {
            self.sv = UIViewController.displaySpinner(onView: self.view)
            let userId = UserDefaults.standard.integer(forKey: "HajUserID")
            let jsonUrlString = scheduleEndpoint + "\(schduledate.replacedArabicDigitsWithEnglish)&user=\(String(userId))&lang=\(String(langForApi))"
            guard let url = URL(string: jsonUrlString) else { return }
            URLSession.shared.dataTask(with: url) { (data, response, err) in
                UIViewController.removeSpinner(spinner: self.sv!)
                if (err != nil){
                    DispatchQueue.main.async {
                        self.view.presentToast(msg: NSLocalizedString("serverErr", comment: "") )
                    }
                    return
                }
                if let status = response as? HTTPURLResponse {
                    
                    if status.statusCode != 200{
                        DispatchQueue.main.async { self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                        }
                        return
                    }
                }
                guard let data = data else { return }
                do {
                    print(data)
                    let response = try JSONDecoder().decode(scheduleDataRes.self, from: data)
                    
                    
                    if response.Status == 1 {
                        
                        self.scheduleVsitsData = response.VisitsList
                        
                        if  self.scheduleVsitsData.count == 0 {
                            DispatchQueue.main.async {
                                self.scheduleTabelView.reloadData()
                                self.label.center = CGPoint(x: 160, y: 400)
                                self.label.textAlignment = .center
                                self.label.center.x = self.view.center.x
                                self.label.font = UIFont(name: "Cairo SemiBold", size: 16)
                                self.label.textColor = #colorLiteral(red: 0.4476690292, green: 0.4476690292, blue: 0.4476690292, alpha: 1)
                                self.label.text = response.Message
                                self.view.addSubview(self.label)
                                
                            }
                            
                            
                        }else{
                            DispatchQueue.main.async {
                                self.label.removeFromSuperview()
                                self.scheduleTabelView.reloadData()
                            }
                            
                        }
                        
                    }
                    else{
                        DispatchQueue.main.async {
                            
                            if response.VisitsList.count == 0 {
                                self.scheduleVsitsData = response.VisitsList
                                self.scheduleTabelView.reloadData()
                            }
                            
                            self.label.center = CGPoint(x: 160, y: 420)
                            self.label.textAlignment = .center
                            self.label.tag = 1
                            self.label.center.x = self.view.center.x
                            self.label.font = UIFont(name: "Cairo SemiBold", size: 16)
                            self.label.textColor = #colorLiteral(red: 0.4476690292, green: 0.4476690292, blue: 0.4476690292, alpha: 1)
                            self.label.text = response.Message
                            self.view.addSubview(self.label)
                            
                        }
                        
                    }
                }
                    
                catch let jsonErr {
                    print("Error serializing json:", jsonErr)
                    self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                }
            }.resume()
        }
        else{
            self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
        }
    }
    
}
