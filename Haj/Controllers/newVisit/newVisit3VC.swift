//
//  newVisit3VC.swift
//
//
//
//  Copyright © 2019 ITRoots. All rights reserved.
//

import UIKit

class newVisit3VC: LocalizerViewController,UITextViewDelegate {
    
    @IBOutlet weak var date2colors: UILabel!
    @IBOutlet weak var date1colors: UILabel!
    @IBOutlet weak var filesColors: UILabel!
    @IBOutlet weak var acccolors: UILabel!
    @IBOutlet weak var descColors: UILabel!
    @IBOutlet weak var widthConstraintForsecondView: NSLayoutConstraint!
    @IBOutlet weak var widthConstraintForFirstView: NSLayoutConstraint!
    @IBOutlet weak var receivedfilesText: UITextView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var isgeoDataBtn: UIButton!
    var isgeoDataBtnActive = false
    var organizationId:Int = -1
    var TemplateSid:Int = 0
    var visitId:Int = -1
    var sv: UIView?
    var from :String = ""
    var from2 :String = ""
    var data:RecevedDetils = RecevedDetils(SID: -1, ReceivedDataFilesID: "", ReceivedDataDescription: "", SpatialAccuracy: "", DataCreationDate: "", DataLastUpdatedDate: "")
    
    @IBOutlet weak var shapeFileBtn: UIButton!
    var isshapeFileActive = false
    @IBOutlet weak var mapsbtn: UIButton!
    var ismapActive = false
    @IBOutlet weak var dataBaseBtn: UIButton!
    var isdatabaseActive = false
    @IBOutlet weak var pdfBtn: UIButton!
    var ispdfActive = false
    @IBOutlet weak var cadBtn: UIButton!
    var iscadActive = false
    @IBOutlet weak var kmlBtn: UIButton!
    var iskmlactive = false
    var data4:[dynamicDetails] = []
    var Choosenfiles:[String] = []
    var errdate1:Bool = false
    var errdate2:Bool = false
    @IBOutlet weak var acc: UITextField!
    @IBOutlet weak var updatedate: UITextField!
    @IBOutlet weak var createDate: UITextField!
    @IBOutlet weak var ContentView: UIView!
    var datepicker:UIDatePicker?
    var datepicker2:UIDatePicker?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if L102Language.currentAppleLanguage() == "ar"{
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "leftArrow"), style: .plain, target: self, action: #selector(gobackTapped))
            self.navigationItem.rightBarButtonItem?.tintColor = .white
        }else{
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "leftArrowCopy-1"), style: .plain, target: self, action: #selector(gobackTapped))
            self.navigationItem.rightBarButtonItem?.tintColor = .white
            
        }
        
        if L102Language.currentAppleLanguage() == "ar"{
            
            receivedfilesText.textAlignment  = .right
            langForApi =  1
            
        }else{
            langForApi =  2
            receivedfilesText.textAlignment  = .left
        }
        
        
        if isgeoDataBtnActive == false {
            filesColors.textColor   = #colorLiteral(red: 0.5490196078, green: 0.5490196078, blue: 0.5490196078, alpha: 1)
            descColors.textColor   = #colorLiteral(red: 0.5490196078, green: 0.5490196078, blue: 0.5490196078, alpha: 1)
            acccolors.textColor   = #colorLiteral(red: 0.5490196078, green: 0.5490196078, blue: 0.5490196078, alpha: 1)
            date1colors.textColor   = #colorLiteral(red: 0.5490196078, green: 0.5490196078, blue: 0.5490196078, alpha: 1)
            date2colors.textColor   = #colorLiteral(red: 0.5490196078, green: 0.5490196078, blue: 0.5490196078, alpha: 1)
            
            ContentView.backgroundColor = #colorLiteral(red: 0.9461328747, green: 0.9461328747, blue: 0.9461328747, alpha: 1)
            saveBtn.isHidden = true
            mapsbtn.isUserInteractionEnabled = false
            pdfBtn.isUserInteractionEnabled = false
            shapeFileBtn.isUserInteractionEnabled = false
            kmlBtn.isUserInteractionEnabled = false
            dataBaseBtn.isUserInteractionEnabled = false
            cadBtn.isUserInteractionEnabled = false
            acc.isUserInteractionEnabled = false
            receivedfilesText.isUserInteractionEnabled = false
            createDate.isUserInteractionEnabled = false
            updatedate.isUserInteractionEnabled = false
            
            
        }else   if isgeoDataBtnActive == true {
            filesColors.textColor   =  .black
            descColors.textColor   = .black
            acccolors.textColor   = .black
            date1colors.textColor   = .black
            date2colors.textColor   = .black
            
            ContentView.backgroundColor = .white
            saveBtn.isHidden = false
            mapsbtn.isUserInteractionEnabled = true
            pdfBtn.isUserInteractionEnabled = true
            shapeFileBtn.isUserInteractionEnabled = true
            kmlBtn.isUserInteractionEnabled = true
            dataBaseBtn.isUserInteractionEnabled = true
            cadBtn.isUserInteractionEnabled = true
            acc.isUserInteractionEnabled = true
            receivedfilesText.isUserInteractionEnabled = true
            createDate.isUserInteractionEnabled = true
            updatedate.isUserInteractionEnabled = true
            
            
        }
        
        
        
        if from2 == "show" {
            saveBtn.isHidden = true
            
            isgeoDataBtn.isUserInteractionEnabled = false
            
            mapsbtn.isUserInteractionEnabled = false
            pdfBtn.isUserInteractionEnabled = false
            shapeFileBtn.isUserInteractionEnabled = false
            kmlBtn.isUserInteractionEnabled = false
            dataBaseBtn.isUserInteractionEnabled = false
            cadBtn.isUserInteractionEnabled = false
            acc.isUserInteractionEnabled = false
            receivedfilesText.isUserInteractionEnabled = false
            createDate.isUserInteractionEnabled = false
            updatedate.isUserInteractionEnabled = false
            
        }
        if from == "edit_show" {
            print("received data \(data)")
            if data.SID != -1 {
                isgeoDataBtnActive = true
                if isgeoDataBtnActive == false{
                    filesColors.textColor   = #colorLiteral(red: 0.5490196078, green: 0.5490196078, blue: 0.5490196078, alpha: 1)
                    descColors.textColor   = #colorLiteral(red: 0.5490196078, green: 0.5490196078, blue: 0.5490196078, alpha: 1)
                    acccolors.textColor   = #colorLiteral(red: 0.5490196078, green: 0.5490196078, blue: 0.5490196078, alpha: 1)
                    date1colors.textColor   = #colorLiteral(red: 0.5490196078, green: 0.5490196078, blue: 0.5490196078, alpha: 1)
                    date2colors.textColor   = #colorLiteral(red: 0.5490196078, green: 0.5490196078, blue: 0.5490196078, alpha: 1)
                    
                    ContentView.backgroundColor = #colorLiteral(red: 0.9461328747, green: 0.9461328747, blue: 0.9461328747, alpha: 1)
                    saveBtn.isHidden = true
                    mapsbtn.isUserInteractionEnabled = false
                    pdfBtn.isUserInteractionEnabled = false
                    shapeFileBtn.isUserInteractionEnabled = false
                    kmlBtn.isUserInteractionEnabled = false
                    dataBaseBtn.isUserInteractionEnabled = false
                    cadBtn.isUserInteractionEnabled = false
                    acc.isUserInteractionEnabled = false
                    receivedfilesText.isUserInteractionEnabled = false
                    createDate.isUserInteractionEnabled = false
                    updatedate.isUserInteractionEnabled = false
                    
                    
                }else   if isgeoDataBtnActive == true{
                    filesColors.textColor   =  .black
                    descColors.textColor   = .black
                    acccolors.textColor   = .black
                    date1colors.textColor   = .black
                    date2colors.textColor   = .black
                    
                    ContentView.backgroundColor = .white
                    if from2 == "show" {
                        saveBtn.isHidden = true
                        
                        mapsbtn.isUserInteractionEnabled = false
                        pdfBtn.isUserInteractionEnabled = false
                        shapeFileBtn.isUserInteractionEnabled = false
                        kmlBtn.isUserInteractionEnabled = false
                        dataBaseBtn.isUserInteractionEnabled = false
                        cadBtn.isUserInteractionEnabled = false
                        acc.isUserInteractionEnabled = false
                        receivedfilesText.isUserInteractionEnabled = false
                        createDate.isUserInteractionEnabled = false
                        updatedate.isUserInteractionEnabled = false
                        
                    }else {
                        saveBtn.isHidden = false
                        
                        mapsbtn.isUserInteractionEnabled = true
                        pdfBtn.isUserInteractionEnabled = true
                        shapeFileBtn.isUserInteractionEnabled = true
                        kmlBtn.isUserInteractionEnabled = true
                        dataBaseBtn.isUserInteractionEnabled = true
                        cadBtn.isUserInteractionEnabled = true
                        acc.isUserInteractionEnabled = true
                        receivedfilesText.isUserInteractionEnabled = true
                        createDate.isUserInteractionEnabled = true
                        updatedate.isUserInteractionEnabled = true
                    }
                    
                }
                
                
                
                isgeoDataBtn.setImage(UIImage(named: "vectorSmartObject-2"), for: .normal)
                
                let files =  data.ReceivedDataFilesID.split(separator: ",")
                for file in files {
                    switch file {
                    case "6":
                        ispdfActive = true
                        pdfBtn.setImage(UIImage(named: "vectorSmartObject-2"), for: .normal)
                        Choosenfiles.append("6")
                    case "4":
                        iscadActive = true
                        cadBtn.setImage(UIImage(named: "vectorSmartObject-2"), for: .normal)
                        Choosenfiles.append("4")
                        
                    case "2":
                        iskmlactive = true
                        kmlBtn.setImage(UIImage(named: "vectorSmartObject-2"), for: .normal)
                        Choosenfiles.append("2")
                    case "5":
                        isdatabaseActive = true
                        dataBaseBtn.setImage(UIImage(named: "vectorSmartObject-2"), for: .normal)
                        Choosenfiles.append("5")
                    case "3":
                        isshapeFileActive = true
                        shapeFileBtn.setImage(UIImage(named: "vectorSmartObject-2"), for: .normal)
                        Choosenfiles.append("3")
                    case "1":
                        ismapActive = true
                        mapsbtn.setImage(UIImage(named: "vectorSmartObject-2"), for: .normal)
                        Choosenfiles.append("1")
                    default:
                        print("switch default")
                    }
                }
                
                acc.text = data.SpatialAccuracy
                createDate.text = String(data.DataCreationDate.split(separator: "T")[0])
                if data.DataLastUpdatedDate == "" {
                    updatedate.text  = ""
                    
                }else {
                    updatedate.text = String(data.DataLastUpdatedDate.split(separator: "T")[0])
                }
                
            }
        }
        
        datepicker = UIDatePicker()
        datepicker?.datePickerMode = .date
        datepicker?.addTarget(self, action: #selector(dateChanged(datepicker:)), for: .valueChanged)
        
        
        if L102Language.currentAppleLanguage() == "ar"{
            
            datepicker?.locale = NSLocale(localeIdentifier: "ar_AR") as Locale
            
        }else{
            datepicker?.locale = NSLocale(localeIdentifier: "en_EN") as Locale
        }
        
        datepicker2 = UIDatePicker()
        datepicker2?.datePickerMode = .date
        datepicker2?.addTarget(self, action: #selector(dateChanged2(datepicker:)), for: .valueChanged)
        
        
        if L102Language.currentAppleLanguage() == "ar"{
            
            datepicker2?.locale = NSLocale(localeIdentifier: "ar_AR") as Locale
            
        }else{
            datepicker2?.locale = NSLocale(localeIdentifier: "en_EN") as Locale
        }
        
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(tap(tapgesture:)))
        
        view.addGestureRecognizer(tapgesture)
        
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40))
        toolbar.barStyle = .default
        toolbar.tintColor = .black
        let doneBtn = UIBarButtonItem(title: NSLocalizedString("donetxt", comment: ""), style: .done, target: self, action:#selector(doneFunc(done:)))
        
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil);
        
        toolbar.setItems([flexibleSpace,doneBtn], animated: true)
        
        let toolbar2 = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40))
        toolbar2.barStyle = .default
        toolbar2.tintColor = .black
        let doneBtn2 = UIBarButtonItem(title: NSLocalizedString("donetxt", comment: ""), style: .done, target: self, action:#selector(doneFunc2(done:)))
        
        let flexibleSpace2 = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil);
        
        toolbar2.setItems([flexibleSpace2,doneBtn2], animated: true)
        
        updatedate.inputAccessoryView = toolbar
        updatedate.inputView = datepicker
        createDate.inputAccessoryView = toolbar2
        createDate.inputView = datepicker2
        
        if from == "edit_show" && from2 == "show"{
            self.navigationItem.title = NSLocalizedString("visitdetails", comment: "")
        }else   if from == "edit_show" && from2 == ""{
            self.navigationItem.title = NSLocalizedString("editVisit", comment: "")
        }else{
            self.navigationItem.title = NSLocalizedString("newVisit", comment: "")
        }
        
        
        self.navigationItem.hidesBackButton = true
        
        receivedfilesText.delegate = self
        
        if from == "edit_show" && data.SID != -1  {
            receivedfilesText.text = data.ReceivedDataDescription
        }else{
            receivedfilesText.text = NSLocalizedString("descOfReceivedfiles", comment: "")
        }
        
        receivedfilesText.textColor = UIColor.lightGray
        receivedfilesText.layer.cornerRadius = 8
        receivedfilesText.layer.borderWidth = 1.0
        receivedfilesText.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.9040828339)
        let width  = UIScreen.main.bounds.width
        widthConstraintForFirstView.constant = (width/2)-20
        widthConstraintForsecondView.constant = width/2
        
        saveBtn.layer.cornerRadius = 21
        let gradient = CAGradientLayer()
        gradient.frame = saveBtn.bounds
        gradient.cornerRadius = 21
        gradient.startPoint = CGPoint(x:0.0, y:0.5)
        gradient.endPoint = CGPoint(x:1.0, y:0.5)
        let colorright = UIColor(red: 150.0 / 255.0, green: 0.0 / 255.0, blue: 5.0 / 255.0, alpha: 1.0).cgColor
        let colorleft = UIColor(red: 223.0 / 255.0, green: 18.0 / 255.0, blue: 28.0 / 255.0, alpha: 1.0).cgColor
        gradient.colors = [colorright, colorleft]
        saveBtn.layer.addSublayer(gradient)
        
        nextBtn.layer.cornerRadius = 21
        let gradient1 = CAGradientLayer()
        gradient1.frame = nextBtn.bounds
        gradient1.cornerRadius = 21
        gradient1.startPoint = CGPoint(x:0.0, y:0.5)
        gradient1.endPoint = CGPoint(x:1.0, y:0.5)
        let colorright1 = UIColor(red: 169.0 / 255.0, green: 125.0 / 255.0, blue: 52.0 / 255.0, alpha: 1.0).cgColor
        let colorleft1 = UIColor(red: 207.0 / 255.0, green: 161.0 / 255.0, blue: 85.0 / 255.0, alpha: 1.0).cgColor
        gradient1.colors = [colorright1, colorleft1]
        nextBtn.layer.addSublayer(gradient1)
    }
    
    
    @IBAction func isgeoDataClicked(_ sender: UIButton) {
        print("isgeoDataBtnActive : \(isgeoDataBtnActive)")
        isgeoDataBtnActive = !isgeoDataBtnActive
        
        if isgeoDataBtnActive {
            isgeoDataBtn.setImage(UIImage(named: "vectorSmartObject-2"), for: .normal)
        } else {
            isgeoDataBtn.setImage(UIImage(named:"roundedRectangle3"), for: .normal)
        }
        
        if isgeoDataBtnActive == false{
            filesColors.textColor   = #colorLiteral(red: 0.5490196078, green: 0.5490196078, blue: 0.5490196078, alpha: 1)
            descColors.textColor   = #colorLiteral(red: 0.5490196078, green: 0.5490196078, blue: 0.5490196078, alpha: 1)
            acccolors.textColor   = #colorLiteral(red: 0.5490196078, green: 0.5490196078, blue: 0.5490196078, alpha: 1)
            date1colors.textColor   = #colorLiteral(red: 0.5490196078, green: 0.5490196078, blue: 0.5490196078, alpha: 1)
            date2colors.textColor   = #colorLiteral(red: 0.5490196078, green: 0.5490196078, blue: 0.5490196078, alpha: 1)
            
            ContentView.backgroundColor = #colorLiteral(red: 0.9461328747, green: 0.9461328747, blue: 0.9461328747, alpha: 1)
            saveBtn.isHidden = true
            mapsbtn.isUserInteractionEnabled = false
            pdfBtn.isUserInteractionEnabled = false
            shapeFileBtn.isUserInteractionEnabled = false
            kmlBtn.isUserInteractionEnabled = false
            dataBaseBtn.isUserInteractionEnabled = false
            cadBtn.isUserInteractionEnabled = false
            acc.isUserInteractionEnabled = false
            receivedfilesText.isUserInteractionEnabled = false
            createDate.isUserInteractionEnabled = false
            updatedate.isUserInteractionEnabled = false
            
            
        }else   if isgeoDataBtnActive == true{
            filesColors.textColor   =  .black
            descColors.textColor   = .black
            acccolors.textColor   = .black
            date1colors.textColor   = .black
            date2colors.textColor   = .black
            
            ContentView.backgroundColor = .white
            saveBtn.isHidden = false
            mapsbtn.isUserInteractionEnabled = true
            pdfBtn.isUserInteractionEnabled = true
            shapeFileBtn.isUserInteractionEnabled = true
            kmlBtn.isUserInteractionEnabled = true
            dataBaseBtn.isUserInteractionEnabled = true
            cadBtn.isUserInteractionEnabled = true
            acc.isUserInteractionEnabled = true
            receivedfilesText.isUserInteractionEnabled = true
            createDate.isUserInteractionEnabled = true
            updatedate.isUserInteractionEnabled = true
            
            
        }
        
        
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = NSLocalizedString("descOfReceivedfiles", comment: "")
            textView.textColor = UIColor.lightGray
        }
    }
    
    
    @IBAction func pdfClicked(_ sender: UIButton) {
        //6
        print("ispdfActive : \(ispdfActive)")
        ispdfActive = !ispdfActive
        
        if ispdfActive {
            pdfBtn.setImage(UIImage(named: "vectorSmartObject-2"), for: .normal)
            Choosenfiles.append("6")
        } else {
            pdfBtn.setImage(UIImage(named:"roundedRectangle3"), for: .normal)
            for (i,e) in Choosenfiles.enumerated() {
                if e == "6"{
                    Choosenfiles.remove(at: i)
                }
            }
            
        }
        
    }
    @IBAction func cadClicked(_ sender: UIButton) {
        //4
        iscadActive = !iscadActive
        if iscadActive {
            cadBtn.setImage(UIImage(named: "vectorSmartObject-2"), for: .normal)
            Choosenfiles.append("4")
        } else {
            cadBtn.setImage(UIImage(named:"roundedRectangle3"), for: .normal)
            for (i,e) in Choosenfiles.enumerated() {
                if e == "4"{
                    Choosenfiles.remove(at: i)
                }
            }
            
        }
        
        
    }
    
    @IBAction func kmlClicked(_ sender: UIButton) {
        //2
        iskmlactive = !iskmlactive
        if iskmlactive {
            kmlBtn.setImage(UIImage(named: "vectorSmartObject-2"), for: .normal)
            Choosenfiles.append("2")
        } else {
            kmlBtn.setImage(UIImage(named:"roundedRectangle3"), for: .normal)
            for (i,e) in Choosenfiles.enumerated() {
                if e == "2"{
                    Choosenfiles.remove(at: i)
                }
            }
            
        }
        
        
    }
    
    
    @IBAction func dataBaseClicked(_ sender: UIButton) {
        //5
        isdatabaseActive = !isdatabaseActive
        if isdatabaseActive {
            dataBaseBtn.setImage(UIImage(named: "vectorSmartObject-2"), for: .normal)
            Choosenfiles.append("5")
        } else {
            dataBaseBtn.setImage(UIImage(named:"roundedRectangle3"), for: .normal)
            for (i,e) in Choosenfiles.enumerated() {
                if e == "5"{
                    Choosenfiles.remove(at: i)
                }
            }
            
        }
        
        
    }
    
    @IBAction func shapeFileClicked(_ sender: UIButton) {
        //3
        isshapeFileActive = !isshapeFileActive
        if isshapeFileActive {
            shapeFileBtn.setImage(UIImage(named: "vectorSmartObject-2"), for: .normal)
            Choosenfiles.append("3")
        } else {
            shapeFileBtn.setImage(UIImage(named:"roundedRectangle3"), for: .normal)
            for (i,e) in Choosenfiles.enumerated() {
                if e == "3"{
                    Choosenfiles.remove(at: i)
                }
            }
            
        }
        
        
    }
    
    @IBAction func mapsclicked(_ sender: UIButton) {
        //1
        ismapActive = !ismapActive
        if ismapActive {
            mapsbtn.setImage(UIImage(named: "vectorSmartObject-2"), for: .normal)
            Choosenfiles.append("1")
        } else {
            mapsbtn.setImage(UIImage(named:"roundedRectangle3"), for: .normal)
            for (i,e) in Choosenfiles.enumerated() {
                if e == "1"{
                    Choosenfiles.remove(at: i)
                }
            }
        }
        
        
    }
    @IBAction func saveClicked(_ sender: UIButton) {
        print("isgeoDataBtnActive = \(isgeoDataBtnActive)")
        if isgeoDataBtnActive {
            let joined = Choosenfiles.joined(separator: ",")
            
            print("choosenFilesjoined = \(joined) ")
            
            if Choosenfiles.count == 0 {
                self.view.presentToast2(msg: NSLocalizedString("Receivedfileserr", comment: ""))
            }else if receivedfilesText.text == NSLocalizedString("descOfReceivedfiles", comment: ""){
                self.view.presentToast2(msg:  NSLocalizedString("Receiveddatadesc", comment: ""))
            }else if acc.text == ""{
                self.view.presentToast2(msg:  NSLocalizedString("Accerr", comment: ""))
            }else if createDate.text == ""{
                self.view.presentToast2(msg:  NSLocalizedString("Createerr", comment: ""))
            }else if self.errdate2 == true{
                self.view.presentToast2(msg:NSLocalizedString("Dateerr2", comment: ""))
                
            }else if self.errdate1 == true{
                self.view.presentToast2(msg:NSLocalizedString("Dateerr1", comment: ""))
                
            }
            else{
                
                var updateDateTemp = updatedate.text!
                if updateDateTemp == ""{
                    print("empty update date")
                    updateDateTemp = createDate.text!
                }
                
                if data.SID == -1 {
                    print("add new visit 3 ")
                    if Reachability.isConnectedToNetwork() {
                        sv = UIViewController.displaySpinner(onView: self.view)
                        let jsonUrlString = visitaddReceivedDataEndpoint
                        guard let url = URL(string: jsonUrlString) else { return }
                        do {
                            
                            print("user id \(UserDefaults.standard.integer(forKey: "HajUserID")) ,, visit id \(visitId),organizationId \(organizationId)")
                            
                            let postObject = [
                                "UserID": UserDefaults.standard.integer(forKey: "HajUserID"),
                                "OrgID": organizationId,
                                "VisitID":visitId,
                                "FilesIDCommaSeperated":Choosenfiles.joined(separator: ","),
                                "ReceivedDataDescription": receivedfilesText.text!,
                                "SpatialAccuracy": acc.text!,
                                "DataCreationDate": createDate.text!,
                                "DataUpdatedDate": updateDateTemp,
                                "Lang": langForApi
                                
                                ] as [String : Any]
                            
                            ApiService.callPost(url: url, params: postObject, finish: self.finishaddReceivedData)
                        }
                    }
                    else{
                        self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
                    }
                }else{
                    print("update new visit 3")
                    if Reachability.isConnectedToNetwork() {
                        sv = UIViewController.displaySpinner(onView: self.view)
                        let jsonUrlString = updateReceivedDataEndpoint
                        guard let url = URL(string: jsonUrlString) else { return }
                        do {
                            
                            print("user id \(UserDefaults.standard.integer(forKey: "HajUserID")) ,, visit id \(visitId),organizationId \(organizationId)")
                            
                            
                            print("data sid = \(data.SID)")
                            let postObject = [
                                
                                "ReceivedDataID":data.SID,
                                "UserID": UserDefaults.standard.integer(forKey: "HajUserID"),
                                "OrgID": organizationId,
                                "VisitID":visitId,
                                "FilesIDCommaSeperated":Choosenfiles.joined(separator: ","),
                                "ReceivedDataDescription": receivedfilesText.text!,
                                "SpatialAccuracy": acc.text!,
                                "DataCreationDate": createDate.text!,
                                "DataUpdatedDate": updateDateTemp,
                                "Lang": langForApi
                                
                                ] as [String : Any]
                            
                            ApiService.callPost(url: url, params: postObject, finish: self.finishaddReceivedData)
                        }
                    }
                    else{
                        self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
                    }
                    
                    
                }
                
            }
        }else{
            self.view.presentToast2(msg: NSLocalizedString("Isgeo", comment: ""))
        }
        
        
        
    }
    
    
    func finishaddReceivedData (message:String, data:Data?) -> Void
    {
        do
        {
            UIViewController.removeSpinner(spinner: self.sv!)
            if message == "Success" {
                if data != nil
                {
                    let response = try JSONDecoder().decode(visitAddReceievedDataRes.self, from: data!)
                    print("register response.Status : \(response.Status)")
                    
                    if response.Status == 1 {
                        
                        DispatchQueue.main.async {
                            self.view.presentToast2(msg: response.Message)
                            
                        }
                        
                    }
                    else {
                        self.view.presentToast2(msg: response.Message)
                    }
                    
                }
            }
            else{
                self.view.presentToast(msg:NSLocalizedString("serverErr", comment: "") )
                print("userregistered for notifications failed")
            }
        }
        catch
        {
            print("userregistered for notifications Parse Error: \(error)")
        }
    }
    
    
    
    
    @IBAction func nextClicked(_ sender: UIButton) {
        self.performSegue(withIdentifier: "visit4", sender: nil)
    }
    
    
    @IBAction func goback(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func dateChanged(datepicker:UIDatePicker) {
        print("datechanged")
        
    }
    @objc func dateChanged2(datepicker:UIDatePicker) {
        print("datechanged2")
        
    }
    @objc func tap(tapgesture:UITapGestureRecognizer) {
        
        print("tap gesture reconginzer")
        view.endEditing(true)
    }
    @objc func doneFunc(done:UIBarButtonItem){
        print("done btn")
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "MM-dd-yyyy"
        let lastupdatedate = dateformatter.string(from: self.datepicker!.date)
        updatedate.text = dateformatter.string(from: self.datepicker!.date)
        view.endEditing(true)
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-dd-yyyy"
        let currentDate = formatter.string(from: date)
        if lastupdatedate.compare(currentDate) == .orderedAscending {
            print("First Date is smaller then second date")
            self.view.presentToast2(msg: NSLocalizedString("Dateerr1", comment: "") )
            self.errdate1 = true
        }else{
            self.errdate1 = false
        }
        
    }
    @objc func doneFunc2(done:UIBarButtonItem){
        print("done btn 2")
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "MM-dd-yyyy"
        let creationDate = dateformatter.string(from: self.datepicker2!.date)
        
        createDate.text = dateformatter.string(from: self.datepicker2!.date)
        view.endEditing(true)
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-dd-yyyy"
        let currentDate = formatter.string(from: date)
        if creationDate.compare(currentDate) == .orderedAscending {
            print("First Date is smaller then second date")
            self.view.presentToast2(msg:NSLocalizedString("Dateerr2", comment: ""))
            self.errdate2 = true
        }else{
            self.errdate2 = false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "visit4" {
            if let destinationVC = segue.destination as? newVisit4VC {
                
                destinationVC.organizationId = organizationId
                destinationVC.visitId = visitId
                destinationVC.from = from
                destinationVC.from2 = from2
                destinationVC.data4 = data4
                destinationVC.TemplateSid = TemplateSid
                
                
            }
        }
        
    }
    
    @objc func gobackTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
}
