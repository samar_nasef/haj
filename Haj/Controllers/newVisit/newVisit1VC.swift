//
//  newVisit1VC.swift
//
//
//
//  Copyright © 2019 ITRoots. All rights reserved.
//

import UIKit

import MapKit
import CoreLocation


class newVisit1VC: LocalizerViewController ,CLLocationManagerDelegate{
    
    let locationManager = CLLocationManager()
    var userLat:Double = 0
    var userLong:Double = 0
    var locationEnabled = false
    
    @IBOutlet weak var branchBtn: UIButton!
    @IBOutlet weak var branchView: UIView!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var datebtn: UIButton!
    @IBOutlet weak var datepicker: UIDatePicker!
    @IBOutlet weak var orgView: UIView!
    @IBOutlet weak var startVisitBtn: UIButton!
    var sv: UIView?
    @IBOutlet weak var orgbtn: UIButton!
    @IBOutlet weak var visitObj: UITextField!
    var visitDate:String = ""
    var orgsDataArr : [organization] = []
    var branchesDataArr:[branchObj] = []
    var choosenOrgSid : Int = 0
    var choosenbranchSid:Int = 0
    var choosenTemplateSid : Int = 0
    var choosenbranchtemplate:Int = 0
    var visitId : Int = -1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if L102Language.currentAppleLanguage() == "ar"{
            
            orgbtn.contentHorizontalAlignment = .right
            branchBtn.contentHorizontalAlignment = .right
            datebtn.contentHorizontalAlignment = .right
            langForApi =  1
            
        }else{
            langForApi =  2
            orgbtn.contentHorizontalAlignment = .left
            branchBtn.contentHorizontalAlignment = .left
            datebtn.contentHorizontalAlignment = .left
        }
        
        
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        self.navigationItem.title = NSLocalizedString("newVisit", comment: "")
        self.navigationItem.hidesBackButton = true
        if L102Language.currentAppleLanguage() == "ar"{
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "leftArrow"), style: .plain, target: self, action: #selector(gobackTapped))
            self.navigationItem.rightBarButtonItem?.tintColor = .white
        }else{
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "leftArrowCopy-1"), style: .plain, target: self, action: #selector(gobackTapped))
            self.navigationItem.rightBarButtonItem?.tintColor = .white
            
        }
        
        dateView.layer.cornerRadius = 8
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let currentDate = formatter.string(from: date)
        datebtn.setTitle(currentDate, for: .normal)
        
        visitObj.layer.shadowOpacity = 1
        visitObj.layer.shadowRadius = 3.0
        visitObj.layer.shadowOffset = CGSize(width: 0, height: 3)
        visitObj.layer.shadowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        
        orgView.layer.cornerRadius = 8
        orgView.layer.shadowOpacity = 1
        orgView.layer.shadowRadius = 3.0
        orgView.layer.shadowOffset = CGSize(width: 0, height: 3)
        orgView.layer.shadowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        orgView.layer.borderWidth = 1
        orgView.layer.borderColor = #colorLiteral(red: 0.895331796, green: 0.895331796, blue: 0.895331796, alpha: 1)
        
        branchView.layer.cornerRadius = 8
        branchView.layer.shadowOpacity = 1
        branchView.layer.shadowRadius = 3.0
        branchView.layer.shadowOffset = CGSize(width: 0, height: 3)
        branchView.layer.shadowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        branchView.layer.borderWidth = 1
        branchView.layer.borderColor = #colorLiteral(red: 0.895331796, green: 0.895331796, blue: 0.895331796, alpha: 1)
        
        startVisitBtn.layer.cornerRadius = 21
        
        let gradient = CAGradientLayer()
        gradient.frame = startVisitBtn.bounds
        gradient.cornerRadius = 21
        gradient.startPoint = CGPoint(x:0.0, y:0.5)
        gradient.endPoint = CGPoint(x:1.0, y:0.5)
        let colorright = UIColor(red: 150.0 / 255.0, green: 0.0 / 255.0, blue: 5.0 / 255.0, alpha: 1.0).cgColor
        let colorleft = UIColor(red: 223.0 / 255.0, green: 18.0 / 255.0, blue: 28.0 / 255.0, alpha: 1.0).cgColor
        gradient.colors = [colorright, colorleft]
        startVisitBtn.layer.addSublayer(gradient)
        
        
        if Reachability.isConnectedToNetwork() {
            sv = UIViewController.displaySpinner(onView: self.view)
            let userId = UserDefaults.standard.integer(forKey: "HajUserID")
            let jsonUrlString = listOrgsEndpoint+String(userId)+"&lang=\(String(langForApi))"
            
            guard let url = URL(string: jsonUrlString) else { return }
            URLSession.shared.dataTask(with: url) { (data, response, err) in
                UIViewController.removeSpinner(spinner: self.sv!)
                if (err != nil){
                    self.view.presentToast(msg: NSLocalizedString("serverErr", comment: "") )
                    return
                }
                if let status = response as? HTTPURLResponse {
                    print("request status \(status.statusCode)")
                    if status.statusCode != 200{
                        self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                        return
                    }
                }
                guard let data = data else { return }
                do {
                    print(data)
                    let response = try JSONDecoder().decode(listorgs.self, from: data)
                    
                    if response.Status == 1 {
                        DispatchQueue.main.async {
                            self.orgsDataArr = response.Organizations
                            
                            if self.orgsDataArr.count > 0 {
                                self.choosenOrgSid = self.orgsDataArr[0].SID
                                self.choosenTemplateSid = self.orgsDataArr[0].FormTemplateSID
                                self.orgbtn.setTitle(self.orgsDataArr[0].Title, for:.normal)
                                self.listBranches()
                            }
                        }
                        
                    }
                    else{
                        DispatchQueue.main.async {
                            self.view.presentToast(msg: response.Message)
                        }
                    }
                }
                    
                catch let jsonErr {
                    print("Error serializing json:", jsonErr)
                    self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                }
            }.resume()
        }
        else{
            self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
        }
        
    }
    
    
    @IBAction func chooseVisitDate(_ sender: UIButton) {
        print("choose date ")
        
    }
    
    @IBAction func dateChanged(_ sender: UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let strDate = dateFormatter.string(from: datepicker.date)
        datebtn.setTitle(strDate, for: .normal)
        visitDate = strDate
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let result = formatter.string(from: date)
        if strDate.compare(result) == .orderedAscending {
            print("First Date is smaller then second date")
            self.view.presentToast(msg: "تاريخ الزياره اقل من الوقت الحالى ")
        }
        
        DispatchQueue.main.async {self.datepicker.isHidden = true}
        
    }
    
    
    @IBAction func startNewVisit(_ sender: UIButton) {
        
        if  choosenOrgSid == 0 {
            self.view.presentToast2(msg: NSLocalizedString("selectOrgErr", comment: ""))
        }
        else if  visitObj.text == "" {
            self.view.presentToast2(msg: NSLocalizedString("perposeerr", comment: ""))
        }else if  userLat == 0 ||  userLong == 0 {
            self.view.presentToast2(msg: NSLocalizedString("locationReq", comment: ""))
        }
        else{
            if Reachability.isConnectedToNetwork() {
                sv = UIViewController.displaySpinner(onView: self.view)
                let jsonUrlString = createVisitEndpoint
                guard let url = URL(string: jsonUrlString) else { return }
                do {
                    
                    let postObject = [
                        "UserID": UserDefaults.standard.integer(forKey: "HajUserID"),
                        "OrgID": choosenOrgSid,
                        "Latitude": userLat,
                        "Longitude": userLong,
                        "VisitTitle": visitObj.text!,
                        "OrgBranchID": choosenbranchSid,
                        "Lang": langForApi,
                        
                        ] as [String : Any]
                    
                    ApiService.callPost(url: url, params: postObject, finish: self.finishaddvisitPost)
                }
            }
            else{
                self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
            }
        }
    }
    
    func finishaddvisitPost (message:String, data:Data?) -> Void
    {
        do
        {
            UIViewController.removeSpinner(spinner: self.sv!)
            if message == "Success" {
                if data != nil
                {
                    let response = try JSONDecoder().decode(createVisitApiRes.self, from: data!)
                    print("register response.Status : \(response.Status)")
                    
                    if response.Status == 1 {
                        
                        DispatchQueue.main.async {
                            if let x = response.VisitID {
                                self.visitId = x
                            }
                            
                            self.view.presentToast(msg: response.Message)
                            self.performSegue(withIdentifier: "visit2", sender: nil)
                        }
                    }
                    else {
                        DispatchQueue.main.async {
                            self.view.presentToast(msg: response.Message)
                        }
                    }
                    
                }
            }
            else{
                self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                print("userregistered for notifications failed")
            }
        }
        catch
        {
            print("userregistered for notifications Parse Error: \(error)")
        }
    }
    
    @IBAction func chooseOrg(_ sender: UIButton) {
        let optionMenu = UIAlertController(title: NSLocalizedString("selectOrg", comment: ""), message: nil, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel,handler: { (action) -> Void in
            print("cancel button tapped")
            
        })
        
        optionMenu.addAction(cancelAction)
        
        for i in orgsDataArr {
            let orgAction = UIAlertAction(title:i.Title, style: .default, handler: { (action) -> Void in
                self.choosenOrgSid = i.SID
                self.listBranches()
                self.choosenTemplateSid = i.FormTemplateSID
                self.orgbtn.setTitle(i.Title, for:.normal)
            })
            
            optionMenu.addAction(orgAction)
        }
        
        optionMenu.popoverPresentationController?.sourceView = self.view;
        optionMenu.popoverPresentationController?.sourceRect = CGRect(x: 0,y: UIScreen.main.bounds.height - 100 ,width: UIScreen.main.bounds.width,height: 1.0);
        
        self.present(optionMenu, animated: true, completion: nil)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "visit2" {
            if let destinationVC = segue.destination as? newVisit2VC {
                print("viistId before sending to another view = \(visitId)")
                destinationVC.visitIdfromaontherview = visitId
                destinationVC.organizationId = choosenOrgSid
                destinationVC.TemplateSid = choosenbranchtemplate
                
            }
        }
    }
    
    @objc func gobackTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        self.userLat = locValue.latitude
        self.userLong = locValue.longitude
        
    }
    
    @IBAction func chooseBranch(_ sender: UIButton) {
        
        if branchesDataArr.count > 0 {
            
            let optionMenu = UIAlertController(title: NSLocalizedString("chooseBranch", comment: ""), message: nil, preferredStyle: .actionSheet)
            
            let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel,handler: { (action) -> Void in
                print("cancel button tapped")
            })
            
            optionMenu.addAction(cancelAction)
            
            for i in branchesDataArr {
                print("i : \(i)")
                let orgAction = UIAlertAction(title:i.Title, style: .default, handler: { (action) -> Void in
                    
                    self.choosenbranchtemplate = i.FormTemplate_SID
                    self.choosenbranchSid = i.SID
                    self.branchBtn.setTitle(i.Title, for:.normal)
                })
                
                optionMenu.addAction(orgAction)
            }
            
            optionMenu.popoverPresentationController?.sourceView = self.view;
            optionMenu.popoverPresentationController?.sourceRect = CGRect(x: 0,y: UIScreen.main.bounds.height - 100 ,width: UIScreen.main.bounds.width,height: 1.0);
            
            self.present(optionMenu, animated: true, completion: nil)
            
        }else{
            self.view.presentToast(msg: NSLocalizedString("nobrancehesfororg", comment: ""))
        }
        
    }
    
    func listBranches(){
        if Reachability.isConnectedToNetwork() {
            sv = UIViewController.displaySpinner(onView: self.view)
            let userId = UserDefaults.standard.integer(forKey: "HajUserID")
            let jsonUrlString = orgbranchListEndpoint+String(userId)+"&org=\(String(choosenOrgSid))"+"&lang=\(String(langForApi))"
            
            guard let url = URL(string: jsonUrlString) else { return }
            URLSession.shared.dataTask(with: url) { (data, response, err) in
                UIViewController.removeSpinner(spinner: self.sv!)
                if (err != nil){
                    self.view.presentToast(msg: NSLocalizedString("serverErr", comment: "") )
                    return
                }
                if let status = response as? HTTPURLResponse {
                    print("request status \(status.statusCode)")
                    if status.statusCode != 200{
                        self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                        return
                    }
                }
                guard let data = data else { return }
                do {
                    print(data)
                    let response = try JSONDecoder().decode(orgbranchList.self, from: data)
                    
                    if response.Status == 1 {
                        DispatchQueue.main.async {
                            self.branchesDataArr = response.OrganizationBranches
                            if self.branchesDataArr.count > 0 {
                                self.choosenbranchSid = self.branchesDataArr[0].SID
                                
                                self.choosenbranchtemplate = self.branchesDataArr[0].FormTemplate_SID
                                self.branchBtn.setTitle(self.branchesDataArr[0].Title, for:.normal)
                            }
                        }
                        
                    }
                    else{
                        DispatchQueue.main.async {
                            self.branchesDataArr = []
                            self.choosenbranchSid = 0
                            self.choosenbranchtemplate = 0
                            self.branchBtn.setTitle(NSLocalizedString("chooseBranch", comment: ""), for:.normal)
                            
                        }
                    }
                }
                    
                catch let jsonErr {
                    print("Error serializing json:", jsonErr)
                    self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                }
            }.resume()
        }
        else{
            self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
        }
        
    }
    
    
}
