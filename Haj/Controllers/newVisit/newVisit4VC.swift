//
//  newVisit4VC.swift
//
//
//
//  Copyright © 2019 ITRoots. All rights reserved.
//

import UIKit

class newVisit4VC: LocalizerViewController ,UITextFieldDelegate{
    
    
    @IBOutlet weak var newView: UIView!
    @IBOutlet weak var savebtn: UIButton!
    @IBOutlet weak var saveBtnTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var stackView: UIStackView!
    
    var organizationId:Int = -1
    var TemplateSid:Int = 0
    var visitId:Int = -1
    var data4:[dynamicDetails] = []
    var from :String = ""
    var from2 :String = ""
    var sv: UIView?
    var topConst:Int = 40
    var topConst2:Int = 80
    var attributeListForValidation:[attrList?] = []
    var attributeListForValidation2:[attrList?] = []
    var textfeildsArr:[UITextField] = []
    var checkboxArr:[UIButton] = []
    @IBOutlet weak var requireView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if L102Language.currentAppleLanguage() == "ar"{
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "leftArrow"), style: .plain, target: self, action: #selector(gobackTapped))
            self.navigationItem.rightBarButtonItem?.tintColor = .white
        }else{
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "leftArrowCopy-1"), style: .plain, target: self, action: #selector(gobackTapped))
            self.navigationItem.rightBarButtonItem?.tintColor = .white
            
        }
        
        
        if L102Language.currentAppleLanguage() == "ar"{
            langForApi =  1
            
        }else{
            langForApi =  2
            
        }
        
        if from2 == "show" {
            savebtn.isHidden = true
        }
        
        if from == "edit_show" && from2 == "show"{
            self.navigationItem.title = NSLocalizedString("visitdetails", comment: "")
        }else   if from == "edit_show" && from2 == ""{
            self.navigationItem.title = NSLocalizedString("editVisit", comment: "")
        }else{
            self.navigationItem.title = NSLocalizedString("newVisit", comment: "")
        }
        
        
        self.navigationItem.hidesBackButton = true
        
        savebtn.layer.cornerRadius = 21
        let gradient = CAGradientLayer()
        gradient.frame = savebtn.bounds
        gradient.cornerRadius = 21
        gradient.startPoint = CGPoint(x:0.0, y:0.5)
        gradient.endPoint = CGPoint(x:1.0, y:0.5)
        let colorright = UIColor(red: 150.0 / 255.0, green: 0.0 / 255.0, blue: 5.0 / 255.0, alpha: 1.0).cgColor
        let colorleft = UIColor(red: 223.0 / 255.0, green: 18.0 / 255.0, blue: 28.0 / 255.0, alpha: 1.0).cgColor
        gradient.colors = [colorright, colorleft]
        savebtn.layer.addSublayer(gradient)
        
        if Reachability.isConnectedToNetwork() {
            sv = UIViewController.displaySpinner(onView: self.view)
            
            let jsonUrlString = listTemplateAttrEndpoint+String(TemplateSid)+"&user="+String(UserDefaults.standard.integer(forKey: "HajUserID"))+"&lang=\(String(langForApi))"
            
            guard let url = URL(string: jsonUrlString) else { return }
            print("url : \(url)")
            URLSession.shared.dataTask(with: url) { (data, response, err) in
                UIViewController.removeSpinner(spinner: self.sv!)
                if (err != nil){
                    self.view.presentToast(msg: NSLocalizedString("serverErr", comment: "") )
                    return
                }
                if let status = response as? HTTPURLResponse {
                    print("request status \(status.statusCode)")
                    if status.statusCode != 200{
                        self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                        return
                    }
                }
                guard let data = data else { return }
                do {
                    print(data)
                    let response = try JSONDecoder().decode(listtemplatearrtRes.self, from: data)
                    print("listtemplatearrtRes response.Status = \(response.Status)")
                    
                    if response.Status == 1 {
                        
                        DispatchQueue.main.async {
                            self.attributeListForValidation = response.AttributesList
                            
                            if response.AttributesList.count > 0 {
                                print("attr list count = \(response.AttributesList.count)")
                                for item in response.AttributesList {
                                    print("att item : \(String(describing: item) )")
                                    
                                    if item?.ControlDataTypeSID == 1{
                                        
                                        let view1 = UIView()
                                        
                                        let label = UILabel()
                                        label.text = item?.Title
                                        label.textColor = .black
                                        label.textAlignment = .right
                                        label.font = UIFont(name: "Cairo SemiBold", size: 15)
                                        label.translatesAutoresizingMaskIntoConstraints = false
                                        view1.addSubview(label)
                                        
                                        let topLabel =  NSLayoutConstraint(item: label, attribute: .top, relatedBy: .equal, toItem: view1, attribute: .top, multiplier: 1.0, constant: 20)
                                        let rightlabel =  NSLayoutConstraint(item: label, attribute: .trailing, relatedBy: .equal, toItem: view1, attribute: .trailing, multiplier: 1.0, constant: -10)
                                        let leftlabel =  NSLayoutConstraint(item: label, attribute: .leading, relatedBy: .equal, toItem: view1, attribute: .leading, multiplier: 1.0, constant: 10)
                                        view1.addConstraints([topLabel,rightlabel,leftlabel])
                                        
                                        let text = UITextField()
                                        text.delegate = self
                                        text.textAlignment = .right
                                        text.backgroundColor = .white
                                        text.textColor = #colorLiteral(red: 0.5490196078, green: 0.5490196078, blue: 0.5490196078, alpha: 1)
                                        text.tag = item!.SID
                                        let textId = item!.SID
                                        if self.from == "edit_show"{
                                            for item in self.data4{
                                                if item.DynamicAttributeSID == textId {
                                                    text.text = item.DynamicAttributeValue
                                                    break
                                                }
                                                
                                            }
                                            
                                        }
                                        
                                        if self.from2 == "show"{
                                            text.isUserInteractionEnabled = false
                                        }
                                        
                                        if item?.TextboxDatatype == 2{
                                            text.keyboardType = .numberPad
                                        }else if  item?.TextboxDatatype == 3{
                                            text.keyboardType = .decimalPad
                                        }
                                        else if  item?.TextboxDatatype == 5{
                                            text.keyboardType = .numberPad
                                        }else if  item?.TextboxDatatype == 4{
                                            
                                            var datepicker:UIDatePicker?
                                            datepicker = UIDatePicker()
                                            datepicker?.datePickerMode = .date
                                            datepicker?.addTarget(self, action: #selector(self.dateChanged(datepicker:)), for: .valueChanged)
                                            let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40))
                                            toolbar.barStyle = .default
                                            toolbar.tintColor = .black
                                            let doneBtn = UIBarButtonItem(title: NSLocalizedString("donetxt", comment: ""), style: .done, target: self, action:#selector(self.doneFunc(done:)))
                                            
                                            let flexibleSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil);
                                            
                                            toolbar.setItems([flexibleSpace,doneBtn], animated: true)
                                            
                                            text.inputAccessoryView = toolbar
                                            text.inputView = datepicker
                                            
                                            
                                        }
                                        
                                        
                                        text.borderStyle = .roundedRect
                                        text.font = UIFont(name: "Cairo SemiBold", size: 15)
                                        text.translatesAutoresizingMaskIntoConstraints=false
                                        self.textfeildsArr.append(text)
                                        
                                        view1.addSubview(text)
                                        
                                        let toptext =  NSLayoutConstraint(item: text, attribute: .top, relatedBy: .equal, toItem: label, attribute: .top, multiplier: 1.0, constant: 30)
                                        let righttext =  NSLayoutConstraint(item: text, attribute: .trailing, relatedBy: .equal, toItem: view1, attribute: .trailing, multiplier: 1.0, constant: -10)
                                        let lefttext =  NSLayoutConstraint(item: text, attribute: .leading, relatedBy: .equal, toItem: view1, attribute: .leading, multiplier: 1.0, constant: 10)
                                        let heighttext = NSLayoutConstraint(item: text, attribute: .height, relatedBy: .equal, toItem: label, attribute: .height, multiplier: 1.0, constant: 30)
                                        view1.addConstraints([toptext,righttext,lefttext,heighttext])
                                        
                                        let  topview1 =  NSLayoutConstraint(item: view1, attribute: .top, relatedBy: .equal, toItem: self.requireView, attribute: .top, multiplier: 1.0, constant: CGFloat(self.topConst))
                                        
                                        
                                        let rightview1 =  NSLayoutConstraint(item: view1, attribute: .trailing, relatedBy: .equal, toItem: self.newView, attribute: .trailing, multiplier: 1.0, constant: 0)
                                        let leftview1 =  NSLayoutConstraint(item: view1, attribute: .leading, relatedBy: .equal, toItem: self.newView, attribute: .leading, multiplier: 1.0, constant: 0)
                                        let heightview1 = NSLayoutConstraint(item: view1, attribute: .height, relatedBy: .equal, toItem: self.newView, attribute: .height, multiplier: 1.0, constant: 40)
                                        view1.translatesAutoresizingMaskIntoConstraints = false
                                        self.newView.addSubview(view1)
                                        self.newView.addConstraints([topview1,rightview1,leftview1,heightview1])
                                        self.topConst += 100
                                        self.topConst2 += 100
                                    } // end of text type
                                    else if item?.ControlDataTypeSID == 2{
                                        print("item?.ControlDataTypeSID == 2")
                                        
                                        let view1 = UIView()
                                        
                                        let btn = UIButton()
                                        btn.setImage(UIImage(named:"roundedRectangle3"), for: .normal)
                                        btn.translatesAutoresizingMaskIntoConstraints = false
                                        
                                        
                                        btn.tag = item!.SID
                                        btn.setTitle("0", for: .normal)
                                        btn.setTitleColor(.white, for: .normal)
                                        
                                        
                                        let btnId = item!.SID
                                        if self.from == "edit_show"{
                                            for item in self.data4{
                                                if item.DynamicAttributeSID == btnId {
                                                    if item.DynamicAttributeValue == "0"{
                                                        btn.setImage(UIImage(named:"roundedRectangle3"), for: .normal)
                                                        btn.setTitle("0", for: .normal)
                                                        
                                                    }else if item.DynamicAttributeValue == "1"{
                                                        
                                                        btn.setTitle("1", for: .normal)
                                                        btn.setImage(UIImage(named: "vectorSmartObject-2"), for: .normal)
                                                    }
                                                    break
                                                }
                                                
                                            }
                                            
                                        }
                                        
                                        if self.from2 == "show"{
                                            btn.isUserInteractionEnabled = false
                                        }
                                        
                                        
                                        btn.addTarget(self, action: #selector(self.changeimg(_:)), for: .touchUpInside)
                                        self.checkboxArr.append(btn)
                                        view1.addSubview(btn)
                                        
                                        let topbtn =  NSLayoutConstraint(item: btn, attribute: .top, relatedBy: .equal, toItem: view1, attribute: .top, multiplier: 1.0, constant: 30)
                                        
                                        let rightbtn =  NSLayoutConstraint(item: btn, attribute: .trailing, relatedBy: .equal, toItem: view1, attribute: .trailing, multiplier: 1.0, constant: -20)
                                        
                                        let heightbtn = NSLayoutConstraint(item: btn, attribute: .height, relatedBy: .equal, toItem: btn, attribute: .height, multiplier: 1.0, constant: 80)
                                        let widthbtn = NSLayoutConstraint(item: btn, attribute: .width, relatedBy: .equal, toItem: btn, attribute: .width, multiplier: 1.0, constant: 80)
                                        view1.addConstraints([topbtn,heightbtn,widthbtn,rightbtn])
                                        
                                        
                                        let label = UILabel()
                                        label.text = item?.Title
                                        label.textColor = .black
                                        label.textAlignment = .right
                                        label.font = UIFont(name: "Cairo SemiBold", size: 15)
                                        label.translatesAutoresizingMaskIntoConstraints = false
                                        
                                        view1.addSubview(label)
                                        
                                        let topLabel =  NSLayoutConstraint(item: label, attribute: .top, relatedBy: .equal, toItem: view1, attribute: .top, multiplier: 1.0, constant: 25)
                                        let rightlabel =  NSLayoutConstraint(item: label, attribute: .trailing, relatedBy: .equal, toItem: view1, attribute: .trailing, multiplier: 1.0, constant: -50)
                                        
                                        view1.addConstraints([topLabel,rightlabel])
                                        
                                        let  topview1 =  NSLayoutConstraint(item: view1, attribute: .top, relatedBy: .equal, toItem: self.requireView, attribute: .top, multiplier: 1.0, constant: CGFloat(self.topConst))
                                        
                                        let rightview1 =  NSLayoutConstraint(item: view1, attribute: .trailing, relatedBy: .equal, toItem: self.newView, attribute: .trailing, multiplier: 1.0, constant: 0)
                                        let leftview1 =  NSLayoutConstraint(item: view1, attribute: .leading, relatedBy: .equal, toItem: self.newView, attribute: .leading, multiplier: 1.0, constant: 0)
                                        
                                        let heightview1 = NSLayoutConstraint(item: view1, attribute: .height, relatedBy: .equal, toItem: self.newView, attribute: .height, multiplier: 1.0, constant: 20)
                                        view1.translatesAutoresizingMaskIntoConstraints = false
                                        self.newView.addSubview(view1)
                                        self.newView.addConstraints([topview1,rightview1,leftview1,heightview1])
                                        self.topConst += 50
                                        self.topConst2 += 100
                                        
                                    }
                                    
                                }
                                print("draw save btn topconst : \(self.topConst) ")
                                
                                self.loopThroughSubViewAndAlignLabelTextView(subviews: self.view.subviews)
                                
                                
                            }
                        }
                        
                        
                    }
                    else{
                        DispatchQueue.main.async {
                            self.view.presentToast2(msg: response.Message)
                            self.savebtn.isHidden = true
                        }
                    }
                }
                    
                catch let jsonErr {
                    print("Error serializing json:", jsonErr)
                    self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                }
            }.resume()
        }
        else{
            self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
        }
        
        
        self.savebtn.bringSubviewToFront(self.newView)
        
    }
    
    
    
    @IBAction func goback(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("textField.sid : \(textField.tag)")
        var  maxLength = 0
        for item in self.attributeListForValidation {
            if item?.SID == textField.tag {
                maxLength = item!.Length
            }
        }
        print("max length =  \(maxLength) , text sid = \(textField.tag)")
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    
    func finishaddReceivedData (message:String, data:Data?) -> Void
    {
        do
        {
            UIViewController.removeSpinner(spinner: self.sv!)
            if message == "Success" {
                if data != nil
                {
                    let response = try JSONDecoder().decode(visitAddReceievedDataRes.self, from: data!)
                    print("register response.Status : \(response.Status)")
                    
                    if response.Status == 1 {
                        
                        DispatchQueue.main.async {
                            self.view.presentToast2(msg: response.Message)
                            
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let mainViewController = mainStoryBoard.instantiateViewController(withIdentifier: "mainViewController") as! UITabBarController
                            mainViewController.selectedIndex = 3
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate?
                            appDelegate!.window?.rootViewController = mainViewController
                        }
                        
                    }
                    else {
                        DispatchQueue.main.async {
                            self.view.presentToast2(msg: response.Message)
                        }
                    }
                    
                }
            }
            else{
                self.view.presentToast(msg:NSLocalizedString("serverErr", comment: "") )
                print("userregistered for notifications failed")
            }
        }
        catch
        {
            print("userregistered for notifications Parse Error: \(error)")
        }
    }
    
    
    
    @objc func doneFunc(done:UIBarButtonItem){
        
    }
    
    @objc func dateChanged(datepicker:UIDatePicker) {
        print("datechanged")
        
    }
    
    
    @objc func changeimg(_ btn: UIButton){
        
        
        
        DispatchQueue.main.async {
            
            for item in self.attributeListForValidation {
                if item!.SID == btn.tag {
                    
                    if btn.title(for: .normal) == "0"{
                        btn.setTitle("1", for: .normal)
                        btn.setImage(UIImage(named: "vectorSmartObject-2"), for: .normal)
                        
                    }else  if btn.title(for: .normal) == "1"{
                        btn.setTitle("0", for: .normal)
                        btn.setImage(UIImage(named:"roundedRectangle3"), for: .normal)
                    }
                    
                    
                    
                    
                }
                
            }
            
            
        }
    }
    
    @objc func gobackTapped() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func xsave(_ sender: UIButton) {
        
        var arrData: Array<saveattrObj2> = []
        var errorFlag = false
        
        for (i,_) in textfeildsArr.enumerated(){
            
            
            if attributeListForValidation[i]?.ControlDataTypeSID == 1{
                if attributeListForValidation[i]?.SID == textfeildsArr[i].tag {
                    if textfeildsArr[i].text == "" && attributeListForValidation[i]?.Optional == false {
                        self.view.presentToast2(msg:  NSLocalizedString("plsenter", comment: "")+(attributeListForValidation[i]?.Title)!)
                        errorFlag = true
                        break
                    }
                }
            }
            
        }
        
        for (i,_) in checkboxArr.enumerated(){
            if attributeListForValidation[i]?.ControlDataTypeSID == 2{
                if attributeListForValidation[i]?.SID == checkboxArr[i].tag {
                    if checkboxArr[i].title(for: .normal) == "0" && attributeListForValidation[i]?.Optional == false {
                        self.view.presentToast2(msg:  NSLocalizedString("plsselect", comment: "")+(attributeListForValidation[i]?.Title)!)
                        errorFlag = true
                        break
                    }
                }
                
                
            }
            
        }
        
        
        if errorFlag == false {
            print("call api visit4 ")
            
            
            
            for (i,_) in checkboxArr.enumerated(){
                
                
                let tempobj:saveattrObj2 = saveattrObj2(UserID: UserDefaults.standard.integer(forKey: "HajUserID"), VisitID: visitId, AttributeID: checkboxArr[i].tag , AttributeValue:  checkboxArr[i].title(for: .normal)!)
                print("temp obj \(tempobj)")
                arrData.append(tempobj)
                
                
            }
            
            
            for (i,_) in textfeildsArr.enumerated(){
                
                let tempobj:saveattrObj2 = saveattrObj2(UserID: UserDefaults.standard.integer(forKey: "HajUserID"), VisitID: visitId, AttributeID: textfeildsArr[i].tag , AttributeValue:  textfeildsArr[i].text!)
                print("temp obj2 \(tempobj)")
                arrData.append(tempobj)
                
                
                
            }
            
            if Reachability.isConnectedToNetwork() {
                sv = UIViewController.displaySpinner(onView: self.view)
                let jsonUrlString = saveVisitAtrrEndpoint
                guard let url = URL(string: jsonUrlString) else { return }
                do {
                    
                    var familyMembers : [Any] = []
                    for item in arrData {
                        print("item : \(item)")
                        familyMembers.append(item.toJSON())
                    }
                    
                    let postObject = familyMembers
                    
                    
                    ApiService.callPost2(url: url, params: postObject, finish: self.finishaddReceivedData)
                    
                    
                }
            }
            else{
                self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
            }
            
        }
        
        
    }
    
    @IBAction func save222(_ sender: UIButton) {
        print("save222")
    }
    
    
}
