//
//  newVisit2VC.swift
//
//
//
//  Copyright © 2019 ITRoots. All rights reserved.
//

import UIKit

class newVisit2VC: LocalizerViewController {
    
    var visitIdfromaontherview:Int = -1
    var organizationId:Int = -1
    var TemplateSid:Int = 0
    var ContactOfficerPhone:String? = ""
    var orgLat:Double = 0
    var orgLng:Double = 0
    var sv: UIView?
    var visit3data:RecevedDetils = RecevedDetils(SID: -1, ReceivedDataFilesID: "", ReceivedDataDescription: "", SpatialAccuracy: "", DataCreationDate: "", DataLastUpdatedDate: "")
    var visit4data:[dynamicDetails] = []
    var visitIdToeditORShow:Int = -1
    var from:String = ""
    var from2:String = ""
    
    @IBOutlet weak var orgname: UITextField!
    @IBOutlet weak var visitID: UITextField!
    @IBOutlet weak var orgDirector: UITextField!
    @IBOutlet weak var orgContact: UITextField!
    @IBOutlet weak var orgAddress: UITextField!
    @IBOutlet weak var allContacts: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if L102Language.currentAppleLanguage() == "ar"{
            
            langForApi =  1
            
        }else{
            langForApi =  2
            
        }
        
        
        if from == "edit_show" && from2 == "show"{
            self.navigationItem.title = NSLocalizedString("visitdetails", comment: "")
        }else   if from == "edit_show" && from2 == ""{
            self.navigationItem.title = NSLocalizedString("editVisit", comment: "")
        }else{
            self.navigationItem.title = NSLocalizedString("newVisit", comment: "")
        }
        
        self.navigationItem.hidesBackButton = true
        
        if L102Language.currentAppleLanguage() == "ar"{
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "leftArrow"), style: .plain, target: self, action: #selector(gobackTapped))
            self.navigationItem.rightBarButtonItem?.tintColor = .white
        }else{
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "leftArrowCopy-1"), style: .plain, target: self, action: #selector(gobackTapped))
            self.navigationItem.rightBarButtonItem?.tintColor = .white
            
        }
        
        
        allContacts.layer.cornerRadius = 8
        
        saveBtn.layer.cornerRadius = 21
        let gradient = CAGradientLayer()
        gradient.frame = saveBtn.bounds
        gradient.cornerRadius = 21
        gradient.startPoint = CGPoint(x:0.0, y:0.5)
        gradient.endPoint = CGPoint(x:1.0, y:0.5)
        let colorright = UIColor(red: 150.0 / 255.0, green: 0.0 / 255.0, blue: 5.0 / 255.0, alpha: 1.0).cgColor
        let colorleft = UIColor(red: 223.0 / 255.0, green: 18.0 / 255.0, blue: 28.0 / 255.0, alpha: 1.0).cgColor
        gradient.colors = [colorright, colorleft]
        saveBtn.layer.addSublayer(gradient)
        
        nextBtn.layer.cornerRadius = 21
        let gradient1 = CAGradientLayer()
        gradient1.frame = nextBtn.bounds
        gradient1.cornerRadius = 21
        gradient1.startPoint = CGPoint(x:0.0, y:0.5)
        gradient1.endPoint = CGPoint(x:1.0, y:0.5)
        let colorright1 = UIColor(red: 169.0 / 255.0, green: 125.0 / 255.0, blue: 52.0 / 255.0, alpha: 1.0).cgColor
        let colorleft1 = UIColor(red: 207.0 / 255.0, green: 161.0 / 255.0, blue: 85.0 / 255.0, alpha: 1.0).cgColor
        gradient1.colors = [colorright1, colorleft1]
        nextBtn.layer.addSublayer(gradient1)
        
        if from2 == "show" {
            orgAddress.isUserInteractionEnabled = false
            orgDirector.isUserInteractionEnabled = false
            saveBtn.isHidden = true
            
        }
        if from == "edit_show" {
            
            visitIdfromaontherview = visitIdToeditORShow
            if Reachability.isConnectedToNetwork() {
                sv = UIViewController.displaySpinner(onView: self.view)
                
                let jsonUrlString = getVisitDetailsEndpoint + String(visitIdToeditORShow)+"&user="+String(UserDefaults.standard.integer(forKey: "HajUserID"))+"&lang=\(String(langForApi))"
                
                guard let url = URL(string: jsonUrlString) else { return }
                print("url : \(url)")
                URLSession.shared.dataTask(with: url) { (data, response, err) in
                    UIViewController.removeSpinner(spinner: self.sv!)
                    if (err != nil){
                        self.view.presentToast(msg: NSLocalizedString("serverErr", comment: "") )
                        return
                    }
                    if let status = response as? HTTPURLResponse {
                        print("request status \(status.statusCode)")
                        if status.statusCode != 200{
                            self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                            return
                        }
                    }
                    guard let data = data else { return }
                    do {
                        print(data)
                        let response = try JSONDecoder().decode(getvisitDetailsRes.self, from: data)
                        print("orgdetils response.Status = \(response.Status)")
                        
                        if response.Status == 1 {
                            
                            
                            DispatchQueue.main.async {
                                if let x = response.ReceivedDataDetails {
                                    self.visit3data = x
                                }
                                
                                if let y = response.DynamicAttributes{
                                    self.visit4data = y
                                }
                                
                                self.organizationId = response.OrgDetails.SID
                                self.orgname.text = response.OrgDetails.Title
                                self.orgDirector.text = response.OrgDetails.ManagerName
                                self.orgContact.text = response.OrgDetails.ContactOfficer
                                self.orgAddress.text = response.OrgDetails.OrgAddress
                                self.visitID.text = String(response.VisitDetails.VisitNumber)
                                if let xx = response.OrgDetails.ContactOfficerPhone{
                                    self.ContactOfficerPhone = xx
                                }
                                
                                self.orgLat = response.OrgDetails.Lat
                                self.orgLng = response.OrgDetails.Long
                                self.TemplateSid = response.VisitDetails.FormTempateSID
                                
                            }
                            
                        }
                        else{
                            self.view.presentToast2(msg: response.Message)
                        }
                    }
                        
                    catch let jsonErr {
                        print("Error serializing json:", jsonErr)
                        self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                    }
                }.resume()
            }
            else{
                self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
            }
            
        }else {
            print("from new " )
            getvisitNumber()
            
            if Reachability.isConnectedToNetwork() {
                sv = UIViewController.displaySpinner(onView: self.view)
                
                let jsonUrlString = getOrgDetailsByIdEndpoint+String(UserDefaults.standard.integer(forKey: "HajUserID"))+"&org="+String(organizationId)+"&lang=\(String(langForApi))"
                
                guard let url = URL(string: jsonUrlString) else { return }
                
                URLSession.shared.dataTask(with: url) { (data, response, err) in
                    UIViewController.removeSpinner(spinner: self.sv!)
                    if (err != nil){
                        self.view.presentToast(msg: NSLocalizedString("serverErr", comment: "") )
                        return
                    }
                    if let status = response as? HTTPURLResponse {
                        print("request status \(status.statusCode)")
                        if status.statusCode != 200{
                            self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                            return
                        }
                    }
                    guard let data = data else { return }
                    do {
                        print(data)
                        let response = try JSONDecoder().decode(getOrgDeatils.self, from: data)
                        print("orgdetils response.Status = \(response.Status)")
                        
                        if response.Status == 1 {
                            
                            
                            DispatchQueue.main.async {
                                self.orgname.text = response.Details.Title
                                self.orgDirector.text = response.Details.ManagerName
                                self.orgContact.text = response.Details.ContactOfficer
                                self.orgAddress.text = response.Details.OrgAddress
                                if let xx = response.Details.ContactOfficerPhone{
                                    self.ContactOfficerPhone = xx
                                }
                                
                                self.orgLat = response.Details.Lat
                                self.orgLng = response.Details.Long
                                
                            }
                            
                        }
                        else{
                            self.view.presentToast2(msg: response.Message)
                        }
                    }
                        
                    catch let jsonErr {
                        print("Error serializing json:", jsonErr)
                        self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                    }
                }.resume()
            }
            else{
                self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
            }
            
            
        }
        
    }
    
    
    @IBAction func seeAllContacts(_ sender: Any) {
        self.performSegue(withIdentifier: "allContactSegue", sender: nil)
    }
    
    @IBAction func saveData(_ sender: UIButton) {
        
        if  orgDirector.text == "" {
            self.view.presentToast2(msg: NSLocalizedString("enterorgmanager", comment: ""))
        }else if  orgAddress.text == "" {
            self.view.presentToast2(msg: NSLocalizedString("Enteraddress", comment: ""))
        }else{
            
            if Reachability.isConnectedToNetwork() {
                sv = UIViewController.displaySpinner(onView: self.view)
                let jsonUrlString = updateOrgDatatEndpoint
                guard let url = URL(string: jsonUrlString) else { return }
                do {
                    
                    let postObject = [
                        "UserID": UserDefaults.standard.integer(forKey: "HajUserID"),
                        "OrgID": organizationId,
                        "OrgLat": orgLat,
                        "OrgLong": orgLng,
                        "OrgAddress": orgAddress.text!,
                        "ContactOfficer": orgContact.text!,
                        "OfficerPhone": ContactOfficerPhone!,
                        "ManagerName": orgDirector.text!,
                        "Lang": langForApi
                        
                        ] as [String : Any]
                    
                    ApiService.callPost(url: url, params: postObject, finish: self.finishsaveOrgDataPost)
                }
            }
            else{
                self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
            }
            
        }
        
    }
    
    func finishsaveOrgDataPost (message:String, data:Data?) -> Void
    {
        do
        {
            UIViewController.removeSpinner(spinner: self.sv!)
            if message == "Success" {
                if data != nil
                {
                    let response = try JSONDecoder().decode(updateOrgRes.self, from: data!)
                    print("register response.Status : \(response.Status)")
                    
                    if response.Status == 1 {
                        
                        DispatchQueue.main.async {
                            self.view.presentToast2(msg: response.Message)
                            
                        }
                        
                    }
                    else {
                        self.view.presentToast2(msg: response.Message)
                    }
                    
                }
            }
            else{
                self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                print("userregistered for notifications failed")
            }
        }
        catch
        {
            print("userregistered for notifications Parse Error: \(error)")
        }
    }
    
    @IBAction func nextView(_ sender: UIButton) {
        self.performSegue(withIdentifier: "visit3", sender: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "allContactSegue" {
            if let destinationVC = segue.destination as? allContactsTableViewController {
                
                destinationVC.organizationId = organizationId
            }
        }else if segue.identifier == "visit3" {
            if let destinationVC = segue.destination as? newVisit3VC {
                
                destinationVC.organizationId = organizationId
                destinationVC.visitId = visitIdfromaontherview
                destinationVC.from = from
                destinationVC.from2 = from2
                destinationVC.data = visit3data
                destinationVC.data4 =  visit4data
                destinationVC.TemplateSid =  TemplateSid
                
            }
        }
    }
    
    @IBAction func goback(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func gobackTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func  getvisitNumber(){
        
        if Reachability.isConnectedToNetwork() {
            
            let jsonUrlString = getVisitDetailsEndpoint + String(visitIdfromaontherview)+"&user="+String(UserDefaults.standard.integer(forKey: "HajUserID"))+"&lang=\(String(langForApi))"
            
            guard let url = URL(string: jsonUrlString) else { return }
            print("url : \(url)")
            URLSession.shared.dataTask(with: url) { (data, response, err) in
                
                if (err != nil){
                    self.view.presentToast(msg: NSLocalizedString("serverErr", comment: "") )
                    return
                }
                if let status = response as? HTTPURLResponse {
                    print("request status \(status.statusCode)")
                    if status.statusCode != 200{
                        self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                        return
                    }
                }
                guard let data = data else { return }
                do {
                    print(data)
                    let response = try JSONDecoder().decode(getvisitDetailsRes.self, from: data)
                    print("orgdetils response.Status = \(response.Status)")
                    
                    if response.Status == 1 {
                        
                        DispatchQueue.main.async {
                            
                            self.visitID.text = String(response.VisitDetails.VisitNumber)
                            
                        }
                        
                    }
                    else{
                        print("response.Message from getvisitnumber : \(response.Message)")
                        
                    }
                }
                    
                catch let jsonErr {
                    print("Error serializing json:", jsonErr)
                    self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                }
            }.resume()
        }
        else{
            self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
        }
    }
    
}
