//
//  allContactsTableViewController.swift
//
//
//
//  Copyright © 2019 ITRoots. All rights reserved.
//

import UIKit

class allContactsTableViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    var fbtn = UIButton()
    var fbtn2 = UIButton()
    var refreshControl = UIRefreshControl()
    var addOrEdit:String = ""
    var label:UILabel = UILabel()
    @IBOutlet var contactsTableView: UITableView!
    var sv: UIView?
    var organizationId:Int = -1
    var contactsData:[contactPerson]=[]
    var editperson:contactPerson = contactPerson(SID: -1, PersonName: "", PhoneNumber: "", PositionName: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if L102Language.currentAppleLanguage() == "ar"{
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "leftArrow"), style: .plain, target: self, action: #selector(gobackTapped))
            self.navigationItem.rightBarButtonItem?.tintColor = .white
        }else{
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "leftArrowCopy-1"), style: .plain, target: self, action: #selector(gobackTapped))
            self.navigationItem.rightBarButtonItem?.tintColor = .white
            
        }
        
        
        if L102Language.currentAppleLanguage() == "ar"{
            floatingButton()
            
            langForApi =  1
            
        }else{
            langForApi =  2
            floatingButtonen()
        }
        
        label =  UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
        
        refreshControl.addTarget(self, action: #selector(loadVisits), for: UIControl.Event.valueChanged)
        contactsTableView.addSubview(refreshControl)
        
        self.navigationItem.title = NSLocalizedString("contacts", comment: "")
        self.navigationItem.hidesBackButton = true
        contactsTableView.delegate = self
        contactsTableView.dataSource = self
        contactsTableView.tableFooterView = UIView()
        
        if Reachability.isConnectedToNetwork() {
            sv = UIViewController.displaySpinner(onView: self.view)
            let userId = UserDefaults.standard.integer(forKey: "HajUserID")
            let jsonUrlString = orgContactPersonListEndpoint+String(userId)+"&org="+String(organizationId)+"&lang=\(String(langForApi))"
            
            guard let url = URL(string: jsonUrlString) else { return }
            URLSession.shared.dataTask(with: url) { (data, response, err) in
                UIViewController.removeSpinner(spinner: self.sv!)
                if (err != nil){
                    self.view.presentToast(msg: NSLocalizedString("serverErr", comment: "") )
                    return
                }
                if let status = response as? HTTPURLResponse {
                    print("request status \(status.statusCode)")
                    if status.statusCode != 200{
                        self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                        return
                    }
                }
                guard let data = data else { return }
                do {
                    print(data)
                    let response = try JSONDecoder().decode(listOrgContacts.self, from: data)
                    
                    DispatchQueue.main.async {
                        
                        self.label.removeFromSuperview()
                        
                    }
                    if response.Status == 1 {
                        self.contactsData = response.OrgContactPerson
                        if self.contactsData.count == 0 {
                            DispatchQueue.main.async {
                                
                                self.label.center = CGPoint(x: 160, y: 285)
                                self.label.textAlignment = .center
                                self.label.center.x = self.view.center.x
                                self.label.font = UIFont(name: "Cairo SemiBold", size: 16)
                                self.label.textColor = #colorLiteral(red: 0.4476690292, green: 0.4476690292, blue: 0.4476690292, alpha: 1)
                                self.label.text = response.Message
                                self.view.addSubview(self.label)
                                
                            }
                        }
                        else{
                            DispatchQueue.main.async {
                                
                                self.contactsTableView.reloadData()
                            }
                        }
                    }
                    else{
                        
                        DispatchQueue.main.async {
                            
                            self.label.center = CGPoint(x: 160, y: 285)
                            self.label.tag = 1
                            
                            self.label.textAlignment = .center
                            
                            self.label.center.x = self.view.center.x
                            
                            self.label.font = UIFont(name: "Cairo SemiBold", size: 16)
                            self.label.textColor = #colorLiteral(red: 0.4476690292, green: 0.4476690292, blue: 0.4476690292, alpha: 1)
                            self.label.text = response.Message
                            self.view.addSubview(self.label)
                            
                        }
                        
                        
                    }
                }
                    
                catch let jsonErr {
                    print("Error serializing json:", jsonErr)
                    self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                }
            }.resume()
        }
        else{
            self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
        }
        
        
        
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return contactsData.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "contctCell", for: indexPath) as! contactTableViewCell
        let item = self.contactsData[indexPath.row]
        cell.name.text = item.PersonName
        cell.job.text = item.PositionName
        var phone = item.PhoneNumber
        let first3 = String(phone.prefix(3))
        
        if first3 == "966" {
            for _ in 0..<3{
                phone.removeFirst()
            }
        }
        let first4 = String(phone.prefix(4))
        
        if first4 == "+966" {
            for _ in 0..<4{
                phone.removeFirst()
            }
            
        }
        
        
        cell.phone.text = phone
        cell.editContact.tag = item.SID
        cell.editContact.addTarget(self, action: #selector(editcontact(_:)), for: .touchUpInside)
        cell.deleteContact.tag = item.SID
        cell.deleteContact.addTarget(self, action: #selector(removecontact(_:)), for: .touchUpInside)
        
        return cell
    }
    
    
    @objc func editcontact(_ btn: UIButton){
        
        editperson.SID = btn.tag
        for item in contactsData {
            if item.SID == btn.tag {
                editperson.PersonName =  item.PersonName
                editperson.PhoneNumber = item.PhoneNumber
                editperson.PositionName = item.PositionName
                addOrEdit = "editContact"
                self.performSegue(withIdentifier: "editContact", sender: nil)
                
            }
        }
        
    }
    
    @objc func removecontact(_ btn: UIButton){
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        
        let paragraphStyle = NSMutableParagraphStyle()
        
        if L102Language.currentAppleLanguage() == "ar"{
            
            paragraphStyle.alignment = .right
        }else{
            paragraphStyle.alignment = .left
            
        }
        
        
        let messageText = NSMutableAttributedString(
            string: NSLocalizedString("confirmdeletemsg", comment: ""),
            attributes: [
                NSAttributedString.Key.paragraphStyle: paragraphStyle,
                NSAttributedString.Key.font : UIFont.preferredFont(forTextStyle: .body),
                NSAttributedString.Key.foregroundColor : UIColor.black
            ]
        )
        
        alert.setValue(messageText, forKey: "attributedMessage")
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("delete", comment: ""), style: .default, handler: { (action) -> Void in
            
            
            if Reachability.isConnectedToNetwork() {
                self.sv = UIViewController.displaySpinner(onView: self.view)
                let userId = UserDefaults.standard.integer(forKey: "HajUserID")
                
                let jsonUrlString = removeOrgContactEndpoint+String(userId)+"&person="+String(btn.tag)+"&lang=\(String(langForApi))"
                
                guard let url = URL(string: jsonUrlString) else { return }
                URLSession.shared.dataTask(with: url) { (data, response, err) in
                    UIViewController.removeSpinner(spinner: self.sv!)
                    if (err != nil){
                        self.view.presentToast(msg: NSLocalizedString("serverErr", comment: "") )
                        return
                    }
                    if let status = response as? HTTPURLResponse {
                        print("request status \(status.statusCode)")
                        if status.statusCode != 200{
                            self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                            return
                        }
                    }
                    guard let data = data else { return }
                    do {
                        print(data)
                        let response = try JSONDecoder().decode(insertRes.self, from: data)
                        
                        
                        if response.Status == 1 {
                            
                            DispatchQueue.main.async {
                                self.view.presentToast(msg: response.Message)
                                
                                self.loadVisits()
                                
                                
                            }
                            
                            
                        }
                        else{
                            DispatchQueue.main.async {
                                self.view.presentToast(msg: response.Message)
                            }
                            
                        }
                    }
                        
                    catch let jsonErr {
                        print("Error serializing json:", jsonErr)
                        self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                    }
                }.resume()
            }
            else{
                self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
            }
            
            
            
            
            
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .default, handler: { (action) -> Void in
            
            
        }))
        
        self.present(alert, animated: true)
        
        
    }
    
    
    func floatingButton(){
        
        let height = UIScreen.main.bounds.height
        fbtn.frame = CGRect(x: 30, y:height-280 , width: 100, height: 100)
        fbtn.setBackgroundImage(UIImage(named: "floatingButton"), for: .normal)
        fbtn.clipsToBounds = true
        fbtn.layer.cornerRadius = 50
        fbtn.addTarget(self,action: #selector(newContact), for: .touchUpInside)
        contactsTableView.addSubview(fbtn)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let  off = self.contactsTableView.contentOffset.y
        let height = UIScreen.main.bounds.height
        fbtn.frame = CGRect(x: 30, y:   off + (height-280), width: fbtn.frame.size.width, height: fbtn.frame.size.height)
        let width  = UIScreen.main.bounds.width
        fbtn2.frame = CGRect(x: width-130, y:   off + (height-280), width: fbtn2.frame.size.width, height: fbtn2.frame.size.height)
    }
    
    func floatingButtonen(){
        
        let width  = UIScreen.main.bounds.width
        let height = UIScreen.main.bounds.height
        fbtn2.frame = CGRect(x: width-130 , y:height-280 , width: 100, height: 100)
        fbtn2.setBackgroundImage(UIImage(named: "floatingButton"), for: .normal)
        fbtn2.clipsToBounds = true
        fbtn2.layer.cornerRadius = 50
        fbtn2.addTarget(self,action: #selector(newContact), for: .touchUpInside)
        contactsTableView.addSubview(fbtn2)
    }
    @objc func newContact(){
        addOrEdit = "addContact"
        self.performSegue(withIdentifier: "editContact", sender: nil)
    }
    
    @objc func loadVisits() {
        
        if Reachability.isConnectedToNetwork() {
            sv = UIViewController.displaySpinner(onView: self.view)
            let userId = UserDefaults.standard.integer(forKey: "HajUserID")
            let jsonUrlString = orgContactPersonListEndpoint+String(userId)+"&org="+String(organizationId)+"&lang=\(String(langForApi))"
            
            guard let url = URL(string: jsonUrlString) else { return }
            URLSession.shared.dataTask(with: url) { (data, response, err) in
                UIViewController.removeSpinner(spinner: self.sv!)
                if (err != nil){
                    self.view.presentToast(msg: NSLocalizedString("serverErr", comment: "") )
                    return
                }
                if let status = response as? HTTPURLResponse {
                    print("request status \(status.statusCode)")
                    if status.statusCode != 200{
                        self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                        return
                    }
                }
                guard let data = data else { return }
                do {
                    print(data)
                    let response = try JSONDecoder().decode(listOrgContacts.self, from: data)
                    
                    DispatchQueue.main.async {
                        
                        self.refreshControl.endRefreshing()
                        self.label.removeFromSuperview()
                        self.contactsData = []
                        self.contactsTableView.reloadData()
                        
                    }
                    if response.Status == 1 {
                        self.contactsData = response.OrgContactPerson
                        if self.contactsData.count == 0 {
                            DispatchQueue.main.async {
                                
                                self.label.textAlignment = .center
                                
                                self.label.center.x = self.view.center.x
                                
                                self.label.font = UIFont(name: "Cairo SemiBold", size: 16)
                                self.label.textColor = #colorLiteral(red: 0.4476690292, green: 0.4476690292, blue: 0.4476690292, alpha: 1)
                                self.label.text = response.Message
                                self.view.addSubview(self.label)
                                
                                
                            }
                        }
                        else{
                            DispatchQueue.main.async {
                                self.contactsData = response.OrgContactPerson
                                
                                self.contactsTableView.reloadData()
                            }
                        }
                    }
                    else{
                        
                        DispatchQueue.main.async {
                            
                            self.label.center = CGPoint(x: 160, y: 285)
                            self.label.textAlignment = .center
                            
                            self.label.center.x = self.view.center.x
                            
                            self.label.font = UIFont(name: "Cairo SemiBold", size: 16)
                            self.label.textColor = #colorLiteral(red: 0.4476690292, green: 0.4476690292, blue: 0.4476690292, alpha: 1)
                            self.label.text = response.Message
                            self.view.addSubview(self.label)
                        }
                        
                    }
                }
                    
                catch let jsonErr {
                    print("Error serializing json:", jsonErr)
                    DispatchQueue.main.async {
                        self.refreshControl.endRefreshing()
                        self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                    }
                }
            }.resume()
        }
        else{
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
                self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
            }
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "editContact" {
            if let destinationVC = segue.destination as? addContactViewController {
                destinationVC.orgId = organizationId
                destinationVC.from = addOrEdit
                destinationVC.editObj = editperson
                
            }
            
        }
    }
    
    
    @IBAction func goback(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func unwindToallContact(segue: UIStoryboardSegue) {
        self.loadVisits()
    }
    
    @objc func gobackTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
}
