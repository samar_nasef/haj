//
//  addContactViewController.swift
//
//
//
//  Copyright © 2019 ITRoots. All rights reserved.
//

import UIKit

class addContactViewController: LocalizerViewController ,UITextFieldDelegate{
    
    @IBOutlet weak var nametext: UITextField!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var phoneText: UITextField!
    @IBOutlet weak var jobText: UITextField!
    @IBOutlet weak var jobView: UIView!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var mainView: UIView!
    
    var sv: UIView?
    var from:String = ""
    var orgId:Int = -1
    var editObj:contactPerson = contactPerson(SID: -1, PersonName: "", PhoneNumber: "", PositionName: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        phoneText.delegate = self
        
        if L102Language.currentAppleLanguage() == "ar"{
            langForApi =  1
            
        }else{
            langForApi =  2
            
        }
        
        self.navigationItem.hidesBackButton = true
        
        if from == "editContact" {
            
            nametext.text = editObj.PersonName
            jobText.text = editObj.PositionName
            
            let first3 = String(editObj.PhoneNumber.prefix(3))
            
            if first3 == "966" {
                for _ in 0..<3{
                    editObj.PhoneNumber.removeFirst()
                }
            }
            let first4 = String(editObj.PhoneNumber.prefix(4))
            
            if first4 == "+966" {
                for _ in 0..<4{
                    editObj.PhoneNumber.removeFirst()
                }
            }
            
            phoneText.text = editObj.PhoneNumber
            saveBtn.setTitle(NSLocalizedString("update", comment: ""), for: .normal)
        }else{
            print("addContact from if ")
        }
        nametext.becomeFirstResponder()
        
        mainView.layer.cornerRadius = 10
        
        nameView.layer.cornerRadius = 8
        nameView.layer.shadowOpacity = 1
        nameView.layer.shadowRadius = 3.0
        nameView.layer.shadowOffset = CGSize(width: 0, height: 3)
        nameView.layer.shadowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        nameView.layer.borderWidth = 1
        nameView.layer.borderColor = #colorLiteral(red: 0.895331796, green: 0.895331796, blue: 0.895331796, alpha: 1)
        
        jobView.layer.cornerRadius = 8
        jobView.layer.shadowOpacity = 1
        jobView.layer.shadowRadius = 3.0
        jobView.layer.shadowOffset = CGSize(width: 0, height: 3)
        jobView.layer.shadowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        jobView.layer.borderWidth = 1
        jobView.layer.borderColor = #colorLiteral(red: 0.895331796, green: 0.895331796, blue: 0.895331796, alpha: 1)
        
        phoneView.layer.cornerRadius = 8
        phoneView.layer.shadowOpacity = 1
        phoneView.layer.shadowRadius = 3.0
        phoneView.layer.shadowOffset = CGSize(width: 0, height: 3)
        phoneView.layer.shadowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        phoneView.layer.borderWidth = 1
        phoneView.layer.borderColor = #colorLiteral(red: 0.895331796, green: 0.895331796, blue: 0.895331796, alpha: 1)
        
        saveBtn.layer.cornerRadius = 21
        let gradient = CAGradientLayer()
        gradient.frame = saveBtn.bounds
        gradient.cornerRadius = 21
        gradient.startPoint = CGPoint(x:0.0, y:0.5)
        gradient.endPoint = CGPoint(x:1.0, y:0.5)
        let colorright = UIColor(red: 150.0 / 255.0, green: 0.0 / 255.0, blue: 5.0 / 255.0, alpha: 1.0).cgColor
        let colorleft = UIColor(red: 223.0 / 255.0, green: 18.0 / 255.0, blue: 28.0 / 255.0, alpha: 1.0).cgColor
        gradient.colors = [colorright, colorleft]
        saveBtn.layer.addSublayer(gradient)
        
    }
    
    @IBAction func saveclicked(_ sender: UIButton) {
        
        if  nametext.text == "" {
            self.view.presentToast(msg: NSLocalizedString("typeName", comment: ""))
        }else if jobText.text == ""
        {
            self.view.presentToast(msg: NSLocalizedString("enterjob2", comment: ""))
        }else if phoneText.text == ""
        {
            self.view.presentToast(msg: NSLocalizedString("phoneErrno", comment: ""))
        }else if isValidPhone(phoneStr: phoneText.text!.replacedArabicDigitsWithEnglish) == false
        {
            self.view.presentToast(msg: NSLocalizedString("Vaildphone", comment: "") )
        }else{
            
            if from == "editContact" {
                if Reachability.isConnectedToNetwork() {
                    sv = UIViewController.displaySpinner(onView: self.view)
                    let jsonUrlString = editOrgContactEndpoint
                    guard let url = URL(string: jsonUrlString) else { return }
                    do {
                        
                        let postObject = [
                            
                            "Person":editObj.SID,
                            "User":  UserDefaults.standard.integer(forKey: "HajUserID"),
                            "Org": orgId,
                            "Name": nametext.text!,
                            "Position": jobText.text!,
                            "Phone": "966"+phoneText.text!.replacedArabicDigitsWithEnglish,
                            "Lang": langForApi
                            
                            ] as [String : Any]
                        
                        ApiService.callPost(url: url, params: postObject, finish: self.finisheditContactPost)
                    }
                }
                else{
                    self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
                }
                
                
                
            }else{
                print("addContact from if ")
                
                if Reachability.isConnectedToNetwork() {
                    sv = UIViewController.displaySpinner(onView: self.view)
                    let jsonUrlString = insertOrgContactEndpoint
                    guard let url = URL(string: jsonUrlString) else { return }
                    do {
                        
                        let postObject = [
                            "User":  UserDefaults.standard.integer(forKey: "HajUserID"),
                            "Org": orgId,
                            "Name": nametext.text!,
                            "Position": jobText.text!,
                            "Phone": "966"+phoneText.text!.replacedArabicDigitsWithEnglish,
                            "Lang": langForApi
                            
                            ] as [String : Any]
                        
                        ApiService.callPost(url: url, params: postObject, finish: self.finishaddcontactPost)
                    }
                }
                else{
                    self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
                }
                
                
            }
            
        }
        
        
    }
    
    
    
    func isValidPhone(phoneStr:String) -> Bool {
        
        //        let regularExpressionForPhone = "^\\d{3}-\\d{3}-\\d{4}$"
        let regularExpressionForPhone = "[0-9]{9,10}"
        let testPhone = NSPredicate(format:"SELF MATCHES %@", regularExpressionForPhone)
        return testPhone.evaluate(with: phoneStr)
    }
    
    func finisheditContactPost (message:String, data:Data?) -> Void
    {
        do
        {
            UIViewController.removeSpinner(spinner: self.sv!)
            if message == "Success" {
                if data != nil
                {
                    let response = try JSONDecoder().decode(insertRes.self, from: data!)
                    print("register response.Status : \(response.Status)")
                    
                    if response.Status == 1 {
                        
                        DispatchQueue.main.async {
                            self.view.presentToast(msg: response.Message)
                            self.performSegue(withIdentifier: "unwindSegToPopView", sender: self)
                            
                        }
                        
                    }
                    else {
                        DispatchQueue.main.async {
                            self.view.presentToast(msg: response.Message)
                        }
                    }
                    
                }
            }
            else{
                self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                print("userregistered for notifications failed")
            }
        }
        catch
        {
            print("userregistered for notifications Parse Error: \(error)")
        }
    }
    
    
    func finishaddcontactPost (message:String, data:Data?) -> Void
    {
        do
        {
            UIViewController.removeSpinner(spinner: self.sv!)
            if message == "Success" {
                if data != nil
                {
                    let response = try JSONDecoder().decode(insertRes.self, from: data!)
                    
                    
                    if response.Status == -2{
                        DispatchQueue.main.async {
                            self.view.presentToast(msg: response.Message)
                        }
                    }else {
                        DispatchQueue.main.async {
                            self.view.presentToast(msg: response.Message)
                            self.performSegue(withIdentifier: "unwindSegToPopView", sender: self)
                            
                            
                        }
                        
                    }
                    
                }
            }
            else{
                self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                print("userregistered for notifications failed")
            }
        }
        catch
        {
            print("userregistered for notifications Parse Error: \(error)")
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 10
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    @IBAction func closeView(_ sender: UIButton) {
        self.performSegue(withIdentifier: "unwindSegToPopView", sender: self)
    }
    
}
