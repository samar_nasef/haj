//
//  HomeTableViewController.swift
//
//
//
//  Copyright © 2019 ITRoots. All rights reserved.
//

import UIKit


class HomeTableViewController: LocalizerViewController,UITableViewDelegate,UITableViewDataSource {
    
    var fbtn = UIButton()
    var fbtn2 = UIButton()
    private var currentPage = 0
    var tmparrlength = -1
    var VisitsData : [visit] = []
    var monthArr : [String] = ["m","يناير","فبراير", "مارس", "أبريل", "مايو", "يونيو",  "يوليو", "أغسطس",  "سبتمبر",  "أكتوبر",  "نوفمبر",  "ديسمبر"];
    
    var monthArren : [String] = ["m","Jan.","Feb.","March","April","May","June","July","Aug.","Sep.","Oct.","Nov.","Dec."];
    
    var sv: UIView?
    var selectedVisitId:Int = -1
    var showfrom:String = ""
    var refreshControl = UIRefreshControl()
    var label:UILabel = UILabel()
    @IBOutlet weak var visitsTabelView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        drawNotificationBell()
        
        NotificationCenter.default.addObserver(self,selector: #selector(self.drawNotificationBell),name: NSNotification.Name(rawValue: "reloadnotificationCount"),object: nil)
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.loadVisits),
                                               name: NSNotification.Name(rawValue: "getnewtoken"),
                                               object: nil)
        
        
        
        if L102Language.currentAppleLanguage() == "ar"{
            floatingButton()
            langForApi =  1
            
        }else{
            langForApi =  2
            floatingButtonen()
        }
        
        
        label =  UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 21))
        
        refreshControl.addTarget(self, action: #selector(loadVisits), for: UIControl.Event.valueChanged)
        visitsTabelView.addSubview(refreshControl)
        
        
        visitsTabelView.delegate = self
        visitsTabelView.dataSource = self
        visitsTabelView.tableFooterView = UIView()
        
    }
    
    func finishListUserPost (message:String, data:Data?) -> Void
    {
        do
        {
            UIViewController.removeSpinner(spinner: self.sv!)
            if message == "Success" {
                if data != nil
                {
                    let response = try JSONDecoder().decode(listUserVisits.self, from: data!)
                    
                    DispatchQueue.main.async {
                        self.refreshControl.endRefreshing()
                        self.label.removeFromSuperview()
                        if self.currentPage  == 0 {
                            self.VisitsData = []
                        }
                        self.visitsTabelView.reloadData()
                        
                    }
                    
                    UserDefaults.standard.set(response.NotificationCount, forKey: "notificationCount")
                    
                    if response.Status == 1 {
                        print("visit status 1")
                        DispatchQueue.main.async {
                            print("visit status 2")
                            if let x = response.VisitsList{
                                self.tmparrlength = x.count
                                
                                if self.currentPage > 0{
                                    for itemx in x {
                                        self.VisitsData.append(itemx)
                                    }
                                    
                                }else{
                                    self.VisitsData = x
                                }
                                
                                self.visitsTabelView.reloadData()
                            }
                            
                        }
                    }
                    else{
                        
                        DispatchQueue.main.async {
                            
                            if response.Status == nil{
                                print("auth error")
                                
                            }else{
                                self.tmparrlength = -1
                                if self.VisitsData.count == 0{
                                    
                                    self.label.center = CGPoint(x: 0, y: 380)
                                    self.label.textAlignment = .center
                                    self.label.center.x = self.view.center.x
                                    
                                    self.label.tag = 1
                                    
                                    self.label.font = UIFont(name: "Cairo SemiBold", size: 16)
                                    self.label.textColor = #colorLiteral(red: 0.4476690292, green: 0.4476690292, blue: 0.4476690292, alpha: 1)
                                    
                                    self.label.text = response.Message
                                    self.view.addSubview(self.label)
                                }
                                
                            }
                            
                            
                        }
                        
                        
                    }
                    
                }
            }
            else{
                DispatchQueue.main.async {
                    
                    self.refreshControl.endRefreshing()
                    
                    self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                }
                print("userregistered for notifications failed")
                
            }
            
            
        }
        catch
        {
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
                print("userregistered for notifications Parse Error: \(error)")
            }
        }
    }
    
    
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return VisitsData.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "homecell", for: indexPath) as! HomeTableViewCell
        let item = self.VisitsData[indexPath.row]
        cell.cellTitle.text = item.Title
        cell.organizationName.text = item.OrgName
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        dateFormatterGet.timeZone = TimeZone.current
        let estimatedDate = dateFormatterGet.date(from: item.VisitDate)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        let estimatedDateString = dateFormatter.string(from: estimatedDate!)
        
        let newDate = estimatedDateString.split(separator: "/")
        
        let month = Int(String(newDate[1]).replacedArabicDigitsWithEnglish)!
        print("month: \(month)")
        if L102Language.currentAppleLanguage() == "en" {
            cell.date.text =  newDate[0] + " " + monthArren[month] + " " + newDate[2]
        }else{
            cell.date.text =  newDate[0] + " " + monthArr[month] + " " + newDate[2]
        }
        
        
        let gradient = CAGradientLayer()
        gradient.frame = cell.showDetailsBtn.bounds
        gradient.cornerRadius = 18
        gradient.startPoint = CGPoint(x:0.0, y:0.5)
        gradient.endPoint = CGPoint(x:1.0, y:0.5)
        let colorright = UIColor(red: 150.0 / 255.0, green: 0.0 / 255.0, blue: 5.0 / 255.0, alpha: 1.0).cgColor
        let colorleft = UIColor(red: 223.0 / 255.0, green: 18.0 / 255.0, blue: 28.0 / 255.0, alpha: 1.0).cgColor
        gradient.colors = [colorright, colorleft]
        cell.showDetailsBtn.layer.addSublayer(gradient)
        cell.showDetailsBtn.layer.cornerRadius = 18
        
        let gradient1 = CAGradientLayer()
        gradient1.frame = cell.notesBtn.bounds
        gradient1.cornerRadius = 18
        gradient1.startPoint = CGPoint(x:0.0, y:0.5)
        gradient1.endPoint = CGPoint(x:1.0, y:0.5)
        let colorright1 = UIColor(red: 56.0 / 255.0, green: 56.0 / 255.0, blue: 56.0 / 255.0, alpha: 1.0).cgColor
        let colorleft1 = UIColor(red: 86.0 / 255.0, green: 86.0 / 255.0, blue: 86.0 / 255.0, alpha: 1.0).cgColor
        gradient1.colors = [colorright1, colorleft1]
        cell.notesBtn.layer.addSublayer(gradient1)
        cell.notesBtn.layer.cornerRadius = 18
        cell.showDetailsBtn.tag = item.SID
        cell.showDetailsBtn.addTarget(self, action: #selector(showVisitDetails(_:)), for: .touchUpInside)
        cell.notesBtn.tag = item.SID
        cell.notesBtn.addTarget(self, action: #selector(shownotes(_:)), for: .touchUpInside)
        cell.editBtn.tag = item.SID
        cell.editBtn.addTarget(self, action: #selector(editBtn(_:)), for: .touchUpInside)
        
        if item.Status == 0 {
            cell.visitStatus.text = NSLocalizedString("waiting", comment: "")
            cell.visitStatus.textColor = #colorLiteral(red: 0.8196078431, green: 0.6431372549, blue: 0.3529411765, alpha: 1)
            cell.notesBtn.isHidden = true
        }else  if item.Status == 1 {
            cell.visitStatus.text = NSLocalizedString("accepted", comment: "")
            cell.visitStatus.textColor = #colorLiteral(red: 0, green: 0.6117647059, blue: 0.2588235294, alpha: 1)
            cell.notesBtn.isHidden = false
        }else  if item.Status == 2 {
            cell.visitStatus.text = NSLocalizedString("rejecred", comment: "")
            cell.visitStatus.textColor = #colorLiteral(red: 0.737254902, green: 0.03529411765, blue: 0.06666666667, alpha: 1)
            cell.notesBtn.isHidden = false
            
        }
        
        
        return cell
    }
    
    @objc func showVisitDetails(_ showdetails: UIButton){
        print("show deatils visit id :  \(showdetails.tag)")
        selectedVisitId = showdetails.tag
        showfrom = "show"
        self.performSegue(withIdentifier: "edit_show_Visit", sender: nil)
        
    }
    
    @objc func shownotes(_ showdetails: UIButton){
        print("show notes visit id :  \(showdetails.tag)")
        selectedVisitId = showdetails.tag
        self.performSegue(withIdentifier: "notesegue", sender: nil)
        
    }
    
    @objc func editBtn(_ showdetails: UIButton){
        print("edit visit id :  \(showdetails.tag)")
        selectedVisitId = showdetails.tag
        showfrom = ""
        self.performSegue(withIdentifier: "edit_show_Visit", sender: nil)
    }
    
    func floatingButton(){
        
        let height = UIScreen.main.bounds.height
        fbtn.frame = CGRect(x: 30, y: height-280, width: 100, height: 100)
        fbtn.setBackgroundImage(UIImage(named: "floatingButton"), for: .normal)
        fbtn.clipsToBounds = true
        fbtn.layer.cornerRadius = 50
        fbtn.addTarget(self,action: #selector(newVisit), for: .touchUpInside)
        visitsTabelView.addSubview(fbtn)
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let  off = self.visitsTabelView.contentOffset.y
        let height = UIScreen.main.bounds.height
        fbtn.frame = CGRect(x: 30, y:   off + (height-280), width: fbtn.frame.size.width, height: fbtn.frame.size.height)
        let width  = UIScreen.main.bounds.width
        fbtn2.frame = CGRect(x: width-130, y:   off + (height-280), width: fbtn2.frame.size.width, height: fbtn2.frame.size.height)
    }
    
    func floatingButtonen(){
        
        let width  = UIScreen.main.bounds.width
        let height = UIScreen.main.bounds.height
        
        fbtn2.frame = CGRect(x: width-130 , y:height-280 , width: 100, height: 100)
        fbtn2.setBackgroundImage(UIImage(named: "floatingButton"), for: .normal)
        fbtn2.clipsToBounds = true
        fbtn2.layer.cornerRadius = 50
        fbtn2.addTarget(self,action: #selector(newVisit), for: .touchUpInside)
        visitsTabelView.addSubview(fbtn2)
    }
    
    
    @objc func newVisit(){
        self.performSegue(withIdentifier: "addNewVisit", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "notesegue" {
            if let destinationVC = segue.destination as? notesTableViewController {
                
                destinationVC.visitId = selectedVisitId
            }
        }else if segue.identifier == "edit_show_Visit" {
            if let destinationVC = segue.destination as? newVisit2VC {
                
                destinationVC.visitIdToeditORShow = selectedVisitId
                destinationVC.from = "edit_show"
                destinationVC.from2 = self.showfrom
                
            }
        }
        
        
    }
    
    
    @objc func loadVisits() {
        print("loadVisits")
        currentPage = 0;
        
        if Reachability.isConnectedToNetwork() {
            sv = UIViewController.displaySpinner(onView: self.view)
            let jsonUrlString = newhomeEndpoint
            
            guard let url = URL(string: jsonUrlString) else { return }
            do {
                
                var firebase_token = ""
                if let firebase_token_val = UserDefaults.standard.string(forKey: "firebase_token")
                {
                    firebase_token = firebase_token_val
                }
                
                
                
                let postObject = [
                    "UserID": UserDefaults.standard.integer(forKey: "HajUserID"),
                    "MobileDeviceID": firebase_token,
                    "MobileType": 2,
                    "Lang": langForApi,
                    "Page":currentPage
                    
                    ] as [String : Any]
                
                ApiService.callPost(url: url, params: postObject, finish: self.finishListUserPost)
            }
        }
        else{
            self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
        }
        
        
    }
    func loginAgainToUpdateToken()  {
        
        print("loginAgainToUpdateToken")
        if let savedPerson = UserDefaults.standard.object(forKey: "HajUserDetails") as? Data {
            let decoder = JSONDecoder()
            if let loadedPerson = try? decoder.decode(userDetails2.self, from: savedPerson) {
                
                let userName =  loadedPerson.Username!
                let userPass =  loadedPerson.password!
                
                
                if Reachability.isConnectedToNetwork() {
                    sv = UIViewController.displaySpinner(onView: self.view)
                    
                    let jsonUrlString = loginEndpoint+userName.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!+"&password="+userPass
                    
                    guard let url = URL(string: jsonUrlString) else { return }
                    
                    URLSession.shared.dataTask(with: url) { (data, response, err) in
                        UIViewController.removeSpinner(spinner: self.sv!)
                        if (err != nil){
                            self.view.presentToast(msg: NSLocalizedString("serverErr", comment: "") )
                            return
                        }
                        if let status = response as? HTTPURLResponse {
                            print("request status \(status.statusCode)")
                            if status.statusCode != 200{
                                self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                                return
                            }
                        }
                        guard let data = data else { return }
                        do {
                            print(data)
                            let response = try JSONDecoder().decode(loginApiRes.self, from: data)
                            
                            if response.Status == 1 {
                                
                                if response.UserDetails.IsVerified == true {
                                    DispatchQueue.main.async {
                                        UserDefaults.standard.set(response.UserDetails.IsVerified, forKey: "HajIsVerifiedStatus")
                                        UserDefaults.standard.set(response.UserDetails.SID, forKey: "HajUserID")
                                        
                                        let x = userDetails2(SID: response.UserDetails.SID, FullName: response.UserDetails.FullName, Username: response.UserDetails.Username, Phone: response.UserDetails.Phone, Email: response.UserDetails.Email, LastSeenDate: response.UserDetails.LastSeenDate, ProfileImage: response.UserDetails.ProfileImage, NotificationEnabled: response.UserDetails.NotificationEnabled, VerificationCode: response.UserDetails.VerificationCode, IsVerified: response.UserDetails.IsVerified, UserType: response.UserDetails.UserType, CreatedBy: response.UserDetails.CreatedBy, Status: response.UserDetails.Status, LastUpdatedDate: response.UserDetails.LastUpdatedDate, LastUpdatedBy: response.UserDetails.LastUpdatedBy,PositionName: response.UserDetails.PositionName,OrgOrDeptName: response.UserDetails.OrgOrDeptName,Token:response.UserDetails.Token,password:userPass)
                                        let encoder = JSONEncoder()
                                        if let encoded = try? encoder.encode(x) {
                                            let defaults = UserDefaults.standard
                                            defaults.set(encoded, forKey: "HajUserDetails")
                                        }
                                        print("new token 22")
                                        self.loadVisits()
                                        
                                        
                                    }
                                }
                                
                            }
                            else{
                                
                            }
                        }
                            
                        catch let jsonErr {
                            print("Error serializing json:", jsonErr)
                            
                        }
                    }.resume()
                }
                else{
                    
                }
                
                
                
            }
        }
        
        
    }
    
    
    @objc func openNotification(sender: UIBarButtonItem){
        
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "notificationVc") as! notificationsViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
        
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.loadVisits()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == VisitsData.count - 1{
            
            if self.tmparrlength  == 10 {
                currentPage = currentPage + 1;
                
                self.loadVisits2()
            }
        }
    }
    
    
    
    @objc func loadVisits2() {
        
        if Reachability.isConnectedToNetwork() {
            sv = UIViewController.displaySpinner(onView: self.view)
            let jsonUrlString = newhomeEndpoint
            
            guard let url = URL(string: jsonUrlString) else { return }
            do {
                
                var firebase_token = ""
                if let firebase_token_val = UserDefaults.standard.string(forKey: "firebase_token")
                {
                    firebase_token = firebase_token_val
                }
                
                
                let postObject = [
                    "UserID": UserDefaults.standard.integer(forKey: "HajUserID"),
                    "MobileDeviceID": firebase_token,
                    "MobileType": 2,
                    "Lang": langForApi,
                    "Page":currentPage
                    
                    ] as [String : Any]
                
                ApiService.callPost(url: url, params: postObject, finish: self.finishListUserPost)
            }
        }
        else{
            self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
        }
        
        
    }
    
    
    
    @objc func drawNotificationBell(){
        let notificationButton = SSBadgeButton()
        notificationButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        notificationButton.setImage(UIImage(named: "notificationsButton-1")?.withRenderingMode(.alwaysTemplate), for: .normal)
        notificationButton.tintColor = .white
        
        if UserDefaults.standard.integer(forKey: "notificationCount") > 0 {
            
            notificationButton.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
            notificationButton.badge = String(UserDefaults.standard.integer(forKey: "notificationCount"))
        }
        
        notificationButton.addTarget(self, action: #selector(openNotification(sender:)), for: .touchUpInside)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: notificationButton)
    }
    
}
