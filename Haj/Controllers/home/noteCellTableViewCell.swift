//
//  noteCellTableViewCell.swift
//
//
//
//  Copyright © 2019 ITRoots. All rights reserved.
//

import UIKit

class noteCellTableViewCell: UITableViewCell {
    
    @IBOutlet weak var noteText: UILabel!
    @IBOutlet weak var dateText: UILabel!
    @IBOutlet weak var byText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
