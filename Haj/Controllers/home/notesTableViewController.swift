//
//  notesTableViewController.swift
//
//
//
//  Copyright © 2019 ITRoots. All rights reserved.
//

import UIKit

class notesTableViewController: UITableViewController {
    
    @IBOutlet var notesTabelVIew: UITableView!
    var notesData : [comment] = []
    var sv: UIView?
    var label:UILabel = UILabel()
    var visitId:Int = -1
    var monthArr : [String] = ["m","يناير","فبراير", "مارس", "أبريل", "مايو", "يونيو",  "يوليو", "أغسطس",  "سبتمبر",  "أكتوبر",  "نوفمبر",  "ديسمبر"];
    
    var monthArren : [String] = ["m","Jan.","Feb.","March","April","May","June","July","Aug.","Sep.","Oct.","Nov.","Dec."];
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        label =  UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 21))
        
        if L102Language.currentAppleLanguage() == "ar"{
            
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "leftArrow"), style: .plain, target: self, action: #selector(gobackTapped))
            self.navigationItem.rightBarButtonItem?.tintColor = .white
        }else{
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "leftArrowCopy-1"), style: .plain, target: self, action: #selector(gobackTapped))
            self.navigationItem.rightBarButtonItem?.tintColor = .white
            
        }
        
        
        if L102Language.currentAppleLanguage() == "ar"{
            
            langForApi =  1
            
        }else{
            langForApi =  2
        }
        
        self.navigationItem.title = NSLocalizedString("notes", comment: "")
        self.navigationItem.hidesBackButton = true
        
        notesTabelVIew.delegate = self
        notesTabelVIew.dataSource = self
        notesTabelVIew.tableFooterView = UIView()
        
        if Reachability.isConnectedToNetwork() {
            sv = UIViewController.displaySpinner(onView: self.view)
            let userId = UserDefaults.standard.integer(forKey: "HajUserID")
            let jsonUrlString = visitNotesEndpoint+String(userId)+"&visit="+String(visitId)+"&lang=\(String(langForApi))"
            
            
            guard let url = URL(string: jsonUrlString) else { return }
            URLSession.shared.dataTask(with: url) { (data, response, err) in
                UIViewController.removeSpinner(spinner: self.sv!)
                if (err != nil){
                    self.view.presentToast(msg: NSLocalizedString("serverErr", comment: "") )
                    return
                }
                if let status = response as? HTTPURLResponse {
                    
                    if status.statusCode != 200{
                        self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                        return
                    }
                }
                guard let data = data else { return }
                do {
                    print(data)
                    let response = try JSONDecoder().decode(notesList.self, from: data)
                    
                    if response.Status == 1 {
                        
                        for item in response.CommentsList {
                            if item.CommentText != "" {
                                self.notesData.append(item)
                            }
                        }
                        
                        if self.notesData.count == 0 {
                            DispatchQueue.main.async {
                                
                                self.label.center = CGPoint(x: 0, y: 300)
                                
                                self.label.textAlignment = .center
                                self.label.center.x = self.view.center.x
                                
                                self.label.tag = 1
                                
                                self.label.font = UIFont(name: "Cairo SemiBold", size: 16)
                                self.label.textColor = #colorLiteral(red: 0.4476690292, green: 0.4476690292, blue: 0.4476690292, alpha: 1)
                                
                                self.label.text =    NSLocalizedString("nonotes", comment: "")
                                
                                self.view.addSubview(self.label)
                                
                            }
                        }
                        else{
                            DispatchQueue.main.async {
                                
                                self.notesTabelVIew.reloadData()
                            }
                        }
                    }
                    else{
                        
                        DispatchQueue.main.async {
                            
                            self.label.center = CGPoint(x: 0, y: 300)
                            
                            self.label.textAlignment = .center
                            self.label.center.x = self.view.center.x
                            
                            self.label.tag = 1
                            self.label.font = UIFont(name: "Cairo SemiBold", size: 16)
                            self.label.textColor = #colorLiteral(red: 0.4476690292, green: 0.4476690292, blue: 0.4476690292, alpha: 1)
                            self.label.text =    NSLocalizedString("nonotes", comment: "")
                            self.view.addSubview(self.label)
                            
                            
                        }
                        
                    }
                }
                    
                catch let jsonErr {
                    print("Error serializing json:", jsonErr)
                    self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                }
            }.resume()
        }
        else{
            self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
        }
        
        
        
    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return notesData.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "noteCell", for: indexPath) as! noteCellTableViewCell
        let item = self.notesData[indexPath.row]
        
        cell.noteText.text = item.CommentText
        
        cell.noteText.numberOfLines = 5
        let maximumLabelSize: CGSize = CGSize(width: 280, height: 9999)
        let expectedLabelSize: CGSize = cell.noteText.sizeThatFits(maximumLabelSize)
        
        var newFrame: CGRect = cell.noteText.frame
        newFrame.size.height = expectedLabelSize.height
        cell.noteText.frame = newFrame
        cell.byText.text = item.AdminFullName
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
        dateFormatterGet.timeZone = TimeZone.current
        let estimatedDate = dateFormatterGet.date(from: item.CreationDate)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let estimatedDateString = dateFormatter.string(from: estimatedDate!)
        let newDate = estimatedDateString.split(separator: "/")
        
        let month = Int(String(newDate[1]).replacedArabicDigitsWithEnglish)!
        
        if L102Language.currentAppleLanguage() == "en" {
            cell.dateText.text =  newDate[0] + " " + monthArren[month] + " " + newDate[2]
        }else{
            cell.dateText.text =  newDate[0] + " " + monthArr[month] + " " + newDate[2]
        }
        
        return cell
    }
    
    @IBAction func goback(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func gobackTapped() {
        self.navigationController?.popViewController(animated: true)
    }
}
