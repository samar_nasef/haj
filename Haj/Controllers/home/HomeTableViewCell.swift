//
//  HomeTableViewCell.swift
//
//
//
//  Copyright © 2019 ITRoots. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var organizationName: UILabel!
    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var visitStatus: UILabel!
    @IBOutlet weak var notesBtn: UIButton!
    @IBOutlet weak var showDetailsBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
