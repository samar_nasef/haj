//
//  notificationTableViewCell.swift
//  
//
//
//  Copyright © 2020 ITRoots. All rights reserved.
//

import UIKit

class notificationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var notificationTime: UILabel!
    @IBOutlet weak var notificationText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
