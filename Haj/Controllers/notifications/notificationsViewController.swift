//
//  notificationsViewController.swift
//
// The notifications screen displays the list of user notifications
//
//  Copyright © 2020 ITRoots. All rights reserved.
//

import UIKit



class notificationsViewController: LocalizerViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var notificationTabel: UITableView!
    var refreshControl = UIRefreshControl()
    var label:UILabel = UILabel()
    var sv: UIView?
    var notificationData:[notificationobj]=[]
    
    @IBOutlet weak var nonotificationlbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        UserDefaults.standard.set(0, forKey: "notificationCount")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadnotificationCount"), object: nil)
        
        self.navigationItem.title = NSLocalizedString("notifications", comment: "")
        self.navigationItem.hidesBackButton = true
        
        if L102Language.currentAppleLanguage() == "ar"{
            langForApi =  1
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "leftArrow"), style: .plain, target: self, action: #selector(gobackTapped))
            self.navigationItem.rightBarButtonItem?.tintColor = .white
        }else{
            langForApi =  2
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "leftArrowCopy-1"), style: .plain, target: self, action: #selector(gobackTapped))
            self.navigationItem.rightBarButtonItem?.tintColor = .white
            
        }
        
        
        label =  UILabel(frame: CGRect(x: 0, y: 0, width: 250, height: 21))
        
        refreshControl.addTarget(self, action: #selector(loadVisits), for: UIControl.Event.valueChanged)
        notificationTabel.addSubview(refreshControl)
        
        notificationTabel.delegate = self
        notificationTabel.dataSource = self
        notificationTabel.tableFooterView = UIView()
        
        if Reachability.isConnectedToNetwork() {
            sv = UIViewController.displaySpinner(onView: self.view)
            let userId = UserDefaults.standard.integer(forKey: "HajUserID")
            
            let jsonUrlString = notificationEndpoint+String(userId)+"&lang=\(String(langForApi))"
            
            guard let url = URL(string: jsonUrlString) else { return }
            URLSession.shared.dataTask(with: url) { (data, response, err) in
                UIViewController.removeSpinner(spinner: self.sv!)
                if (err != nil){
                    self.view.presentToast(msg: NSLocalizedString("serverErr", comment: "") )
                    return
                }
                if let status = response as? HTTPURLResponse {
                    
                    if status.statusCode != 200{
                        self.view.presentToast(msg: NSLocalizedString("serverErr", comment: "") )
                        return
                    }
                }
                guard let data = data else { return }
                do {
                    
                    let response = try JSONDecoder().decode(notificationistRes.self, from: data)
                    
                    DispatchQueue.main.async {
                        
                        self.label.removeFromSuperview()
                        
                    }
                    if response.Status == 1 {
                        self.notificationData = response.NotificationsList!
                        if self.notificationData.count == 0 {
                            DispatchQueue.main.async {
                                
                                self.label.center = CGPoint(x: 160, y: 285)
                                self.label.textAlignment = .center
                                self.label.center.x = self.view.center.x
                                self.label.font = UIFont(name: "Cairo SemiBold", size: 16)
                                self.label.textColor = #colorLiteral(red: 0.4476690292, green: 0.4476690292, blue: 0.4476690292, alpha: 1)
                                self.label.text = response.Message
                                self.view.addSubview(self.label)
                                
                            }
                        }
                        else{
                            DispatchQueue.main.async {
                                
                                self.notificationTabel.reloadData()
                            }
                        }
                    }
                    else{
                        
                        DispatchQueue.main.async {
                            
                            self.label.center = CGPoint(x: 160, y: 285)
                            self.label.tag = 1
                            self.label.textAlignment = .center
                            self.label.center.x = self.view.center.x
                            self.label.font = UIFont(name: "Cairo SemiBold", size: 16)
                            self.label.textColor = #colorLiteral(red: 0.4476690292, green: 0.4476690292, blue: 0.4476690292, alpha: 1)
                            self.label.text = response.Message
                            self.view.addSubview(self.label)
                            
                            
                        }
                        
                    }
                }
                    
                catch _ {
                    
                    self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                }
            }.resume()
        }
        else{
            self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
        }
        
        
        
        
        
    }
    
    @objc func gobackTapped() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return notificationData.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath) as! notificationTableViewCell
        let item = self.notificationData[indexPath.row]
        cell.notificationText.text = item.Message
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
        dateFormatterGet.timeZone = TimeZone.current
        let estimatedDate = dateFormatterGet.date(from: item.CreationDate)
        cell.notificationTime.text = timeAgoSince(estimatedDate!)
        
        return cell
    }
    
    
    @objc func loadVisits() {
        
        if Reachability.isConnectedToNetwork() {
            sv = UIViewController.displaySpinner(onView: self.view)
            let userId = UserDefaults.standard.integer(forKey: "HajUserID")
            let jsonUrlString = notificationEndpoint+String(userId)+"&lang=\(String(langForApi))"
            
            guard let url = URL(string: jsonUrlString) else { return }
            URLSession.shared.dataTask(with: url) { (data, response, err) in
                UIViewController.removeSpinner(spinner: self.sv!)
                if (err != nil){
                    self.view.presentToast(msg: NSLocalizedString("serverErr", comment: "") )
                    return
                }
                if let status = response as? HTTPURLResponse {
                    print("request status \(status.statusCode)")
                    if status.statusCode != 200{
                        self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                        return
                    }
                }
                guard let data = data else { return }
                do {
                    print(data)
                    let response = try JSONDecoder().decode(notificationistRes.self, from: data)
                    
                    DispatchQueue.main.async {
                        
                        self.refreshControl.endRefreshing()
                        self.label.removeFromSuperview()
                        self.notificationData = []
                        self.notificationTabel.reloadData()
                        
                    }
                    if response.Status == 1 {
                        self.notificationData = response.NotificationsList!
                        if self.notificationData.count == 0 {
                            DispatchQueue.main.async {
                                
                                self.label.textAlignment = .center
                                self.label.center.x = self.view.center.x
                                self.label.font = UIFont(name: "Cairo SemiBold", size: 16)
                                self.label.textColor = #colorLiteral(red: 0.4476690292, green: 0.4476690292, blue: 0.4476690292, alpha: 1)
                                self.label.text = response.Message
                                self.view.addSubview(self.label)
                                
                                
                            }
                        }
                        else{
                            DispatchQueue.main.async {
                                self.notificationData = response.NotificationsList!
                                self.notificationTabel.reloadData()
                            }
                        }
                    }
                    else{
                        
                        DispatchQueue.main.async {
                            
                            self.label.center = CGPoint(x: 160, y: 285)
                            self.label.textAlignment = .center
                            
                            self.label.center.x = self.view.center.x
                            
                            self.label.font = UIFont(name: "Cairo SemiBold", size: 16)
                            self.label.textColor = #colorLiteral(red: 0.4476690292, green: 0.4476690292, blue: 0.4476690292, alpha: 1)
                            self.label.text = response.Message
                            self.view.addSubview(self.label)
                            
                            
                            
                            
                            
                        }
                        
                        
                    }
                }
                    
                catch let jsonErr {
                    print("Error serializing json:", jsonErr)
                    DispatchQueue.main.async {
                        self.refreshControl.endRefreshing()
                        self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                    }
                }
            }.resume()
        }
        else{
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
                self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
            }
        }
        
        
        
    }
    
    
    public func timeAgoSince(_ date: Date) -> String {
        
        let calendar = Calendar.current
        let now = Date()
        print("now date : \(now)")
        let unitFlags: NSCalendar.Unit = [.second, .minute, .hour, .day, .weekOfYear, .month, .year]
        let components = (calendar as NSCalendar).components(unitFlags, from: date, to: now, options: [])
        
        if let year = components.year, year >= 2 {
            let value = NSLocalizedString("years ago", comment: "")
            return "\(year) \(value)"
        }
        
        if let year = components.year, year >= 1 {
            return NSLocalizedString("Last year", comment: "")
        }
        
        if let month = components.month, month >= 2 {
            let value = NSLocalizedString("months ago", comment: "")
            return "\(month) \(value)"
        }
        
        if let month = components.month, month >= 1 {
            return NSLocalizedString("Last month", comment: "")
        }
        
        if let week = components.weekOfYear, week >= 2 {
            let value = NSLocalizedString("weeks ago", comment: "")
            return "\(week) \(value)"
        }
        
        if let week = components.weekOfYear, week >= 1 {
            return NSLocalizedString("Last week", comment: "")
        }
        
        if let day = components.day, day >= 2 {
            let value = NSLocalizedString("days ago", comment: "")
            return "\(day) \(value)"
        }
        
        if let day = components.day, day >= 1 {
            return NSLocalizedString("Yesterday", comment: "")
        }
        
        if let hour = components.hour, hour >= 2 {
            let value = NSLocalizedString("hours ago", comment: "")
            return "\(hour) \(value)"
        }
        
        if let hour = components.hour, hour >= 1 {
            return NSLocalizedString("An hour ago", comment: "")
        }
        
        if let minute = components.minute, minute >= 2 {
            let value = NSLocalizedString("minutes ago", comment: "")
            return "\(minute) \(value)"
        }
        
        if let minute = components.minute, minute >= 1 {
            return NSLocalizedString("A minute ago", comment: "")
        }
        
        if let second = components.second, second >= 3 {
            let value = NSLocalizedString("seconds ago", comment: "")
            return "\(second) \(value)"
        }
        
        return NSLocalizedString("Just now", comment: "")
        
    }
    
    
    func timeAgoSinceDate(_ date:Date, numericDates:Bool = false) -> String {
        let calendar = NSCalendar.current
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        let now = Date()
        let earliest = now < date ? now : date
        let latest = (earliest == now) ? date : now
        let components = calendar.dateComponents(unitFlags, from: earliest,  to: latest)
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
        
    }
    
}
