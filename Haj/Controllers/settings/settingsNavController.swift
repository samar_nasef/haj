//
//  settingsNavController.swift
//
//
//
//  Copyright © 2019 ITRoots. All rights reserved.
//

import UIKit

class settingsNavController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let gradient = CAGradientLayer()
        var bounds = navigationBar.bounds
        bounds.size.height += UIApplication.shared.statusBarFrame.size.height
        gradient.frame = bounds
        
        let colorright = UIColor(red: 150.0 / 255.0, green: 0.0 / 255.0, blue: 5.0 / 255.0, alpha: 1.0).cgColor
        let colorleft = UIColor(red: 202.0 / 255.0, green: 16.0 / 255.0, blue: 25.0 / 255.0, alpha: 1.0).cgColor
        
        gradient.colors = [colorright, colorleft]
        
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 1, y: 0)
        
        if let image = getImageFrom(gradientLayer: gradient) {
            navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
        }
        
        
    }
    
    
    func getImageFrom(gradientLayer:CAGradientLayer) -> UIImage? {
        var gradientImage:UIImage?
        UIGraphicsBeginImageContext(gradientLayer.frame.size)
        if let context = UIGraphicsGetCurrentContext() {
            gradientLayer.render(in: context)
            gradientImage = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
        }
        UIGraphicsEndImageContext()
        return gradientImage
    }
    
}
