//
//  aboutAppViewController.swift
//
//
//
//  Copyright © 2019 ITRoots. All rights reserved.
//

import UIKit

class aboutAppViewController: LocalizerViewController {
    
    var sv: UIView?
    @IBOutlet weak var aboutText: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = NSLocalizedString("Aboutapp", comment: "")
        self.navigationItem.hidesBackButton = true
        
        if L102Language.currentAppleLanguage() == "ar"{
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "leftArrow"), style: .plain, target: self, action: #selector(gobackTapped))
            self.navigationItem.rightBarButtonItem?.tintColor = .white
        }else{
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "leftArrowCopy-1"), style: .plain, target: self, action: #selector(gobackTapped))
            self.navigationItem.rightBarButtonItem?.tintColor = .white
            
        }
        
        if Reachability.isConnectedToNetwork() {
            sv = UIViewController.displaySpinner(onView: self.view)
            let userId = UserDefaults.standard.integer(forKey: "HajUserID")
            
            
            if L102Language.currentAppleLanguage() == "ar"{
                
                langForApi =  1
                
            }else{
                langForApi =  2
            }
            
            
            let jsonUrlString = aboutAppEndpoint+String(userId)+"&lang=\(String(langForApi))"
            
            guard let url = URL(string: jsonUrlString) else { return }
            URLSession.shared.dataTask(with: url) { (data, response, err) in
                UIViewController.removeSpinner(spinner: self.sv!)
                if (err != nil){
                    self.view.presentToast(msg: NSLocalizedString("serverErr", comment: "") )
                    return
                }
                if let status = response as? HTTPURLResponse {
                    print("request status \(status.statusCode)")
                    if status.statusCode != 200{
                        self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                        return
                    }
                }
                guard let data = data else { return }
                do {
                    print(data)
                    let response = try JSONDecoder().decode(aboutapi.self, from: data)
                    
                    
                    if response.Status == 1 {
                        
                        DispatchQueue.main.async {
                            self.aboutText.text = response.AboutDetails.AboutText
                            
                        }
                        
                        
                    }
                    else{
                        self.view.presentToast(msg: response.Message)
                        
                        
                    }
                }
                    
                catch let jsonErr {
                    print("Error serializing json:", jsonErr)
                    self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                }
            }.resume()
        }
        else{
            self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
        }
        
    }
    
    @IBAction func goBack(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @objc func gobackTapped() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
