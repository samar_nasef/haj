//
//  changeLangViewController.swift
//
//
//
//  Copyright © 2020 ITRoots. All rights reserved.
//

import UIKit

class changeLangViewController: UIViewController {
    
    var from:String = ""
    @IBOutlet weak var mainView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.layer.cornerRadius = 10
        
    }
    
    
    @IBAction func arabicLang(_ sender: UIButton) {
        
        L102Language.setAppleLAnguageTo(lang: "ar")
        langForApi =  1
        
        var transition: UIView.AnimationOptions = .transitionFlipFromLeft
        
        transition = .transitionFlipFromRight
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        
        
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        if from == "login"{
            rootviewcontroller.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "rootnav")
            
        }else{
            rootviewcontroller.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "mainViewController")
        }
        
        let mainwindow = (UIApplication.shared.delegate?.window!)!
        mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
        UIView.transition(with: mainwindow, duration: 0.55001, options: transition, animations: { () -> Void in
        }) { (finished) -> Void in
            
        }
    }
    
    @IBAction func englishLang(_ sender: Any) {
        
        L102Language.setAppleLAnguageTo(lang: "en")
        langForApi =  2
        
        UIView.appearance().semanticContentAttribute = .forceRightToLeft
        
        
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        if from == "login"{
            rootviewcontroller.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "rootnav")
            
        }else{
            
            rootviewcontroller.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "mainViewController")
        }
        
        let transition: UIView.AnimationOptions = .transitionFlipFromLeft
        let mainwindow = (UIApplication.shared.delegate?.window!)!
        mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
        UIView.transition(with: mainwindow, duration: 0.55001, options: transition, animations: { () -> Void in
        }) { (finished) -> Void in
            
        }
        
        
    }
    
    
    @IBAction func closeLangView(_ sender: UIButton) {
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        if from == "login"{
            rootviewcontroller.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "rootnav")
            
        }else{
            rootviewcontroller.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "mainViewController")
        }
        
    }
    
}
