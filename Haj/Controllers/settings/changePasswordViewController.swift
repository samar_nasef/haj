//
//  changePasswordViewController.swift
//
//
//
//  Copyright © 2019 ITRoots. All rights reserved.
//

import UIKit

class changePasswordViewController: LocalizerViewController {
    
    @IBOutlet weak var currentPass: UITextField!
    @IBOutlet weak var newPass: UITextField!
    @IBOutlet weak var confirmPassview: UIView!
    @IBOutlet weak var confirmPass: UITextField!
    @IBOutlet weak var newPasswordView: UIView!
    @IBOutlet weak var currentPasswordView: UIView!
    @IBOutlet weak var saveBtn: UIButton!
    var sv: UIView?
    var from:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if L102Language.currentAppleLanguage() == "ar"{
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "leftArrow"), style: .plain, target: self, action: #selector(gobackTapped))
            self.navigationItem.rightBarButtonItem?.tintColor = .white
        }else{
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "leftArrowCopy-1"), style: .plain, target: self, action: #selector(gobackTapped))
            self.navigationItem.rightBarButtonItem?.tintColor = .white
            
        }
        
        if L102Language.currentAppleLanguage() == "ar"{
            
            langForApi =  1
            
        }else{
            langForApi =  2
        }
        
        currentPass.isSecureTextEntry = true
        newPass.isSecureTextEntry = true
        confirmPass.isSecureTextEntry = true
        
        if from == "activationChangePass" {
            currentPasswordView.isHidden = true
        }
        self.navigationItem.title = NSLocalizedString("changePass", comment: "")
        
        self.navigationItem.hidesBackButton = true
        
        currentPass.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("currentPass", comment: ""),attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        newPass.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("newPass", comment: ""),attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        confirmPass.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("confirmNewpass", comment: ""),attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        
        
        currentPasswordView.layer.cornerRadius = 8
        currentPasswordView.layer.shadowOpacity = 1
        currentPasswordView.layer.shadowRadius = 3.0
        currentPasswordView.layer.shadowOffset = CGSize(width: 0, height: 3)
        currentPasswordView.layer.shadowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        currentPasswordView.layer.borderWidth = 1
        currentPasswordView.layer.borderColor = #colorLiteral(red: 0.895331796, green: 0.895331796, blue: 0.895331796, alpha: 1)
        
        newPasswordView.layer.cornerRadius = 8
        newPasswordView.layer.shadowOpacity = 1
        newPasswordView.layer.shadowRadius = 3.0
        newPasswordView.layer.shadowOffset = CGSize(width: 0, height: 3)
        newPasswordView.layer.shadowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        newPasswordView.layer.borderWidth = 1
        newPasswordView.layer.borderColor = #colorLiteral(red: 0.895331796, green: 0.895331796, blue: 0.895331796, alpha: 1)
        
        confirmPassview.layer.cornerRadius = 8
        confirmPassview.layer.shadowOpacity = 1
        confirmPassview.layer.shadowRadius = 3.0
        confirmPassview.layer.shadowOffset = CGSize(width: 0, height: 3)
        confirmPassview.layer.shadowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        confirmPassview.layer.borderWidth = 1
        confirmPassview.layer.borderColor = #colorLiteral(red: 0.895331796, green: 0.895331796, blue: 0.895331796, alpha: 1)
        
        saveBtn.layer.cornerRadius = 21
        let gradient = CAGradientLayer()
        gradient.frame = saveBtn.bounds
        gradient.cornerRadius = 21
        gradient.startPoint = CGPoint(x:0.0, y:0.5)
        gradient.endPoint = CGPoint(x:1.0, y:0.5)
        
        let colorright = UIColor(red: 150.0 / 255.0, green: 0.0 / 255.0, blue: 5.0 / 255.0, alpha: 1.0).cgColor
        let colorleft = UIColor(red: 223.0 / 255.0, green: 18.0 / 255.0, blue: 28.0 / 255.0, alpha: 1.0).cgColor
        
        gradient.colors = [colorright, colorleft]
        
        saveBtn.layer.addSublayer(gradient)
        
        
    }
    
    @IBAction func savePass(_ sender: UIButton) {
        
        if from == "activationChangePass" {
            
            if newPass.text == ""
            {
                self.view.presentToast(msg: NSLocalizedString("Newpass", comment: ""))
            }else if newPass.text!.count  < 8 {
                self.view.presentToast(msg: NSLocalizedString("Pass1", comment: ""))
            }
                
            else if confirmPass.text == ""
            {
                self.view.presentToast(msg: NSLocalizedString("Confirmpass", comment: ""))
            }else if confirmPass.text!.count  < 8 {
                self.view.presentToast(msg: NSLocalizedString("Pass1", comment: ""))
            }
                
            else if confirmPass.text! != newPass.text! {
                self.view.presentToast(msg: NSLocalizedString("Notconfirmed", comment: ""))
            }
            else{
                
                if Reachability.isConnectedToNetwork() {
                    sv = UIViewController.displaySpinner(onView: self.view)
                    let jsonUrlString = changePassForForgetEndpoint
                    guard let url = URL(string: jsonUrlString) else { return }
                    do {
                        
                        let postObject = [
                            "UserID": UserDefaults.standard.integer(forKey: "HajUserID"),
                            "Password": newPass.text!,
                            "Lang": langForApi
                            ] as [String : Any]
                        
                        ApiService.callPost3(url: url, params: postObject, finish: self.finishchangePassForForgetPost)
                    }
                }
                else{
                    self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
                }
                
                
            }
            
            
        }else{
            
            
            if currentPass.text == ""
            {
                self.view.presentToast(msg: NSLocalizedString("Currentpass", comment: ""))
            }else if currentPass.text!.count  < 8 {
                self.view.presentToast(msg: NSLocalizedString("Pass1", comment: ""))
            }
            else if newPass.text == ""
            {
                self.view.presentToast(msg: NSLocalizedString("Newpass", comment: ""))
            }else if newPass.text!.count  < 8 {
                self.view.presentToast(msg: NSLocalizedString("Pass1", comment: ""))
            }
            else if confirmPass.text == ""
            {
                self.view.presentToast(msg: NSLocalizedString("Confirmpass", comment: ""))
            }else if confirmPass.text!.count  < 8 {
                self.view.presentToast(msg: NSLocalizedString("Pass1", comment: ""))
            }
            else if confirmPass.text! != newPass.text! {
                self.view.presentToast(msg:  NSLocalizedString("Notconfirmed", comment: ""))
            }
            else{
                
                if Reachability.isConnectedToNetwork() {
                    sv = UIViewController.displaySpinner(onView: self.view)
                    let jsonUrlString = changePasswordEndpoint
                    guard let url = URL(string: jsonUrlString) else { return }
                    do {
                        
                        let postObject = [
                            "UserID": UserDefaults.standard.integer(forKey: "HajUserID"),
                            "Password": currentPass.text!,
                            "NewPassword": newPass.text!,
                            "Lang": langForApi
                            ] as [String : Any]
                        
                        ApiService.callPost(url: url, params: postObject, finish: self.finishchangePassPost)
                    }
                }
                else{
                    self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
                }
            }
        }
    }
    
    func finishchangePassPost (message:String, data:Data?) -> Void
    {
        do
        {
            UIViewController.removeSpinner(spinner: self.sv!)
            if message == "Success" {
                if data != nil
                {
                    let response = try JSONDecoder().decode(changePassRes.self, from: data!)
                    print("register response.Status : \(response.Status)")
                    
                    if response.Status == 1 {
                        
                        DispatchQueue.main.async {
                            self.view.presentToast(msg: response.Message)
                            self.navigationController?.popViewController(animated: true)
                        }
                        
                    }
                    else {
                        DispatchQueue.main.async {
                            self.view.presentToast(msg: response.Message)
                        }
                    }
                    
                }
            }
            else{
                self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                print("userregistered for notifications failed")
            }
        }
        catch
        {
            print("userregistered for notifications Parse Error: \(error)")
        }
    }
    
    
    func finishchangePassForForgetPost (message:String, data:Data?) -> Void
    {
        do
        {
            UIViewController.removeSpinner(spinner: self.sv!)
            if message == "Success" {
                if data != nil
                {
                    let response = try JSONDecoder().decode(changePassForForgetRes.self, from: data!)
                    print("register response.Status : \(response.Status)")
                    
                    if response.Status == 1 {
                        
                        DispatchQueue.main.async {
                            self.view.presentToast(msg: response.Message)
                            
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let mainViewController = mainStoryBoard.instantiateViewController(withIdentifier: "rootnav") as! ViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate?
                            appDelegate!.window?.rootViewController = mainViewController
                            
                        }
                        
                    }
                    else {
                        DispatchQueue.main.async {
                            self.view.presentToast(msg: response.Message)
                        }
                    }
                    
                }
            }
            else{
                self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                print("userregistered for notifications failed")
            }
        }
        catch
        {
            print("userregistered for notifications Parse Error: \(error)")
        }
    }
    
    
    @IBAction func goback(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func gobackTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
