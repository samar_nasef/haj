//
//  settingsViewController.swift
//
//
//
//  Copyright © 2019 ITRoots. All rights reserved.
//

import UIKit

class settingsViewController: UIViewController {
    
    @IBOutlet weak var profileBtn: UIButton!
    @IBOutlet weak var logoutbtn: UIButton!
    @IBOutlet weak var aboutbtn: UIButton!
    @IBOutlet weak var chgpassbtn: UIButton!
    @IBOutlet weak var langbtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if L102Language.currentAppleLanguage() == "en" {
            
            profileBtn.contentHorizontalAlignment = .left
            logoutbtn.contentHorizontalAlignment = .left
            aboutbtn.contentHorizontalAlignment = .left
            chgpassbtn.contentHorizontalAlignment = .left
            langbtn.contentHorizontalAlignment = .left
            
        }else {
            
            profileBtn.contentHorizontalAlignment = .right
            logoutbtn.contentHorizontalAlignment = .right
            aboutbtn.contentHorizontalAlignment = .right
            chgpassbtn.contentHorizontalAlignment = .right
            langbtn.contentHorizontalAlignment = .right
        }
        
        drawNotificationBell()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.drawNotificationBell),
                                               name: NSNotification.Name(rawValue: "reloadnotificationCount"),
                                               object: nil)
        
        
    }
    
    
    @IBAction func Logout(_ sender: UIButton) {
        UserDefaults.standard.removeObject(forKey: "HajIsVerifiedStatus")
        UserDefaults.standard.removeObject(forKey: "HajUserID")
        UserDefaults.standard.removeObject(forKey: "HajUserDetails")
        
        DispatchQueue.main.async {
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let mainViewController = mainStoryBoard.instantiateViewController(withIdentifier: "rootnav") as! ViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate?
            appDelegate!.window?.rootViewController = mainViewController
        }
        
    }
    
    @IBAction func changeLang(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "changeLangSeg", sender: nil)
        
    }
    
    
    @objc func openNotification(sender: UIBarButtonItem){
        
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "notificationVc") as! notificationsViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
        
    }
    
    @objc  func drawNotificationBell(){
        let notificationButton = SSBadgeButton()
        notificationButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        notificationButton.setImage(UIImage(named: "notificationsButton-1")?.withRenderingMode(.alwaysTemplate), for: .normal)
        notificationButton.tintColor = .white
        
        if UserDefaults.standard.integer(forKey: "notificationCount") > 0 {
            
            notificationButton.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
            notificationButton.badge = String(UserDefaults.standard.integer(forKey: "notificationCount"))
        }
        
        notificationButton.addTarget(self, action: #selector(openNotification(sender:)), for: .touchUpInside)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: notificationButton)
    }
    
}
