//
//  signupVC.swift
//  Haj
//
//  Created by elsaid yousif on 11/27/19.
//  Copyright © 2019 ITRoots. All rights reserved.
//

import UIKit
//import iOSDropDown

class signupVC: LocalizerViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate ,UITextFieldDelegate {
    
    //    , UIPickerViewDelegate, UIPickerViewDataSource
    
    
    @IBOutlet weak var backbtnImg: UIButton!
    
    @IBOutlet weak var const10: NSLayoutConstraint!
    @IBOutlet weak var const9: NSLayoutConstraint!
    //    @IBOutlet weak var const7: NSLayoutConstraint!
    
    //    @IBOutlet weak var const11: NSLayoutConstraint!
    
    @IBOutlet weak var const12: NSLayoutConstraint!
    @IBOutlet weak var const8: NSLayoutConstraint!
    //    @IBOutlet weak var const4: UIImageView!
    
    @IBOutlet weak var conts6: NSLayoutConstraint!
    @IBOutlet weak var const5: NSLayoutConstraint!
    @IBOutlet weak var const3: NSLayoutConstraint!
    @IBOutlet weak var const2: NSLayoutConstraint!
    @IBOutlet weak var const1: NSLayoutConstraint!
    
    
    @IBOutlet weak var const13: NSLayoutConstraint!
    
    
    @IBOutlet weak var phonearview: UIView!
    
    @IBOutlet weak var backImg: UIImageView!
    
    @IBOutlet weak var camBtn: UIButton!
    
    //    @IBOutlet weak var camTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var camTopConstraint: NSLayoutConstraint!
    
    //    @IBOutlet weak var camRightConstrint: NSLayoutConstraint!
    
    @IBOutlet weak var userPic: UIImageView!
    
    
    @IBOutlet weak var camRightConstrint: NSLayoutConstraint!
    
    @IBOutlet weak var fullName: UITextField!
    @IBOutlet weak var fullNameView: UIView!
    
    @IBOutlet weak var jobView: UIView!
    @IBOutlet weak var job: UITextField!
    
    @IBOutlet weak var edaraView: UIView!
    @IBOutlet weak var edara: UITextField!
    
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var phone: UITextField!
    
    @IBOutlet weak var userNameView: UIView!
    @IBOutlet weak var userName: UITextField!
    
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var password: UITextField!
    
    @IBOutlet weak var confirmPasswordView: UIView!
    @IBOutlet weak var confirmPassword: UITextField!
    
    @IBOutlet weak var genderView: UIView!
    //    @IBOutlet weak var gender: DropDown!
    var genderVal : Int = 0
    
    var imagesource : Int = 0 //1camera , 2 photolibrary
    
    @IBOutlet weak var phoneenview: UIView!
    @IBOutlet weak var genderBtnText: UIButton!
    
    @IBOutlet weak var phoneentxt: UITextField!
    @IBOutlet weak var newAccountBtn: UIButton!
    
    
    
    @IBOutlet weak var newaccountleftConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var newaccountBottomConstarint: NSLayoutConstraint!
    var sv: UIView?
    var imageString = ""
    //    @IBOutlet weak var genderPicker: UIPickerView!
    
    //    let genderType = ["Red","Yellow"]
    //    var toolBar = UIToolbar()
    //    var picker  = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        leftArrowCopy3
        
        phone.delegate = self
        phoneentxt.delegate = self
        
        if L102Language.currentAppleLanguage() == "ar"{
            
            langForApi =  1
            phoneView.isHidden = false
            phoneenview.isHidden = true
            
        }else{
            langForApi =  2
            phoneView.isHidden = true
            phoneenview.isHidden = false
        }
        
        if L102Language.currentAppleLanguage() == "en" {
            
            backbtnImg.setImage(UIImage(named: "leftArrowCopy3"), for: .normal)
            genderBtnText.contentHorizontalAlignment = .left
            
            backImg.image = UIImage(named: "bg-with-logo")
        }else {
            
            backbtnImg.setImage(UIImage(named: "leftArrowCopy"), for: .normal)
            genderBtnText.contentHorizontalAlignment = .right
            backImg.image = UIImage(named: "bgggg")
        }
        //        fullName.delegate = self
        //        job.delegate = self
        //        edara.delegate = self
        //        email.delegate = self
        //        phone.delegate = self
        
        
        
        password.isSecureTextEntry = true
        
        confirmPassword.isSecureTextEntry = true
        
        
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(tap(tapgesture:)))
        userPic.isUserInteractionEnabled = true
        userPic.layer.cornerRadius = userPic.frame.height / 2
        userPic.addGestureRecognizer(tapgesture)
        
        let width  = UIScreen.main.bounds.width
        
        if width >= 700{
            const1.constant = 170
            const2.constant = 170
            const3.constant = 170
            const5.constant = 170
            conts6.constant = 170
            const12.constant = 170
            const13.constant = 170
            const8.constant = 170
            const9.constant = 170
            const10.constant = 170
            
        }
        
        
        if width == 1024{
            const1.constant = 200
            const2.constant = 200
            const3.constant = 200
            const5.constant = 200
            conts6.constant = 200
            const12.constant = 200
            const13.constant = 200
            const8.constant = 200
            const9.constant = 200
            const10.constant = 200
        }
        
        
        let height = UIScreen.main.bounds.height
        
        camRightConstrint.constant = (width / 2) - 70
        camTopConstraint.constant = (height / 6)
        
        fullNameView.layer.cornerRadius = 25
        jobView.layer.cornerRadius = 25
        edaraView.layer.cornerRadius = 25
        emailView.layer.cornerRadius = 25
        phoneView.layer.cornerRadius = 25
        phoneenview.layer.cornerRadius = 25
        userNameView.layer.cornerRadius = 25
        passwordView.layer.cornerRadius = 25
        confirmPasswordView.layer.cornerRadius = 25
        genderView.layer.cornerRadius = 25
        
        fullName.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("fullname", comment: ""),attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        job.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("jobname", comment: ""),attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        edara.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("orgOrDept", comment: ""),attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        email.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("email", comment: "") ,attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        phone.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("phoneNo", comment: "") ,attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        phoneentxt.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("phoneNo", comment: "") ,attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        
        password.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("pass", comment: "") ,attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        
        
        confirmPassword.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("conPass", comment: "")  ,attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        
        
        userName.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("username", comment: "") ,attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        newAccountBtn.layer.cornerRadius = 21
        
        let gradient = CAGradientLayer()
        gradient.frame = newAccountBtn.bounds
        gradient.cornerRadius = 21
        gradient.startPoint = CGPoint(x:0.0, y:0.5)
        gradient.endPoint = CGPoint(x:1.0, y:0.5)
        let colorright = UIColor(red: 150.0 / 255.0, green: 0.0 / 255.0, blue: 5.0 / 255.0, alpha: 1.0).cgColor
        let colorleft = UIColor(red: 223.0 / 255.0, green: 18.0 / 255.0, blue: 28.0 / 255.0, alpha: 1.0).cgColor
        
        gradient.colors = [colorright, colorleft]
        
        newAccountBtn.layer.addSublayer(gradient)
        _ = width/4
        
        newaccountleftConstraint.constant =  (width / 4) + 30
        newaccountBottomConstarint.constant = (height/15) - 20
        
        
    }
    @IBOutlet weak var backbtnimg: UIButton!
    
    
    @IBAction func backToLogin(_ sender: Any) {
        DispatchQueue.main.async {
            print("backtologin")
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let mainViewController = mainStoryBoard.instantiateViewController(withIdentifier: "rootnav") as! ViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate?
            appDelegate!.window?.rootViewController = mainViewController
        }
        
        
    }
    
    @IBAction func genderClicked(_ sender: Any) {
        
        let optionMenu = UIAlertController(title: nil, message:NSLocalizedString("selectgender", comment: "") , preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction(title: NSLocalizedString("male", comment: "") , style: .default, handler: { (action) -> Void in
            
            self.genderVal = 1
            self.genderBtnText.setTitle(NSLocalizedString("male", comment: ""), for: .normal)
        })
        let saveAction = UIAlertAction(title: NSLocalizedString("female", comment: "") , style: .default, handler: { (action) -> Void in
            
            self.genderVal = 2
            self.genderBtnText.setTitle(NSLocalizedString("female", comment: ""), for: .normal)
        })
        
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel,handler: { (action) -> Void in
            
        })
        
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        
        
        optionMenu.popoverPresentationController?.sourceView = self.view;
        optionMenu.popoverPresentationController?.sourceRect = CGRect(x: 0,y: UIScreen.main.bounds.height - 100 ,width: UIScreen.main.bounds.width,height: 1.0);
        
        self.present(optionMenu, animated: true, completion: nil)
        
    }
    
    
    @IBAction func createNewAccount(_ sender: Any) {
        
        
        if  fullName.text == "" {
            self.view.presentToast(msg: NSLocalizedString("entername", comment: ""))
        }else if job.text == ""
        {
            self.view.presentToast(msg:  NSLocalizedString("enterjob", comment: "") )
        }else if edara.text == ""
        {
            self.view.presentToast(msg:  NSLocalizedString("Enterorg", comment: "") )
        }else if genderVal == 0
        {
            self.view.presentToast(msg:  NSLocalizedString("Entergen", comment: "") )
        }else if email.text == ""
        {
            self.view.presentToast(msg:  NSLocalizedString("Entermail", comment: "") )
        }else if isValidEmail(emailStr: email.text!) == false
        {
            self.view.presentToast(msg:  NSLocalizedString("Entervailemail", comment: "") )
        }
        else if L102Language.currentAppleLanguage() == "ar" && phone.text == ""
        {
            
            self.view.presentToast(msg:  NSLocalizedString("phoneErrno", comment: "") )
            
        }else if L102Language.currentAppleLanguage() == "ar" && isValidPhone(phoneStr: phone.text!.replacedArabicDigitsWithEnglish) == false
        {
            self.view.presentToast(msg:  NSLocalizedString("Vaildphone", comment: "") )
        }else if L102Language.currentAppleLanguage() == "en" && phoneentxt.text == ""
        {
            self.view.presentToast(msg:  NSLocalizedString("phoneErrno", comment: "") )
            
        }else if L102Language.currentAppleLanguage() == "en" && isValidPhone(phoneStr: phoneentxt.text!.replacedArabicDigitsWithEnglish) == false
        {
            self.view.presentToast(msg:  NSLocalizedString("Vaildphone", comment: "") )
        }
            
        else if userName.text == ""
        {
            self.view.presentToast(msg:  NSLocalizedString("Enteruser", comment: "") )
        }else if password.text == ""
        {
            self.view.presentToast(msg:  NSLocalizedString("enterpass", comment: "") )
        }else if password.text!.count  < 8 {
            self.view.presentToast(msg:  NSLocalizedString("Pass1", comment: "") )
        }
        else if confirmPassword.text == ""
        {
            self.view.presentToast(msg:  NSLocalizedString("Confirmpass", comment: "") )
        }else if confirmPassword.text!.count  < 8 {
            self.view.presentToast(msg:  NSLocalizedString("Pass1", comment: "") )
        }
        else if confirmPassword.text! != password.text! {
            self.view.presentToast(msg:  NSLocalizedString("Notconfirmed", comment: "") )
        }
        else{
            
            if Reachability.isConnectedToNetwork() {
                sv = UIViewController.displaySpinner(onView: self.view)
                let jsonUrlString = SignUpEndpoint
                guard let url = URL(string: jsonUrlString) else { return }
                do {
                    
                    var firebase_token = ""
                    if let firebase_token_val = UserDefaults.standard.string(forKey: "firebase_token")
                    {
                        firebase_token = firebase_token_val
                    }
                    
                    var userPhonetoapi =  ""
                    
                    if L102Language.currentAppleLanguage() == "ar"  {
                        userPhonetoapi = "966"+phone.text!.replacedArabicDigitsWithEnglish
                    }else{
                        userPhonetoapi = "966"+phoneentxt.text!.replacedArabicDigitsWithEnglish
                    }
                    let postObject = [
                        "FullName": fullName.text!,
                        "Username": userName.text!,
                        "Password": password.text!,
                        "Phone": userPhonetoapi,
                        "Email": email.text!,
                        "PositionName": job.text!,
                        "OrgOrDeptName": edara.text!,
                        "ProfileImage": imageString.encodeURIComponent() ?? "",
                        "ProfileImageExtension": "jpg",
                        "Gender": genderVal,
                        "MobileDeviceID": firebase_token,
                        "MobileType": 2,
                        "Lang": langForApi
                        
                        ] as [String : Any]
                    
                    ApiService.callPost3(url: url, params: postObject, finish: self.finishSignUpPost)
                }
            }
            else{
                self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
            }
            
        }
        
    }
    
    func finishSignUpPost (message:String, data:Data?) -> Void
    {
        do
        {
            UIViewController.removeSpinner(spinner: self.sv!)
            if message == "Success" {
                if data != nil
                {
                    let response = try JSONDecoder().decode(signupApiRes.self, from: data!)
                    print("register response.Status : \(response.Status)")
                    
                    if response.Status == 1 {
                        
                        DispatchQueue.main.async {
                            self.view.presentToast(msg: response.Message)
                            UserDefaults.standard.set(response.UserID, forKey: "HajUserID")
                            
                            self.performSegue(withIdentifier: "activationSegue", sender: nil)
                        }
                        
                        
                    }
                    else {
                        DispatchQueue.main.async {
                            self.view.presentToast(msg: response.Message)
                        }
                    }
                    
                }
            }
            else{
                self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                print("userregistered for notifications failed")
            }
        }
        catch
        {
            print("userregistered for notifications Parse Error: \(error)")
        }
    }
    
    func isValidEmail(emailStr:String) -> Bool {
        //true if valid email
        //false if email not valid
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
    
    func isValidPhone(phoneStr:String) -> Bool {
        
        //        let regularExpressionForPhone = "^\\d{3}-\\d{3}-\\d{4}$"
        let regularExpressionForPhone = "[0-9]{9,10}"
        let testPhone = NSPredicate(format:"SELF MATCHES %@", regularExpressionForPhone)
        return testPhone.evaluate(with: phoneStr)
    }
    
    
    @IBAction func takePhoto(_ sender: UIButton) {
        
        let optionMenu = UIAlertController(title: nil, message: NSLocalizedString("SelectImageSource", comment: ""), preferredStyle: .actionSheet)
        
        
        let deleteAction = UIAlertAction(title: NSLocalizedString("UseCamera", comment: ""), style: .default, handler: { (action) -> Void in
            
            self.imagesource = 1
            self.chooseImage()
            
        })
        let saveAction = UIAlertAction(title: NSLocalizedString("LoadfromLibrary", comment: ""), style: .default, handler: { (action) -> Void in
            
            self.imagesource = 2
            self.chooseImage()
            
        })
        
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel,handler: { (action) -> Void in
            print("cancel button tapped")
            
        })
        
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
        
    }
    
    func chooseImage(){
        
        let imgPicker = UIImagePickerController()
        imgPicker.delegate = self
        if imagesource == 1{
            imgPicker.sourceType = .camera
            imgPicker.allowsEditing = false
            imgPicker.showsCameraControls = true
        }else if imagesource == 2
        {
            imgPicker.sourceType = .photoLibrary
        }
        
        self.present(imgPicker, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey :
        Any]) {
        if let img = info[UIImagePickerController.InfoKey.originalImage] as?
            UIImage {
            
            self.userPic.image = img.fixOrientation()
            
            
            
            let imageData = img.jpeg(.lowest)
            let base64String = imageData?.base64EncodedString(options: .init(rawValue: 0))
            
            imageString = base64String!
            
            self.dismiss(animated: true, completion: nil)
            
        }
    }
    
    
    @objc func tap(tapgesture:UITapGestureRecognizer) {
        
        
        let optionMenu = UIAlertController(title: nil, message: NSLocalizedString("SelectImageSource", comment: ""), preferredStyle: .actionSheet)
        
        
        let deleteAction = UIAlertAction(title: NSLocalizedString("UseCamera", comment: ""), style: .default, handler: { (action) -> Void in
            
            self.imagesource = 1
            self.chooseImage()
            
        })
        let saveAction = UIAlertAction(title: NSLocalizedString("LoadfromLibrary", comment: ""), style: .default, handler: { (action) -> Void in
            
            self.imagesource = 2
            self.chooseImage()
            
        })
        
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel,handler: { (action) -> Void in
            print("cancel button tapped")
            
            
        })
        
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        
        
        self.present(optionMenu, animated: true, completion: nil)
        
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 10
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
}
