//
//  ViewController.swift
//
//
//
//  Copyright © 2019 ITRoots. All rights reserved.
//

import UIKit


class ViewController: LocalizerViewController ,UITextFieldDelegate {
    
    
    @IBOutlet weak var newlwea: NSLayoutConstraint!
    @IBOutlet weak var forgetleadingPass: NSLayoutConstraint!
    @IBOutlet weak var pasleadingconst: NSLayoutConstraint!
    @IBOutlet weak var userleadingconstraint: NSLayoutConstraint!
    @IBOutlet weak var backImg: UIImageView!
    @IBOutlet weak var lang: UIButton!
    var arabic = false
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var usernameView: UIView!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var labeltopConstaint: NSLayoutConstraint!
    @IBOutlet weak var loginleftConstarint: NSLayoutConstraint!
    @IBOutlet weak var loginbottomConstraint: NSLayoutConstraint!
    var sv: UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if L102Language.currentAppleLanguage() == "ar"{
            langForApi =  1
        }else{
            langForApi =  2
        }
        
        if L102Language.currentAppleLanguage() == "en" {
            lang.setImage(UIImage(named: "ar"), for: .normal)
            backImg.image = UIImage(named: "bg-with-logo")
            
            password.attributedPlaceholder = NSAttributedString(string: "Password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
            
            userName.attributedPlaceholder = NSAttributedString(string: "User  name",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
            
        } else {
            lang.setImage(UIImage(named:"en"), for: .normal)
            backImg.image = UIImage(named: "bgggg")
            
            password.attributedPlaceholder = NSAttributedString(string: "كلمة المرور",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
            
            userName.attributedPlaceholder = NSAttributedString(string: "اسم المستخدم",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
            
            
        }
        
        
        
        passwordView.layer.cornerRadius = 25
        usernameView.layer.cornerRadius = 25
        loginBtn.layer.cornerRadius = 21
        let gradient = CAGradientLayer()
        gradient.frame = loginBtn.bounds
        gradient.cornerRadius = 21
        gradient.startPoint = CGPoint(x:0.0, y:0.5)
        gradient.endPoint = CGPoint(x:1.0, y:0.5)
        let colorright = UIColor(red: 150.0 / 255.0, green: 0.0 / 255.0, blue: 5.0 / 255.0, alpha: 1.0).cgColor
        let colorleft = UIColor(red: 223.0 / 255.0, green: 18.0 / 255.0, blue: 28.0 / 255.0, alpha: 1.0).cgColor
        gradient.colors = [colorright, colorleft]
        loginBtn.layer.addSublayer(gradient)
        let width  = UIScreen.main.bounds.width
        let height = UIScreen.main.bounds.height
        _ = width/4
        
        if width >= 700{
            userleadingconstraint.constant = 170
            newlwea.constant = 170
            forgetleadingPass.constant = 170
            pasleadingconst.constant = 170
        }
        
        if width == 1024{
            userleadingconstraint.constant = 200
            newlwea.constant = 200
            forgetleadingPass.constant = 200
            pasleadingconst.constant = 200
        }
        
        labeltopConstaint.constant = (height / 3) - 50
        loginleftConstarint.constant =  (width / 4) + 30
        loginbottomConstraint.constant = (height/15) - 20
        password.isSecureTextEntry = true
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height / 2
            }
        }
    }
    
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    @objc func hideKeyboard(){
        view.endEditing(true)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    @IBAction func loginClicked(_ sender: UIButton) {
        
        
        if  userName.text == "" {
            self.view.presentToast(msg:  NSLocalizedString("Enternamexx", comment: "") )
        }else if password.text == ""
        {
            self.view.presentToast(msg: NSLocalizedString("enterpass", comment: "") )
        }
        else{
            
            if Reachability.isConnectedToNetwork() {
                sv = UIViewController.displaySpinner(onView: self.view)
                let jsonUrlString = loginEndpoint+userName.text!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!+"&password="+password.text!+"&lang=\(String(langForApi))"
                
                guard let url = URL(string: jsonUrlString) else { return }
                
                URLSession.shared.dataTask(with: url) { (data, response, err) in
                    UIViewController.removeSpinner(spinner: self.sv!)
                    if (err != nil){
                        self.view.presentToast(msg: NSLocalizedString("serverErr", comment: "") )
                        return
                    }
                    if let status = response as? HTTPURLResponse {
                        print("request status \(status.statusCode)")
                        if status.statusCode != 200{
                            self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                            return
                        }
                    }
                    guard let data = data else { return }
                    do {
                        print(data)
                        let response = try JSONDecoder().decode(loginApiRes.self, from: data)
                        
                        if response.Status == 1 {
                            
                            if response.UserDetails.IsVerified == true {
                                DispatchQueue.main.async {
                                    self.view.presentToast(msg: response.Message)
                                    UserDefaults.standard.set(response.UserDetails.IsVerified, forKey: "HajIsVerifiedStatus")
                                    UserDefaults.standard.set(response.UserDetails.SID, forKey: "HajUserID")
                                    
                                    let x = userDetails2(SID: response.UserDetails.SID, FullName: response.UserDetails.FullName, Username: response.UserDetails.Username, Phone: response.UserDetails.Phone, Email: response.UserDetails.Email, LastSeenDate: response.UserDetails.LastSeenDate, ProfileImage: response.UserDetails.ProfileImage, NotificationEnabled: response.UserDetails.NotificationEnabled, VerificationCode: response.UserDetails.VerificationCode, IsVerified: response.UserDetails.IsVerified, UserType: response.UserDetails.UserType, CreatedBy: response.UserDetails.CreatedBy, Status: response.UserDetails.Status, LastUpdatedDate: response.UserDetails.LastUpdatedDate, LastUpdatedBy: response.UserDetails.LastUpdatedBy,PositionName: response.UserDetails.PositionName,OrgOrDeptName: response.UserDetails.OrgOrDeptName,Token:response.UserDetails.Token,password:self.password.text!)
                                    let encoder = JSONEncoder()
                                    if let encoded = try? encoder.encode(x) {
                                        let defaults = UserDefaults.standard
                                        defaults.set(encoded, forKey: "HajUserDetails")
                                    }
                                    
                                    
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let mainViewController = mainStoryBoard.instantiateViewController(withIdentifier: "mainViewController") as! UITabBarController
                                    mainViewController.selectedIndex = 3
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate?
                                    appDelegate!.window?.rootViewController = mainViewController
                                    
                                    
                                }
                            }
                            
                        }
                        else{
                            DispatchQueue.main.async {
                                self.view.presentToast(msg: response.Message)
                            }
                        }
                    }
                        
                    catch let jsonErr {
                        print("Error serializing json:", jsonErr)
                        self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                    }
                }.resume()
            }
            else{
                self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
            }
            
        }
        
        
    }
    
    
    @IBAction func changeLang(_ sender: UIButton) {
        self.performSegue(withIdentifier: "changeLangSeg2", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "changeLangSeg2" {
            if let destinationVC = segue.destination as? changeLangViewController {
                destinationVC.from = "login"
                
            }
        }
    }
    
    
}
