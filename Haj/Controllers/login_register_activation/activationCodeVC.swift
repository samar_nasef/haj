//
//  activationCodeVC.swift
//
//
//
//  Copyright © 2019 ITRoots. All rights reserved.
//

import UIKit

class activationCodeVC: LocalizerViewController,UITextFieldDelegate {
    
    var from:String = ""
    
    @IBOutlet weak var const2: NSLayoutConstraint!
    @IBOutlet weak var const1: NSLayoutConstraint!
    @IBOutlet weak var enCodeview: UIView!
    @IBOutlet weak var enlbl2: UILabel!
    @IBOutlet weak var enlbl3: UILabel!
    @IBOutlet weak var enlbl1: UILabel!
    @IBOutlet weak var enlbl4: UILabel!
    @IBOutlet weak var encode3: UITextField!
    @IBOutlet weak var encode2: UITextField!
    @IBOutlet weak var encode4: UITextField!
    @IBOutlet weak var encode1: UITextField!
    @IBOutlet weak var arCodeView: UIView!
    @IBOutlet weak var backImg: UIImageView!
    @IBOutlet weak var code4BackLabel: UILabel!
    @IBOutlet weak var code3BackLabel: UILabel!
    @IBOutlet weak var code2BackLabel: UILabel!
    @IBOutlet weak var code1BackLabel: UILabel!
    @IBOutlet weak var ressendView: UIView!
    @IBOutlet weak var activationView: UIView!
    @IBOutlet weak var code1: UITextField!
    @IBOutlet weak var code2: UITextField!
    @IBOutlet weak var code3: UITextField!
    @IBOutlet weak var code4: UITextField!
    @IBOutlet weak var resendCodeBtn: UIButton!
    @IBOutlet weak var timerLbl: UILabel!
    @IBOutlet weak var sendbtn: UIButton!
    @IBOutlet weak var activationLeftConst: NSLayoutConstraint!
    @IBOutlet weak var botomConst: NSLayoutConstraint!
    var sv: UIView?
    var seconds = 60
    var timer = Timer()
    
    var activationCodeForForget:Int = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if L102Language.currentAppleLanguage() == "ar"{
            code1.delegate = self
            code2.delegate = self
            code3.delegate = self
            code4.delegate = self
            code1.becomeFirstResponder()
            
            code1.addTarget(self, action: #selector(code1Changed), for: .editingChanged)
            code2.addTarget(self, action: #selector(code2Changed), for: .editingChanged)
            code3.addTarget(self, action: #selector(code3Changed), for: .editingChanged)
            code4.addTarget(self, action: #selector(code4Changed), for: .editingChanged)
            
            activationView.layer.cornerRadius = 25
            activationView.isHidden = false
            enCodeview.isHidden = true
            
            langForApi =  1
            
        }else{
            
            enCodeview.layer.cornerRadius = 25
            
            encode1.delegate = self
            encode2.delegate = self
            encode3.delegate = self
            encode4.delegate = self
            encode1.becomeFirstResponder()
            
            encode1.addTarget(self, action: #selector(encode1Changed), for: .editingChanged)
            encode2.addTarget(self, action: #selector(encode2Changed), for: .editingChanged)
            encode3.addTarget(self, action: #selector(encode3Changed), for: .editingChanged)
            encode4.addTarget(self, action: #selector(encode4Changed), for: .editingChanged)
            
            enCodeview.isHidden = false
            activationView.isHidden = true
            
            langForApi =  2
        }
        
        if L102Language.currentAppleLanguage() == "en" {
            
            backImg.image = UIImage(named: "bg-with-logo")
        }else {
            
            backImg.image = UIImage(named: "bgggg")
        }
        
        if from == "forgetPass"{
            ressendView.isHidden = true
        }
        
        
        runTimer()
        sendbtn.layer.cornerRadius = 21
        let gradient = CAGradientLayer()
        gradient.frame = sendbtn.bounds
        gradient.cornerRadius = 21
        gradient.startPoint = CGPoint(x:0.0, y:0.5)
        gradient.endPoint = CGPoint(x:1.0, y:0.5)
        let colorright = UIColor(red: 150.0 / 255.0, green: 0.0 / 255.0, blue: 5.0 / 255.0, alpha: 1.0).cgColor
        let colorleft = UIColor(red: 223.0 / 255.0, green: 18.0 / 255.0, blue: 28.0 / 255.0, alpha: 1.0).cgColor
        
        gradient.colors = [colorright, colorleft]
        
        sendbtn.layer.addSublayer(gradient)
        
        let width  = UIScreen.main.bounds.width
        
        
        if width >= 700{
            const1.constant = 170
            const2.constant = 170
        }
        
        if width == 1024{
            const1.constant = 200
            const2.constant = 200
        }
        
        
        let height = UIScreen.main.bounds.height
        activationLeftConst.constant =  (width / 4) + 30
        botomConst.constant = (height/15) - 20
        
        
    }
    @objc func updateTimer() {
        if seconds != 0
        {
            DispatchQueue.main.async {
                self.seconds -= 1
                self.timerLbl.text = " 00:\(self.seconds)"
            }
            
        }
        else{
            DispatchQueue.main.async {
                self.timer.invalidate()
            }
            
        }
    }
    func runTimer() {
        self.seconds = 60
        DispatchQueue.main.async {
            self.resendCodeBtn.isEnabled = false
            self.resendCodeBtn.setTitleColor(UIColor.lightGray, for: .normal)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 60) {
            DispatchQueue.main.async {
                self.resendCodeBtn.isEnabled = true
                self.resendCodeBtn.setTitleColor(#colorLiteral(red: 0.8117647059, green: 0.631372549, blue: 0.3333333333, alpha: 1), for: .normal)
            }
        }
        
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
        }
    }
    
    @objc func code1Changed() {
        
        if code1.text == "" {
            print("code 1 empty")
        }else{
            code2.becomeFirstResponder()
            
            code2.textColor = #colorLiteral(red: 0.8117647059, green: 0.631372549, blue: 0.3333333333, alpha: 1)
            code2BackLabel.backgroundColor = #colorLiteral(red: 0.8117647059, green: 0.631372549, blue: 0.3333333333, alpha: 1)
            
            code1.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            code1BackLabel.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        }
        
    }
    @objc func code2Changed() {
        
        if code2.text == "" {
            code1.becomeFirstResponder()
            code1.textColor = #colorLiteral(red: 0.8117647059, green: 0.631372549, blue: 0.3333333333, alpha: 1)
            code1BackLabel.backgroundColor = #colorLiteral(red: 0.8117647059, green: 0.631372549, blue: 0.3333333333, alpha: 1)
            
            code2.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            code2BackLabel.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        }else{
            code3.becomeFirstResponder()
            code3.textColor = #colorLiteral(red: 0.8117647059, green: 0.631372549, blue: 0.3333333333, alpha: 1)
            code3BackLabel.backgroundColor = #colorLiteral(red: 0.8117647059, green: 0.631372549, blue: 0.3333333333, alpha: 1)
            
            code2.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            code2BackLabel.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        }
    }
    @objc func code3Changed() {
        if code3.text == "" {
            code2.becomeFirstResponder()
            code2.textColor = #colorLiteral(red: 0.8117647059, green: 0.631372549, blue: 0.3333333333, alpha: 1)
            code2BackLabel.backgroundColor = #colorLiteral(red: 0.8117647059, green: 0.631372549, blue: 0.3333333333, alpha: 1)
            
            code3.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            code3BackLabel.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        }else{
            code4.becomeFirstResponder()
            code4.textColor = #colorLiteral(red: 0.8117647059, green: 0.631372549, blue: 0.3333333333, alpha: 1)
            code4BackLabel.backgroundColor = #colorLiteral(red: 0.8117647059, green: 0.631372549, blue: 0.3333333333, alpha: 1)
            
            code3.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            code3BackLabel.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        }
        
    }
    
    @objc func code4Changed() {
        if code4.text == "" {
            code3.becomeFirstResponder()
            
            code3.textColor = #colorLiteral(red: 0.8117647059, green: 0.631372549, blue: 0.3333333333, alpha: 1)
            code3BackLabel.backgroundColor = #colorLiteral(red: 0.8117647059, green: 0.631372549, blue: 0.3333333333, alpha: 1)
            
            code4.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            code4BackLabel.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        }else{
            print("call api ")
            
        }
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 1
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    
    
    @IBAction func sendActivationCode(_ sender: Any) {
        if L102Language.currentAppleLanguage() == "ar" && (code1.text == "" ||  code2.text == "" ||  code3.text == "" ||  code4.text == "") {
            self.view.presentToast(msg: NSLocalizedString("Enetercode", comment: ""))
        }else if L102Language.currentAppleLanguage() == "en" && (encode1.text == "" ||  encode2.text == "" ||  encode3.text == "" ||  encode4.text == "") {
            self.view.presentToast(msg: NSLocalizedString("Enetercode", comment: ""))
        }
        else {
            var codeToApi = ""
            
            if L102Language.currentAppleLanguage() == "ar"{
                codeToApi = code1.text! + code2.text! + code3.text! + code4.text!
            }else{
                codeToApi = encode1.text! + encode2.text! + encode3.text! + encode4.text!
            }
            
            if from == "forgetPass"{
                if Int(codeToApi) == activationCodeForForget{
                    print("vaild code")
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "changePass", sender: nil)
                    }
                }else{
                    self.view.presentToast(msg: NSLocalizedString("vaildcode", comment: ""))
                }
                
            }else{
                if Reachability.isConnectedToNetwork() {
                    sv = UIViewController.displaySpinner(onView: self.view)
                    let jsonUrlString = activationCodeEndpoint+codeToApi+"&user=\(UserDefaults.standard.integer(forKey: "HajUserID"))"+"&lang=\(String(langForApi))"
                    
                    guard let url = URL(string: jsonUrlString) else { return }
                    URLSession.shared.dataTask(with: url) { (data, response, err) in
                        UIViewController.removeSpinner(spinner: self.sv!)
                        if (err != nil){
                            self.view.presentToast(msg: NSLocalizedString("serverErr", comment: "") )
                            return
                        }
                        if let status = response as? HTTPURLResponse {
                            print("request status \(status.statusCode)")
                            if status.statusCode != 200{
                                self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                                return
                            }
                        }
                        guard let data = data else { return }
                        do {
                            print(data)
                            let response = try JSONDecoder().decode(activationApiRes.self, from: data)
                            
                            if response.Status == 1 {
                                
                                DispatchQueue.main.async {
                                    self.view.presentToast(msg: response.Message)
                                    
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let mainViewController = mainStoryBoard.instantiateViewController(withIdentifier: "rootnav") as! ViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate?
                                    appDelegate!.window?.rootViewController = mainViewController
                                    
                                }
                                
                                
                            }
                            else{
                                DispatchQueue.main.async {
                                    self.view.presentToast(msg: response.Message)
                                }
                            }
                        }
                            
                        catch let jsonErr {
                            print("Error serializing json:", jsonErr)
                            self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                        }
                    }.resume()
                }
                else{
                    
                    self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
                }
            }
            
            
        }
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "changePass" {
            if let destinationVC = segue.destination as? changePasswordViewController {
                
                destinationVC.from = "activationChangePass"
                
                
            }
        }
        
        
        
    }
    
    
    @IBAction func resendCode(_ sender: UIButton) {
        
        if Reachability.isConnectedToNetwork() {
            sv = UIViewController.displaySpinner(onView: self.view)
            let jsonUrlString = resendactivationCodeEndpoint+"\(UserDefaults.standard.integer(forKey: "HajUserID"))"+"&lang=\(String(langForApi))"
            
            guard let url = URL(string: jsonUrlString) else { return }
            URLSession.shared.dataTask(with: url) { (data, response, err) in
                UIViewController.removeSpinner(spinner: self.sv!)
                if (err != nil){
                    self.view.presentToast(msg: NSLocalizedString("serverErr", comment: "") )
                    return
                }
                if let status = response as? HTTPURLResponse {
                    print("request status \(status.statusCode)")
                    if status.statusCode != 200{
                        self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                        return
                    }
                }
                guard let data = data else { return }
                do {
                    print(data)
                    let response = try JSONDecoder().decode(resendActivationRes.self, from: data)
                    print("resendActivation response.Status = \(response.Status)")
                    
                    if response.Status == 1 {
                        
                        DispatchQueue.main.async {
                            self.view.presentToast(msg: response.Message)
                            self.runTimer()
                        }
                        
                    }
                    else{
                        DispatchQueue.main.async {
                            self.view.presentToast(msg: response.Message)
                        }
                    }
                }
                    
                catch let jsonErr {
                    print("Error serializing json:", jsonErr)
                    self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                }
            }.resume()
        }
        else{
            self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
        }
        
        
    }
    
    @objc func encode1Changed() {
        
        if encode1.text == "" {
            print("code 1 empty")
        }else{
            encode2.becomeFirstResponder()
            
            encode2.textColor = #colorLiteral(red: 0.8117647059, green: 0.631372549, blue: 0.3333333333, alpha: 1)
            enlbl2.backgroundColor = #colorLiteral(red: 0.8117647059, green: 0.631372549, blue: 0.3333333333, alpha: 1)
            
            encode1.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            enlbl1.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        }
        
    }
    @objc func encode2Changed() {
        
        if encode2.text == "" {
            encode1.becomeFirstResponder()
            encode1.textColor = #colorLiteral(red: 0.8117647059, green: 0.631372549, blue: 0.3333333333, alpha: 1)
            enlbl1.backgroundColor = #colorLiteral(red: 0.8117647059, green: 0.631372549, blue: 0.3333333333, alpha: 1)
            
            encode2.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            enlbl2.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        }else{
            encode3.becomeFirstResponder()
            encode3.textColor = #colorLiteral(red: 0.8117647059, green: 0.631372549, blue: 0.3333333333, alpha: 1)
            enlbl3.backgroundColor = #colorLiteral(red: 0.8117647059, green: 0.631372549, blue: 0.3333333333, alpha: 1)
            
            encode2.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            enlbl2.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        }
    }
    @objc func encode3Changed() {
        
        if encode3.text == "" {
            encode2.becomeFirstResponder()
            encode2.textColor = #colorLiteral(red: 0.8117647059, green: 0.631372549, blue: 0.3333333333, alpha: 1)
            enlbl2.backgroundColor = #colorLiteral(red: 0.8117647059, green: 0.631372549, blue: 0.3333333333, alpha: 1)
            
            encode3.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            enlbl3.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        }else{
            encode4.becomeFirstResponder()
            encode4.textColor = #colorLiteral(red: 0.8117647059, green: 0.631372549, blue: 0.3333333333, alpha: 1)
            enlbl4.backgroundColor = #colorLiteral(red: 0.8117647059, green: 0.631372549, blue: 0.3333333333, alpha: 1)
            
            encode3.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            enlbl3.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        }
        
    }
    
    @objc func encode4Changed() {
        
        if encode4.text == "" {
            encode3.becomeFirstResponder()
            
            encode3.textColor = #colorLiteral(red: 0.8117647059, green: 0.631372549, blue: 0.3333333333, alpha: 1)
            enlbl3.backgroundColor = #colorLiteral(red: 0.8117647059, green: 0.631372549, blue: 0.3333333333, alpha: 1)
            
            encode4.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            enlbl4.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        }else{
            print("call api ")
            
            
        }
        
    }
    
}
