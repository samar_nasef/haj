//
//  forgetPasswordViewController.swift
//
//
//
//  Copyright © 2019 ITRoots. All rights reserved.
//

import UIKit

class forgetPasswordViewController: LocalizerViewController {
    
    
    @IBOutlet weak var backbtnImg: UIButton!
    @IBOutlet weak var const1: NSLayoutConstraint!
    @IBOutlet weak var const2: NSLayoutConstraint!
    @IBOutlet weak var backImg: UIImageView!
    var sv: UIView?
    var forgetCode:Int = -1
    @IBOutlet weak var sendleftConstrint: NSLayoutConstraint!
    @IBOutlet weak var sendBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var mailText: UITextField!
    @IBOutlet weak var topLabelConstraint: NSLayoutConstraint!
    @IBOutlet weak var mailView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if L102Language.currentAppleLanguage() == "ar"{
            
            langForApi =  1
            
        }else{
            langForApi =  2
        }
        
        if L102Language.currentAppleLanguage() == "en" {
            
            backbtnImg.setImage(UIImage(named: "leftArrowCopy3"), for: .normal)
            backImg.image = UIImage(named: "bg-with-logo")
        }else {
            backbtnImg.setImage(UIImage(named: "leftArrowCopy"), for: .normal)
            backImg.image = UIImage(named: "bgggg")
        }
        
        mailView.layer.cornerRadius = 25
        
        mailText.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("email", comment: "") ,attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        
        let width  = UIScreen.main.bounds.width
        
        if width >= 700{
            const1.constant = 170
            const2.constant = 170
            
        }
        
        if width == 1024{
            const1.constant = 200
            const2.constant = 200
        }
        
        
        let height = UIScreen.main.bounds.height
        sendBtn.layer.cornerRadius = 21
        
        let gradient = CAGradientLayer()
        gradient.frame = sendBtn.bounds
        gradient.cornerRadius = 21
        gradient.startPoint = CGPoint(x:0.0, y:0.5)
        gradient.endPoint = CGPoint(x:1.0, y:0.5)
        
        let colorright = UIColor(red: 150.0 / 255.0, green: 0.0 / 255.0, blue: 5.0 / 255.0, alpha: 1.0).cgColor
        let colorleft = UIColor(red: 223.0 / 255.0, green: 18.0 / 255.0, blue: 28.0 / 255.0, alpha: 1.0).cgColor
        
        gradient.colors = [colorright, colorleft]
        
        sendBtn.layer.addSublayer(gradient)
        
        _ = width/4
        
        topLabelConstraint.constant = (height / 3) - 50
        sendleftConstrint.constant =  (width / 4) + 30
        sendBottomConstraint.constant = (height/15) - 20
        
    }
    
    
    @IBAction func forgetPass(_ sender: UIButton) {
        
        if mailText.text == ""
        {
            self.view.presentToast(msg: NSLocalizedString("Entermail", comment: "") )
        }else if isValidEmail(emailStr: mailText.text!) == false
        {
            self.view.presentToast(msg: NSLocalizedString("Entervailemail", comment: "") )
        }else{
            print("call api")
            
            if Reachability.isConnectedToNetwork() {
                sv = UIViewController.displaySpinner(onView: self.view)
                
                let jsonUrlString = forgetPasswordEndpoint + mailText.text!+"&lang=\(String(langForApi))"
                
                guard let url = URL(string: jsonUrlString) else { return }
                URLSession.shared.dataTask(with: url) { (data, response, err) in
                    UIViewController.removeSpinner(spinner: self.sv!)
                    if (err != nil){
                        self.view.presentToast(msg: NSLocalizedString("serverErr", comment: "") )
                        return
                    }
                    if let status = response as? HTTPURLResponse {
                        print("request status \(status.statusCode)")
                        if status.statusCode != 200{
                            self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                            return
                        }
                    }
                    guard let data = data else { return }
                    do {
                        print(data)
                        let response = try JSONDecoder().decode(forgetPassRes.self, from: data)
                        
                        
                        if response.Status == 1 {
                            
                            DispatchQueue.main.async {
                                
                                self.view.presentToast(msg: response.Message)
                                UserDefaults.standard.set(response.UserID, forKey: "HajUserID")
                                self.forgetCode = response.VerificationCode!
                                print("forgetCode \(response.VerificationCode!)")
                                
                                self.performSegue(withIdentifier: "activationSegue2", sender: nil)
                                
                                
                            }
                            
                            
                        }
                        else{
                            DispatchQueue.main.async {
                                self.view.presentToast(msg: response.Message)
                            }
                            
                        }
                    }
                        
                    catch let jsonErr {
                        print("Error serializing json:", jsonErr)
                        self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                    }
                }.resume()
            }
            else{
                self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
            }
            
            
            
        }
    }
    
    
    
    func isValidEmail(emailStr:String) -> Bool {
        //true if valid email
        //false if email not valid
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
    
    
    @IBAction func goBack(_ sender: UIButton) {
        DispatchQueue.main.async {
            
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let mainViewController = mainStoryBoard.instantiateViewController(withIdentifier: "rootnav") as! ViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate?
            appDelegate!.window?.rootViewController = mainViewController
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "activationSegue2" {
            if let destinationVC = segue.destination as? activationCodeVC {
                
                destinationVC.from = "forgetPass"
                destinationVC.activationCodeForForget = forgetCode
                
            }
        }
        
    }
    
}
