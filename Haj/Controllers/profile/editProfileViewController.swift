//
//  editProfileViewController.swift
//
//
//
//  Copyright © 2019 ITRoots. All rights reserved.
//

import UIKit

class editProfileViewController: LocalizerViewController ,UITextFieldDelegate{
    
    
    @IBOutlet weak var nametext: UITextField!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var phoneText: UITextField!
    @IBOutlet weak var jobText: UITextField!
    @IBOutlet weak var jobView: UIView!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var genderVal: UIButton!
    @IBOutlet weak var genderView: UIView!
    @IBOutlet weak var orgView: UIView!
    @IBOutlet weak var org: UITextField!
    var sv: UIView?
    var genderData :Int = 0
    var editObj:editPerson = editPerson(SID: -1, Name: "", Phone: "", gender: -1, job: "", org: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        phoneText.delegate  = self
        
        if L102Language.currentAppleLanguage() == "en" {
            genderVal.contentHorizontalAlignment = .left
            langForApi =  2
            
        }else {
            langForApi =  1
            genderVal.contentHorizontalAlignment = .right
            
        }
        
        
        self.navigationItem.title = NSLocalizedString("editProfile", comment: "") 
        self.navigationItem.hidesBackButton = true
        if L102Language.currentAppleLanguage() == "ar"{
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "leftArrow"), style: .plain, target: self, action: #selector(gobackTapped))
            self.navigationItem.rightBarButtonItem?.tintColor = .white
        }else{
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "leftArrowCopy-1"), style: .plain, target: self, action: #selector(gobackTapped))
            self.navigationItem.rightBarButtonItem?.tintColor = .white
            
        }
        
        nametext.text = editObj.Name
        jobText.text = editObj.job
        genderData = editObj.gender
        
        if editObj.gender == 1 {
            genderVal.setTitle(NSLocalizedString("male", comment: ""), for: .normal)
        }else if editObj.gender == 2{
            genderVal.setTitle(NSLocalizedString("female", comment: ""), for: .normal)
        }
        
        
        org.text = editObj.org
        
        let first3 = String(editObj.Phone.prefix(3))
        
        if first3 == "966" {
            for _ in 0..<3{
                editObj.Phone.removeFirst()
            }
            
        }
        phoneText.text = editObj.Phone
        nametext.becomeFirstResponder()
        saveBtn.layer.cornerRadius = 21
        let gradient = CAGradientLayer()
        gradient.frame = saveBtn.bounds
        gradient.cornerRadius = 21
        gradient.startPoint = CGPoint(x:0.0, y:0.5)
        gradient.endPoint = CGPoint(x:1.0, y:0.5)
        
        let colorright = UIColor(red: 150.0 / 255.0, green: 0.0 / 255.0, blue: 5.0 / 255.0, alpha: 1.0).cgColor
        let colorleft = UIColor(red: 223.0 / 255.0, green: 18.0 / 255.0, blue: 28.0 / 255.0, alpha: 1.0).cgColor
        
        gradient.colors = [colorright, colorleft]
        
        saveBtn.layer.addSublayer(gradient)
        
    }
    
    @IBAction func saveclicked(_ sender: UIButton) {
        
        if  nametext.text == "" {
            self.view.presentToast(msg:  NSLocalizedString("entername", comment: ""))
        }
        else if phoneText.text == ""
        {
            self.view.presentToast(msg: NSLocalizedString("phoneErrno", comment: ""))
        }else if isValidPhone(phoneStr: phoneText.text!.replacedArabicDigitsWithEnglish) == false
        {
            self.view.presentToast(msg:  NSLocalizedString("Vaildphone", comment: ""))
        }else if genderData == 0
        {
            self.view.presentToast(msg:  NSLocalizedString("Entergen", comment: ""))
        }
        else if jobText.text == ""
        {
            self.view.presentToast(msg:  NSLocalizedString("enterjob", comment: ""))
        }else if org.text == ""
        {
            self.view.presentToast(msg:  NSLocalizedString("Enterorg", comment: ""))
        }else{
            
            if Reachability.isConnectedToNetwork() {
                sv = UIViewController.displaySpinner(onView: self.view)
                let jsonUrlString = updateProfileEndpoint
                guard let url = URL(string: jsonUrlString) else { return }
                do {
                    
                    let postObject = [
                        "UserID": UserDefaults.standard.integer(forKey: "HajUserID"),
                        
                        "FullName": nametext.text!,
                        "Phone": "966"+phoneText.text!.replacedArabicDigitsWithEnglish,
                        "PositionName": jobText.text!,
                        "OrgOrDeptName": org.text!,
                        "Gender": genderData,
                        "Lang": langForApi
                        
                        
                        ] as [String : Any]
                    
                    ApiService.callPost(url: url, params: postObject, finish: self.finishUpdateProfilePost)
                }
            }
            else{
                self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
            }
            
            
            
            
        }
        
        
    }
    
    
    @objc func gobackTapped() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func finishUpdateProfilePost (message:String, data:Data?) -> Void
    {
        do
        {
            UIViewController.removeSpinner(spinner: self.sv!)
            if message == "Success" {
                if data != nil
                {
                    let response = try JSONDecoder().decode(changePassRes.self, from: data!)
                    print("register response.Status : \(response.Status)")
                    
                    if response.Status == 1 {
                        
                        DispatchQueue.main.async {
                            self.view.presentToast(msg: response.Message)
                            
                            if let savedPerson = UserDefaults.standard.object(forKey: "HajUserDetails") as? Data {
                                let decoder = JSONDecoder()
                                if let loadedPerson = try? decoder.decode(userDetails2.self, from: savedPerson) {
                                    
                                    var y = loadedPerson
                                    
                                    y.FullName = self.nametext.text!
                                    y.Phone = "966"+self.phoneText.text!.replacedArabicDigitsWithEnglish
                                    y.PositionName = self.jobText.text!
                                    y.OrgOrDeptName = self.org.text!
                                    y.UserType = self.genderData
                                    let encoder = JSONEncoder()
                                    if let encoded = try? encoder.encode(y) {
                                        let defaults = UserDefaults.standard
                                        defaults.set(encoded, forKey: "HajUserDetails")
                                    }
                                    
                                    
                                }}
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadUserData"), object: nil)
                            
                            self.navigationController?.popViewController(animated: true)
                            
                        }
                        
                    }
                    else {
                        DispatchQueue.main.async {
                            self.view.presentToast(msg: response.Message)
                        }
                    }
                    
                }
            }
            else{
                self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                print("userregistered for notifications failed")
            }
        }
        catch
        {
            print("userregistered for notifications Parse Error: \(error)")
        }
    }
    
    
    @IBAction func genderClicked(_ sender: UIButton) {
        let optionMenu = UIAlertController(title: nil, message: NSLocalizedString("selectgender", comment: "") , preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction(title: NSLocalizedString("male", comment: ""), style: .default, handler: { (action) -> Void in
            
            self.genderData = 1
            self.genderVal.setTitle(NSLocalizedString("male", comment: ""), for: .normal)
        })
        let saveAction = UIAlertAction(title: NSLocalizedString("female", comment: ""), style: .default, handler: { (action) -> Void in
            
            self.genderData = 2
            self.genderVal.setTitle(NSLocalizedString("female", comment: ""), for: .normal)
        })
        
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel,handler: { (action) -> Void in
            
        })
        
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
        
        
    }
    
    func isValidEmail(emailStr:String) -> Bool {
        //true if valid email
        //false if email not valid
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
    
    func isValidPhone(phoneStr:String) -> Bool {
        
        //        let regularExpressionForPhone = "^\\d{3}-\\d{3}-\\d{4}$"
        let regularExpressionForPhone = "[0-9]{9,10}"
        let testPhone = NSPredicate(format:"SELF MATCHES %@", regularExpressionForPhone)
        return testPhone.evaluate(with: phoneStr)
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 10
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    
}
