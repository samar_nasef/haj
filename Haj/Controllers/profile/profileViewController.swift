//
//  profileViewController.swift
//
//
//
//  Copyright © 2019 ITRoots. All rights reserved.
//

import UIKit

class profileViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var userorg: UIButton!
    @IBOutlet weak var gender: UIButton!
    @IBOutlet weak var job: UIButton!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var userMail: UIButton!
    @IBOutlet weak var username2: UIButton!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var profileImgView: UIView!
    @IBOutlet weak var userMobile: UIButton!
    
    var imageString = ""
    var imageUrlVal: String = ""
    var imagesource : Int = 0 //1camera , 2 photolibrary
    var sv: UIView?
    var editperson:editPerson = editPerson(SID: -1, Name: "", Phone: "", gender: -1, job: "", org: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if L102Language.currentAppleLanguage() == "en" {
            langForApi =  2
        }else {
            langForApi =  1
        }
        
        self.navigationItem.title = NSLocalizedString("profile", comment: "")
        self.navigationItem.hidesBackButton = true
        if L102Language.currentAppleLanguage() == "ar"{
            
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "leftArrow"), style: .plain, target: self, action: #selector(gobackTapped))
            self.navigationItem.rightBarButtonItem?.tintColor = .white
        }else{
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "leftArrowCopy-1"), style: .plain, target: self, action: #selector(gobackTapped))
            self.navigationItem.rightBarButtonItem?.tintColor = .white
            
        }
        
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.loadUserData),
                                               name: NSNotification.Name(rawValue: "reloadUserData"),
                                               object: nil)
        
        profileImg.layer.cornerRadius = profileImg.frame.width / 2
        profileImgView.layer.cornerRadius = profileImgView.frame.width / 2
        
        profileImgView.layer.borderWidth = 9
        profileImgView.layer.borderColor = #colorLiteral(red: 0.6862745098, green: 0.6862745098, blue: 0.6862745098, alpha: 0.2548212757)
        
        if let savedPerson = UserDefaults.standard.object(forKey: "HajUserDetails") as? Data {
            let decoder = JSONDecoder()
            if let loadedPerson = try? decoder.decode(userDetails2.self, from: savedPerson) {
                
                
                editperson.Name =  loadedPerson.FullName!
                editperson.Phone = loadedPerson.Phone!
                editperson.gender = loadedPerson.UserType!
                editperson.job = loadedPerson.PositionName!
                editperson.org = loadedPerson.OrgOrDeptName!
                
                if loadedPerson.UserType == 1 {
                    gender.setTitle(NSLocalizedString("male", comment: ""), for: .normal)
                }else if loadedPerson.UserType == 2{
                    gender.setTitle(NSLocalizedString("female", comment: ""), for: .normal)
                }
                
                job.setTitle(loadedPerson.PositionName, for: .normal)
                userorg.setTitle(loadedPerson.OrgOrDeptName, for: .normal)
                userName.text = loadedPerson.Username
                username2.setTitle(loadedPerson.FullName, for: .normal)
                userMail.setTitle(loadedPerson.Email, for: .normal)
                userMobile.setTitle(loadedPerson.Phone, for: .normal)
                if let y = loadedPerson.ProfileImage {
                    
                    let imageUrlToFetch = URL(string: (y))
                    DispatchQueue.global().async { [weak self] in
                        if let data = try? Data(contentsOf: imageUrlToFetch!) {
                            if let image = UIImage(data: data) {
                                DispatchQueue.main.async {
                                    self?.profileImg.image = image
                                }
                            }
                        }
                    }
                    
                    
                }
                
            }
        }
        
        
        
    }
    
    
    @objc func loadUserData(){
        
        
        if let savedPerson = UserDefaults.standard.object(forKey: "HajUserDetails") as? Data {
            let decoder = JSONDecoder()
            if let loadedPerson = try? decoder.decode(userDetails2.self, from: savedPerson) {
                
                
                editperson.Name =  loadedPerson.FullName!
                editperson.Phone = loadedPerson.Phone!
                editperson.gender = loadedPerson.UserType!
                editperson.job = loadedPerson.PositionName!
                editperson.org = loadedPerson.OrgOrDeptName!
                
                if loadedPerson.UserType == 1 {
                    gender.setTitle(NSLocalizedString("male", comment: ""), for: .normal)
                }else if loadedPerson.UserType == 2{
                    gender.setTitle(NSLocalizedString("female", comment: ""), for: .normal)
                }
                
                job.setTitle(loadedPerson.PositionName, for: .normal)
                userorg.setTitle(loadedPerson.OrgOrDeptName, for: .normal)
                userName.text = loadedPerson.Username
                username2.setTitle(loadedPerson.FullName, for: .normal)
                userMail.setTitle(loadedPerson.Email, for: .normal)
                userMobile.setTitle(loadedPerson.Phone, for: .normal)
                
                
            }
        }
        
        
        
        
        
    }
    @IBAction func editUserImg(_ sender: UIButton) {
        
        let optionMenu = UIAlertController(title: nil, message:NSLocalizedString("SelectImageSource", comment: "") , preferredStyle: .actionSheet)
        
        
        let deleteAction = UIAlertAction(title: NSLocalizedString("UseCamera", comment: ""), style: .default, handler: { (action) -> Void in
            
            self.imagesource = 1
            self.chooseImage()
            
        })
        let saveAction = UIAlertAction(title: NSLocalizedString("LoadfromLibrary", comment: ""), style: .default, handler: { (action) -> Void in
            
            self.imagesource = 2
            self.chooseImage()
            
        })
        
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel,handler: { (action) -> Void in
            
        })
        
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
        
    }
    
    func chooseImage(){
        
        let imgPicker = UIImagePickerController()
        imgPicker.delegate = self
        if imagesource == 1{
            imgPicker.sourceType = .camera
            imgPicker.allowsEditing = false
            imgPicker.showsCameraControls = true
        }else if imagesource == 2
        {
            imgPicker.sourceType = .photoLibrary
        }
        
        self.present(imgPicker, animated: true, completion: nil)
        
    }
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey :
        Any]) {
        if let img = info[UIImagePickerController.InfoKey.originalImage] as?
            UIImage {
            self.profileImg.image = img.fixOrientation()
            let imageData = img.jpeg(.lowest)
            let base64String = imageData?.base64EncodedString(options: .init(rawValue: 0))
            
            
            self.dismiss(animated: true, completion: nil)
            
            if Reachability.isConnectedToNetwork() {
                sv = UIViewController.displaySpinner(onView: self.view)
                let jsonUrlString = updateUserProfileImgEndpoint
                guard let url = URL(string: jsonUrlString) else { return }
                do {
                    
                    let postObject = [
                        "UserID": UserDefaults.standard.integer(forKey: "HajUserID"),
                        "ProfileImage": base64String?.encodeURIComponent() ?? "",
                        "ProfileImageExtension": "jpg",
                        "Lang": langForApi
                        
                        ] as [String : Any]
                    
                    ApiService.callPost(url: url, params: postObject, finish: self.finishupdateImge)
                }
            }
            else{
                self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
            }
            
            
            
        } else {
            print("error")
        }
    }
    
    
    func finishupdateImge (message:String, data:Data?) -> Void
    {
        do
        {
            UIViewController.removeSpinner(spinner: self.sv!)
            if message == "Success" {
                if data != nil
                {
                    let response = try JSONDecoder().decode(updateprofileImg.self, from: data!)
                    print("register response.Status : \(response.Status)")
                    
                    if response.Status == 1 {
                        
                        DispatchQueue.main.async {
                            self.view.presentToast(msg: response.Message)
                            
                            
                            if let savedPerson = UserDefaults.standard.object(forKey: "HajUserDetails") as? Data {
                                let decoder = JSONDecoder()
                                if let loadedPerson = try? decoder.decode(userDetails2.self, from: savedPerson) {
                                    
                                    var x = loadedPerson
                                    x.ProfileImage = response.ImageLink
                                    
                                    let encoder = JSONEncoder()
                                    
                                    if let encoded = try? encoder.encode(x) {
                                        let defaults = UserDefaults.standard
                                        defaults.set(encoded, forKey: "HajUserDetails")
                                    }
                                    
                                    
                                }
                            }
                            
                        }
                        
                    }
                    else {
                        DispatchQueue.main.async {
                            self.view.presentToast(msg: response.Message)
                        }
                    }
                    
                }
            }
            else{
                self.view.presentToast(msg:NSLocalizedString("serverErr", comment: ""))
                print("userregistered for notifications failed")
            }
        }
        catch
        {
            print("userregistered for notifications Parse Error: \(error)")
        }
    }
    
    
    
    
    @IBAction func editUserProfile(_ sender: UIButton) {
        print("editUserProfile")
        
        self.performSegue(withIdentifier: "editProfile", sender: nil)
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editProfile" {
            if let destinationVC = segue.destination as? editProfileViewController {
                
                destinationVC.editObj = editperson
                
            }
            
            
            
        }
    }
    
    
    @objc func openNotification(sender: UIBarButtonItem){
        
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "notificationVc") as! notificationsViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
        
        
    }
    
    
    
    @objc func drawNotificationBell(){
        let notificationButton = SSBadgeButton()
        notificationButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        notificationButton.setImage(UIImage(named: "notificationsButton-1")?.withRenderingMode(.alwaysTemplate), for: .normal)
        notificationButton.tintColor = .white
        
        if UserDefaults.standard.integer(forKey: "notificationCount") > 0 {
            
            notificationButton.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
            notificationButton.badge = String(UserDefaults.standard.integer(forKey: "notificationCount"))
        }
        
        notificationButton.addTarget(self, action: #selector(openNotification(sender:)), for: .touchUpInside)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: notificationButton)
    }
    
    
    @objc func gobackTapped() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
