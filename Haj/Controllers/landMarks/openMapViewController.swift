//
//  openMapViewController.swift
//
//
//  
//  Copyright © 2020 ITRoots. All rights reserved.
//

import UIKit
import GoogleMaps

class openMapViewController: UIViewController {
    
    
    @IBOutlet weak var backbtn: UIBarButtonItem!
    @IBOutlet weak var mapx: GMSMapView!
    
    var userLat:Double = 0
    var userLong:Double = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NSLocalizedString("loc", comment: "")
        self.navigationItem.hidesBackButton = true
        
        
        if L102Language.currentAppleLanguage() == "ar"{
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "leftArrow"), style: .plain, target: self, action: #selector(gobackTapped))
            self.navigationItem.rightBarButtonItem?.tintColor = .white
        }else{
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "leftArrowCopy-1"), style: .plain, target: self, action: #selector(gobackTapped))
            self.navigationItem.rightBarButtonItem?.tintColor = .white
            
        }
        
        
        
        
        self.mapx.clear()
        
        let marker = GMSMarker()
        
        marker.position = CLLocationCoordinate2D(latitude: self.userLat, longitude: self.userLong)
        
        //            marker.icon = UIImage(named: "pin")
        marker.map = self.mapx
        
        self.mapx.camera = GMSCameraPosition(target: CLLocationCoordinate2D(latitude: self.userLat, longitude: self.userLong), zoom: 18, bearing: 0, viewingAngle: 0)
        
    }
    
    
    
    @IBAction func goback(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func gobackTapped() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
}
