//
//  addEditViewController.swift
//
//
//
//  Copyright © 2019 ITRoots. All rights reserved.
//

import UIKit
import GoogleMaps

import MapKit
import CoreLocation


class addEditViewController: LocalizerViewController, CLLocationManagerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,GMSMapViewDelegate{
    
    @IBOutlet weak var landMarkImg: UIImageView!
    var getLocation = false
    var from:String = ""
    var editLandmarkId:Int = -1
    var isdaysActive = false
    var isramadanActive = false
    var isfridayActive = false
    var ishajActive = false
    var  editObj = landMark(SID: -1, LayerSID: -1, LayerName: "", Latitude: 0, Longtitude: 0, PointTitle: "", Notes: "", SubFormNo: "", WCNo: -1, PointStatus: -1, SubType: "", StarsNo: -1, PointNo: -1, NearestLandmark: "", WorkDuringNormalDays: false, WorkDuringRamadan: false, WorkDuringFriday: false, WorkDuringPilgrimage: false, CreationDate: "",UploadedImage: "")
    
    @IBOutlet weak var duringHajHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var duringramadanheightconst: NSLayoutConstraint!
    @IBOutlet weak var duringnormalheightcons: NSLayoutConstraint!
    @IBOutlet weak var titleText: UITextField!
    @IBOutlet weak var duringFridayheightconst: NSLayoutConstraint!
    var imageString = ""
    var imagesource : Int = 0 //1camera , 2 photolibrary
    var statusVal:Int = 0
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var statusBtn: UIButton!
    var layersDataArr : [layerObj] = []
    var choosenLayerSid:Int = 0
    var sv: UIView?
    @IBOutlet weak var uploadImgView: UIView!
    @IBOutlet weak var layerBtn: UIButton!
    @IBOutlet weak var layersecondBtn: UIButton!
    let locationManager = CLLocationManager()
    var userLat:Double = 0
    var userLong:Double = 0
    var locationEnabled = false
    @IBOutlet weak var layerView: UIView!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var mymapView: GMSMapView!
    @IBOutlet weak var modelNoLabel: UILabel!
    @IBOutlet weak var modelNoText: UITextField!
    @IBOutlet weak var wcNoText: UITextField!
    @IBOutlet weak var wcNoLabel: UILabel!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var workduringhajBtn: UIButton!
    @IBOutlet weak var notesText: UITextView!
    @IBOutlet weak var workduringHajLabel: UILabel!
    @IBOutlet weak var workduringfridaylabel: UILabel!
    @IBOutlet weak var workduringfridayBtn: UIButton!
    @IBOutlet weak var workduringdaysbtn: UIButton!
    @IBOutlet weak var workDuringDaysLabel: UILabel!
    @IBOutlet weak var workDuringRamadanBtn: UIButton!
    @IBOutlet weak var workduringRamadanLabel: UILabel!
    @IBOutlet weak var nearestLandMarkLabel: UILabel!
    @IBOutlet weak var nearestLandMarkText: UITextField!
    @IBOutlet weak var pointNoText: UITextField!
    @IBOutlet weak var pointNOLabel: UILabel!
    @IBOutlet weak var starsText: UITextField!
    @IBOutlet weak var starsLabel: UILabel!
    @IBOutlet weak var typeText: UITextField!
    @IBOutlet weak var typeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mymapView.delegate = self
        
        statusView.layer.cornerRadius = 8
        statusView.layer.shadowOpacity = 1
        statusView.layer.shadowRadius = 3.0
        statusView.layer.shadowOffset = CGSize(width: 0, height: 3)
        statusView.layer.shadowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        statusView.layer.borderWidth = 1
        statusView.layer.borderColor = #colorLiteral(red: 0.895331796, green: 0.895331796, blue: 0.895331796, alpha: 1)
        
        saveBtn.layer.cornerRadius = 21
        
        let gradient = CAGradientLayer()
        gradient.frame = saveBtn.bounds
        gradient.cornerRadius = 21
        gradient.startPoint = CGPoint(x:0.0, y:0.5)
        gradient.endPoint = CGPoint(x:1.0, y:0.5)
        
        let colorright = UIColor(red: 150.0 / 255.0, green: 0.0 / 255.0, blue: 5.0 / 255.0, alpha: 1.0).cgColor
        let colorleft = UIColor(red: 223.0 / 255.0, green: 18.0 / 255.0, blue: 28.0 / 255.0, alpha: 1.0).cgColor
        
        gradient.colors = [colorright, colorleft]
        
        saveBtn.layer.addSublayer(gradient)
        
        self.statusVal = 1
        self.statusBtn.setTitle(NSLocalizedString("old", comment: ""), for: .normal)
        
        if L102Language.currentAppleLanguage() == "ar"{
            layerBtn.contentHorizontalAlignment = .right
            statusBtn.contentHorizontalAlignment = .right
            langForApi =  1
            
        }else{
            layerBtn.contentHorizontalAlignment = .left
            statusBtn.contentHorizontalAlignment = .left
            
            langForApi =  2
        }
        
        notesText.textColor = UIColor.lightGray
        notesText.layer.cornerRadius = 8
        notesText.layer.borderWidth = 1.0
        notesText.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.9040828339)
        
        uploadImgView.layer.cornerRadius = 21
        
        mymapView.isMyLocationEnabled = true
        mymapView.settings.myLocationButton = true
        
        
        if from == "edit"{
            layerBtn.isUserInteractionEnabled = false
            layersecondBtn.isUserInteractionEnabled = false
            
        }
        
        
        if from == "add"{
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
                
                
            }
        }
        
        if self.from == "add" {
            self.navigationItem.title = NSLocalizedString("addLandMark", comment: "")
        }else if self.from == "edit" {
            self.navigationItem.title = NSLocalizedString("editLandMark", comment: "")
        }
        self.navigationItem.hidesBackButton = true
        
        if L102Language.currentAppleLanguage() == "ar"{
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "leftArrow"), style: .plain, target: self, action: #selector(gobackTapped))
            self.navigationItem.rightBarButtonItem?.tintColor = .white
        }else{
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "leftArrowCopy-1"), style: .plain, target: self, action: #selector(gobackTapped))
            self.navigationItem.rightBarButtonItem?.tintColor = .white
            
        }
        
        layerView.layer.cornerRadius = 8
        
        layerView.layer.shadowOpacity = 1
        layerView.layer.shadowRadius = 3.0
        layerView.layer.shadowOffset = CGSize(width: 0, height: 3)
        layerView.layer.shadowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        layerView.layer.borderWidth = 1
        layerView.layer.borderColor = #colorLiteral(red: 0.895331796, green: 0.895331796, blue: 0.895331796, alpha: 1)
        
        
        if Reachability.isConnectedToNetwork() {
            sv = UIViewController.displaySpinner(onView: self.view)
            let userId = UserDefaults.standard.integer(forKey: "HajUserID")
            let jsonUrlString = listLayersEndpoint+String(userId)+"&lang=\(String(langForApi))"
            
            guard let url = URL(string: jsonUrlString) else { return }
            URLSession.shared.dataTask(with: url) { (data, response, err) in
                UIViewController.removeSpinner(spinner: self.sv!)
                if (err != nil){
                    self.view.presentToast(msg: NSLocalizedString("serverErr", comment: "") )
                    return
                }
                if let status = response as? HTTPURLResponse {
                    print("request status \(status.statusCode)")
                    if status.statusCode != 200{
                        self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                        return
                    }
                }
                guard let data = data else { return }
                do {
                    print(data)
                    let response = try JSONDecoder().decode(listLayersRes.self, from: data)
                    
                    if response.Status == 1 {
                        DispatchQueue.main.async {
                            self.layersDataArr = response.LayersList
                            if self.layersDataArr.count > 0 {
                                
                                if self.from == "edit"{
                                    
                                    
                                    
                                    self.choosenLayerSid = self.editObj.LayerSID
                                    
                                    let imageUrlToFetch = URL(string: (self.editObj.UploadedImage))
                                    DispatchQueue.global().async { [weak self] in
                                        if let data = try? Data(contentsOf: imageUrlToFetch!) {
                                            if let image = UIImage(data: data) {
                                                DispatchQueue.main.async {
                                                    self?.landMarkImg.image = image
                                                    
                                                    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self?.imageTapped(_:)))
                                                    self?.landMarkImg.isUserInteractionEnabled = true
                                                    self?.landMarkImg.addGestureRecognizer(tapGesture)
                                                    
                                                }
                                            }
                                        }
                                    }
                                    
                                    self.layerBtn.setTitle(self.editObj.LayerName, for:.normal)
                                    
                                    for item in self.layersDataArr{
                                        if item.SID == self.editObj.LayerSID{
                                            
                                            
                                            self.userLat = self.editObj.Latitude!
                                            
                                            self.userLong = self.editObj.Longtitude!
                                            
                                            self.mymapView.clear()
                                            let marker = GMSMarker()
                                            
                                            marker.position = CLLocationCoordinate2D(latitude: self.userLat, longitude: self.userLong)
                                            
                                            marker.map = self.mymapView
                                            
                                            self.mymapView.camera = GMSCameraPosition(target: CLLocationCoordinate2D(latitude: self.userLat, longitude: self.userLong), zoom: 18, bearing: 0, viewingAngle: 0)
                                            
                                            
                                            self.titleText.text = self.editObj.PointTitle
                                            
                                            self.notesText.text = self.editObj.Notes
                                            
                                            
                                            self.typeLabel.isHidden = !item.SubType
                                            self.typeText.isHidden = !item.SubType
                                            
                                            
                                            self.typeText.text = self.editObj.SubType
                                            self.modelNoLabel.isHidden = !item.SubFormNo
                                            self.modelNoText.isHidden = !item.SubFormNo
                                            
                                            self.modelNoText.text = self.editObj.SubFormNo
                                            
                                            self.wcNoLabel.isHidden = !item.WCNo
                                            self.wcNoText.isHidden = !item.WCNo
                                            
                                            self.wcNoText.text = String(self.editObj.WCNo!)
                                            
                                            self.starsLabel.isHidden = !item.StarsNo
                                            self.starsText.isHidden = !item.StarsNo
                                            
                                            self.starsText.text = String(self.editObj.StarsNo!)
                                            
                                            self.pointNOLabel.isHidden = !item.PointNo
                                            self.pointNoText.isHidden = !item.PointNo
                                            
                                            self.pointNoText.text = String(self.editObj.PointNo!)
                                            
                                            self.statusView.isHidden = !item.PointStatus
                                            
                                            self.statusLbl.isHidden = !item.PointStatus
                                            
                                            self.nearestLandMarkText.isHidden = !item.NearestLandmark
                                            self.nearestLandMarkLabel.isHidden = !item.NearestLandmark
                                            
                                            self.nearestLandMarkText.text = self.editObj.NearestLandmark!
                                            
                                            self.workDuringRamadanBtn.isHidden = !item.WorkDuringRamadan
                                            self.workduringRamadanLabel.isHidden = !item.WorkDuringRamadan
                                            
                                            if self.editObj.WorkDuringRamadan {
                                                self.workDuringRamadanBtn.setImage(UIImage(named: "vectorSmartObject-2"), for: .normal)
                                                
                                            } else {
                                                self.workDuringRamadanBtn.setImage(UIImage(named:"roundedRectangle3"), for: .normal)
                                                
                                                
                                            }
                                            
                                            if self.workDuringRamadanBtn.isHidden == true {
                                                self.duringramadanheightconst.constant = 0
                                            }else{
                                                self.duringramadanheightconst.constant = 32
                                            }
                                            
                                            self.workduringdaysbtn.isHidden = !item.WorkDuringNormalDays
                                            self.workDuringDaysLabel.isHidden = !item.WorkDuringNormalDays
                                            
                                            if self.editObj.WorkDuringNormalDays {
                                                self.workduringdaysbtn.setImage(UIImage(named: "vectorSmartObject-2"), for: .normal)
                                                
                                            } else {
                                                self.workduringdaysbtn.setImage(UIImage(named:"roundedRectangle3"), for: .normal)
                                            }
                                            
                                            
                                            if self.workduringdaysbtn.isHidden == true {
                                                self.duringnormalheightcons.constant = 0
                                            }else{
                                                self.duringnormalheightcons.constant = 32
                                            }
                                            
                                            self.workduringfridayBtn.isHidden = !item.WorkDuringFriday
                                            self.workduringfridaylabel.isHidden = !item.WorkDuringFriday
                                            
                                            
                                            
                                            if self.editObj.WorkDuringFriday {
                                                self.workduringfridayBtn.setImage(UIImage(named: "vectorSmartObject-2"), for: .normal)
                                                
                                            } else {
                                                self.workduringfridayBtn.setImage(UIImage(named:"roundedRectangle3"), for: .normal)
                                            }
                                            
                                            
                                            if self.workduringfridayBtn.isHidden == true {
                                                self.duringFridayheightconst.constant = 0
                                            }else{
                                                self.duringFridayheightconst.constant = 32
                                            }
                                            
                                            
                                            self.workduringhajBtn.isHidden = !item.WorkDuringPilgrimage
                                            self.workduringHajLabel.isHidden = !item.WorkDuringPilgrimage
                                            
                                            if self.editObj.WorkDuringPilgrimage {
                                                self.workduringhajBtn.setImage(UIImage(named: "vectorSmartObject-2"), for: .normal)
                                                
                                            } else {
                                                self.workduringhajBtn.setImage(UIImage(named:"roundedRectangle3"), for: .normal)
                                            }
                                            
                                            
                                            if self.workduringHajLabel.isHidden == true {
                                                self.duringHajHeightConstraint.constant = 0
                                            }else{
                                                self.duringHajHeightConstraint.constant = 32
                                            }
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                        }
                                    }
                                    
                                    
                                    
                                    
                                    
                                }else{
                                    
                                    
                                    
                                    
                                    
                                    self.choosenLayerSid = self.layersDataArr[0].SID
                                    
                                    self.layerBtn.setTitle(self.layersDataArr[0].LayerName, for:.normal)
                                    
                                    self.typeLabel.isHidden = !self.layersDataArr[0].SubType
                                    self.typeText.isHidden = !self.layersDataArr[0].SubType
                                    
                                    self.modelNoLabel.isHidden = !self.layersDataArr[0].SubFormNo
                                    self.modelNoText.isHidden = !self.layersDataArr[0].SubFormNo
                                    
                                    self.wcNoLabel.isHidden = !self.layersDataArr[0].WCNo
                                    self.wcNoText.isHidden = !self.layersDataArr[0].WCNo
                                    
                                    self.starsLabel.isHidden = !self.layersDataArr[0].StarsNo
                                    self.starsText.isHidden = !self.layersDataArr[0].StarsNo
                                    self.pointNOLabel.isHidden = !self.layersDataArr[0].PointNo
                                    self.pointNoText.isHidden = !self.layersDataArr[0].PointNo
                                    
                                    self.statusView.isHidden = !self.layersDataArr[0].PointStatus
                                    
                                    self.statusLbl.isHidden = !self.layersDataArr[0].PointStatus
                                    
                                    
                                    
                                    self.nearestLandMarkText.isHidden = !self.layersDataArr[0].NearestLandmark
                                    self.nearestLandMarkLabel.isHidden = !self.layersDataArr[0].NearestLandmark
                                    
                                    
                                    self.workDuringRamadanBtn.isHidden = !self.layersDataArr[0].WorkDuringRamadan
                                    self.workduringRamadanLabel.isHidden = !self.layersDataArr[0].WorkDuringRamadan
                                    
                                    if self.workDuringRamadanBtn.isHidden == true {
                                        self.duringramadanheightconst.constant = 0
                                    }else{
                                        self.duringramadanheightconst.constant = 32
                                    }
                                    
                                    self.workduringdaysbtn.isHidden = !self.layersDataArr[0].WorkDuringNormalDays
                                    self.workDuringDaysLabel.isHidden = !self.layersDataArr[0].WorkDuringNormalDays
                                    
                                    
                                    if self.workduringdaysbtn.isHidden == true {
                                        self.duringnormalheightcons.constant = 0
                                    }else{
                                        self.duringnormalheightcons.constant = 32
                                    }
                                    
                                    self.workduringfridayBtn.isHidden = !self.layersDataArr[0].WorkDuringFriday
                                    self.workduringfridaylabel.isHidden = !self.layersDataArr[0].WorkDuringFriday
                                    
                                    
                                    if self.workduringfridayBtn.isHidden == true {
                                        self.duringFridayheightconst.constant = 0
                                    }else{
                                        self.duringFridayheightconst.constant = 32
                                    }
                                    
                                    
                                    self.workduringhajBtn.isHidden = !self.layersDataArr[0].WorkDuringPilgrimage
                                    self.workduringHajLabel.isHidden = !self.layersDataArr[0].WorkDuringPilgrimage
                                    
                                    
                                    if self.workduringHajLabel.isHidden == true {
                                        self.duringHajHeightConstraint.constant = 0
                                    }else{
                                        self.duringHajHeightConstraint.constant = 32
                                    }
                                    
                                }
                                
                                
                            }
                        }
                        
                    }
                    else{
                        DispatchQueue.main.async {
                            self.view.presentToast(msg: response.Message)
                        }
                    }
                }
                    
                catch _ {
                    
                    self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                }
            }.resume()
        }
        else{
            self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
        }
        
        
        
    }
    
    
    
    
    
    @IBAction func listStatus(_ sender: UIButton) {
        
        
        let optionMenu = UIAlertController(title: nil, message:NSLocalizedString("status", comment: "") , preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction(title: NSLocalizedString("old", comment: "") , style: .default, handler: { (action) -> Void in
            
            self.statusVal = 1
            self.statusBtn.setTitle(NSLocalizedString("old", comment: ""), for: .normal)
        })
        let saveAction = UIAlertAction(title: NSLocalizedString("new", comment: "") , style: .default, handler: { (action) -> Void in
            
            self.statusVal = 2
            self.statusBtn.setTitle(NSLocalizedString("new", comment: ""), for: .normal)
        })
        
        let OtherAction = UIAlertAction(title: NSLocalizedString("other", comment: "") , style: .default, handler: { (action) -> Void in
            
            self.statusVal = 3
            self.statusBtn.setTitle(NSLocalizedString("other", comment: ""), for: .normal)
        })
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel,handler: { (action) -> Void in
            
            
            
        })
        
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(OtherAction)
        optionMenu.addAction(cancelAction)
        
        optionMenu.popoverPresentationController?.sourceView = self.view;
        optionMenu.popoverPresentationController?.sourceRect = CGRect(x: 0,y: UIScreen.main.bounds.height - 100 ,width: UIScreen.main.bounds.width,height: 1.0);
        
        self.present(optionMenu, animated: true, completion: nil)
        
        
    }
    
    @IBAction func listLayers(_ sender: UIButton) {
        
        let optionMenu = UIAlertController(title: nil, message: NSLocalizedString("selectlayer", comment: ""), preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel,handler: { (action) -> Void in
            
            
        })
        
        
        optionMenu.addAction(cancelAction)
        
        for i in layersDataArr {
            
            let orgAction = UIAlertAction(title:i.LayerName, style: .default, handler: { (action) -> Void in
                
                self.choosenLayerSid = i.SID
                
                self.layerBtn.setTitle(i.LayerName, for:.normal)
                
                
                self.typeLabel.isHidden = !i.SubType
                self.typeText.isHidden = !i.SubType
                
                self.modelNoLabel.isHidden = !i.SubFormNo
                self.modelNoText.isHidden = !i.SubFormNo
                
                self.wcNoLabel.isHidden = !i.WCNo
                self.wcNoText.isHidden = !i.WCNo
                
                self.starsLabel.isHidden = !i.StarsNo
                self.starsText.isHidden = !i.StarsNo
                self.pointNOLabel.isHidden = !i.PointNo
                self.pointNoText.isHidden = !i.PointNo
                
                self.statusView.isHidden = !i.PointStatus
                self.statusLbl.isHidden = !i.PointStatus
                
                self.nearestLandMarkText.isHidden = !i.NearestLandmark
                self.nearestLandMarkLabel.isHidden = !i.NearestLandmark
                
                self.workDuringRamadanBtn.isHidden = !i.WorkDuringRamadan
                self.workduringRamadanLabel.isHidden = !i.WorkDuringRamadan
                
                
                if self.workDuringRamadanBtn.isHidden == true {
                    self.duringramadanheightconst.constant = 0
                }else{
                    self.duringramadanheightconst.constant = 32
                }
                
                self.workduringdaysbtn.isHidden = !i.WorkDuringNormalDays
                self.workDuringDaysLabel.isHidden = !i.WorkDuringNormalDays
                
                
                
                if self.workduringdaysbtn.isHidden == true {
                    self.duringnormalheightcons.constant = 0
                }else{
                    self.duringnormalheightcons.constant = 32
                }
                
                self.workduringfridayBtn.isHidden = !i.WorkDuringFriday
                self.workduringfridaylabel.isHidden = !i.WorkDuringFriday
                
                
                if self.workduringfridayBtn.isHidden == true {
                    self.duringFridayheightconst.constant = 0
                }else{
                    self.duringFridayheightconst.constant = 32
                }
                
                self.workduringhajBtn.isHidden = !i.WorkDuringPilgrimage
                self.workduringHajLabel.isHidden = !i.WorkDuringPilgrimage
                
                
                if self.workduringHajLabel.isHidden == true {
                    self.duringHajHeightConstraint.constant = 0
                }else{
                    self.duringHajHeightConstraint.constant = 32
                }
                
                
                
            })
            optionMenu.addAction(orgAction)
        }
        
        self.present(optionMenu, animated: true, completion: nil)
        
        
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        self.userLat = locValue.latitude
        self.userLong = locValue.longitude
        
        mymapView.camera = GMSCameraPosition(target: locValue, zoom: 18, bearing: 0, viewingAngle: 0)
        
        self.locationManager.stopUpdatingLocation()
    }
    
    
    
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        userLat = coordinate.latitude
        userLong = coordinate.longitude
        self.mymapView.clear()
        
        let marker = GMSMarker()
        
        marker.position = CLLocationCoordinate2D(latitude: userLat, longitude: userLong)
        
        //            marker.icon = UIImage(named: "pin")
        marker.map = self.mymapView
        
        
    }
    
    @objc func gobackTapped() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func uploadImg(_ sender: UIButton) {
        
        let optionMenu = UIAlertController(title: nil, message: NSLocalizedString("SelectImageSource", comment: ""), preferredStyle: .actionSheet)
        
        
        let deleteAction = UIAlertAction(title: NSLocalizedString("UseCamera", comment: ""), style: .default, handler: { (action) -> Void in
            
            self.imagesource = 1
            self.chooseImage()
            
        })
        let saveAction = UIAlertAction(title: NSLocalizedString("LoadfromLibrary", comment: ""), style: .default, handler: { (action) -> Void in
            
            self.imagesource = 2
            self.chooseImage()
            
        })
        
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel,handler: { (action) -> Void in
            
        })
        
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
        
        
        
    }
    func chooseImage(){
        
        let imgPicker = UIImagePickerController()
        imgPicker.delegate = self
        if imagesource == 1{
            imgPicker.sourceType = .camera
            imgPicker.allowsEditing = false
            imgPicker.showsCameraControls = true
        }else if imagesource == 2
        {
            imgPicker.sourceType = .photoLibrary
        }
        
        self.present(imgPicker, animated: true, completion: nil)
        
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey :
        Any]) {
        
        if let img = info[UIImagePickerController.InfoKey.originalImage] as?
            UIImage {
            
            self.landMarkImg.image = img.fixOrientation()
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(imageTapped(_:)))
            self.landMarkImg.isUserInteractionEnabled = true
            self.landMarkImg.addGestureRecognizer(tapGesture)
            
            let imageData = img.jpeg(.lowest)
            let base64String = imageData?.base64EncodedString(options: .init(rawValue: 0))
            
            imageString = base64String!
            
            self.view.presentToast2(msg: NSLocalizedString("uploadImg", comment: ""))
            self.dismiss(animated: true, completion: nil)
            
            
        }
    }
    
    @IBAction func addEditLandMark(_ sender: UIButton) {
        print("addEditLandMark")
        
        if  titleText.text == "" {
            self.view.presentToast2(msg: NSLocalizedString("typeName", comment: ""))
        }else if userLat == 0 ||  userLong == 0{
            self.view.presentToast2(msg: NSLocalizedString("locationReq", comment: ""))
        }else{
            
            if from == "edit" {
                if Reachability.isConnectedToNetwork() {
                    sv = UIViewController.displaySpinner(onView: self.view)
                    let jsonUrlString = updateLandMarkEndpoint
                    print("edit url : \(jsonUrlString)")
                    guard let url = URL(string: jsonUrlString) else { return }
                    do {
                        var landImg = imageString.encodeURIComponent()
                        var ex = "jpg"
                        if landImg == ""{
                            print("!img")
                            landImg = self.editObj.UploadedImage
                            ex = ""
                        }
                        
                        
                        let postObject = [
                            "SID": editLandmarkId,
                            "UserID": UserDefaults.standard.integer(forKey: "HajUserID"),
                            "Lang": langForApi,
                            "LayerID": self.choosenLayerSid,
                            "Latitude": userLat,
                            "Longtitude": userLong,
                            "Title": titleText.text!,
                            "Notes": notesText.text!,
                            "SubFormNo": modelNoText.text!,
                            "WCNo": wcNoText.text!,
                            "PointStatus": statusVal,
                            "SubType": typeText.text!,
                            "StarsNo": starsText.text!,
                            "PointNo": pointNoText.text!,
                            "NearestLandmark": nearestLandMarkText.text!,
                            "WorkDuringNormalDays": isdaysActive,
                            "WorkDuringRamadan": isramadanActive,
                            "WorkDuringFriday": isfridayActive,
                            "WorkDuringPilgrimage": ishajActive,
                            "ImageExtension": ex,
                            "UploadedImage": landImg!
                            ] as [String : Any]
                        
                        ApiService.callPost(url: url, params: postObject, finish: self.finishaddLandMarkPost)
                    }
                }
                else{
                    self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
                }
                
            }else if from == "add"{
                if Reachability.isConnectedToNetwork() {
                    sv = UIViewController.displaySpinner(onView: self.view)
                    let jsonUrlString = insertLandMarkEndpoint
                    guard let url = URL(string: jsonUrlString) else { return }
                    do {
                        
                        let postObject = [
                            "UserID": UserDefaults.standard.integer(forKey: "HajUserID"),
                            "Lang": langForApi,
                            "LayerID": self.choosenLayerSid,
                            "Latitude": userLat,
                            "Longtitude": userLong,
                            "Title": titleText.text!,
                            "Notes": notesText.text!,
                            "SubFormNo": modelNoText.text!,
                            "WCNo": wcNoText.text!,
                            "PointStatus": statusVal,
                            "SubType": typeText.text!,
                            "StarsNo": starsText.text!,
                            "PointNo": pointNoText.text!,
                            "NearestLandmark": nearestLandMarkText.text!,
                            "WorkDuringNormalDays": isdaysActive,
                            "WorkDuringRamadan": isramadanActive,
                            "WorkDuringFriday": isfridayActive,
                            "WorkDuringPilgrimage": ishajActive,
                            "ImageExtension": "jpg",
                            "UploadedImage": imageString.encodeURIComponent() ?? ""
                            ] as [String : Any]
                        
                        ApiService.callPost(url: url, params: postObject, finish: self.finishaddLandMarkPost)
                    }
                }
                else{
                    self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
                }
            }
        }
        
    }
    
    
    func finishaddLandMarkPost (message:String, data:Data?) -> Void
    {
        do
        {
            UIViewController.removeSpinner(spinner: self.sv!)
            if message == "Success" {
                if data != nil
                {
                    let response = try JSONDecoder().decode(insertLandMarkRes.self, from: data!)
                    print("insertLandMarkRes response.Status : \(response.Status)")
                    
                    if response.Status == 1 {
                        
                        DispatchQueue.main.async {
                            self.view.presentToast(msg: response.Message)
                            
                            
                            
                            self.navigationController?.popViewController(animated: true)
                            
                            
                        }
                        
                    }
                    else {
                        DispatchQueue.main.async {
                            self.view.presentToast(msg: response.Message)
                        }
                    }
                    
                }
            }
            else{
                self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                print("userregistered for notifications failed")
            }
        }
        catch
        {
            print("userregistered for notifications Parse Error: \(error)")
        }
    }
    
    
    
    @IBAction func workduringHajClicked(_ sender: UIButton) {
        ishajActive = !ishajActive
        if ishajActive {
            workduringhajBtn.setImage(UIImage(named: "vectorSmartObject-2"), for: .normal)
            
        } else {
            workduringhajBtn.setImage(UIImage(named:"roundedRectangle3"), for: .normal)
            
            
        }
    }
    
    @IBAction func workduringdaysclicked(_ sender: UIButton) {
        isdaysActive = !isdaysActive
        if isdaysActive {
            workduringdaysbtn.setImage(UIImage(named: "vectorSmartObject-2"), for: .normal)
            
        } else {
            workduringdaysbtn.setImage(UIImage(named:"roundedRectangle3"), for: .normal)
            
            
        }
        
    }
    
    @IBAction func workduringramadanclicked(_ sender: UIButton) {
        
        isramadanActive = !isramadanActive
        if isramadanActive {
            workDuringRamadanBtn.setImage(UIImage(named: "vectorSmartObject-2"), for: .normal)
            
        } else {
            workDuringRamadanBtn.setImage(UIImage(named:"roundedRectangle3"), for: .normal)
            
            
        }
    }
    
    @IBAction func workduringfridayClicked(_ sender: UIButton) {
        isfridayActive = !isfridayActive
        if isfridayActive {
            workduringfridayBtn.setImage(UIImage(named: "vectorSmartObject-2"), for: .normal)
            
        } else {
            workduringfridayBtn.setImage(UIImage(named:"roundedRectangle3"), for: .normal)
            
            
        }
        
    }
    
    
    func triggerTouchAction(gestureReconizer: UITapGestureRecognizer) {
        
    }
    
    func initGoogleMaps() {
        
        let camera = GMSCameraPosition.camera(withLatitude: userLat, longitude: userLong, zoom: 1.0)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        mapView.isMyLocationEnabled = true
        self.mymapView.camera = camera
        self.mymapView.delegate = self
        self.mymapView.isMyLocationEnabled = true
        self.mymapView.settings.myLocationButton = true
        if locationEnabled == false {
            
        }
        else {
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: userLat, longitude: userLong)
            marker.map = self.mymapView
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.view.presentToast(msg: NSLocalizedString("cannotLocateUser", comment: ""))
        locationEnabled = false
        
        
    }
    
    
    
    @objc func imageTapped(_ sender: UITapGestureRecognizer) {
        let imageView = sender.view as! UIImageView
        let newImageView = UIImageView(image: imageView.image)
        newImageView.frame = UIScreen.main.bounds
        
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        newImageView.addGestureRecognizer(tap)
        self.view.addSubview(newImageView)
        //        self.navigationController?.isNavigationBarHidden = true
        //        self.tabBarController?.tabBar.isHidden = true
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = false
        sender.view?.removeFromSuperview()
    }
    
}
