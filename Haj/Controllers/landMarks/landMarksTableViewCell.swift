//
//  landMarksTableViewCell.swift
//
//
//
//  Copyright © 2019 ITRoots. All rights reserved.
//

import UIKit

class landMarksTableViewCell: UITableViewCell {
    @IBOutlet weak var lblvisitfordir: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var layerName: UILabel!
    @IBOutlet weak var visit: UILabel!
    @IBOutlet weak var map: UIButton!
    @IBOutlet weak var editLandMArk: UIButton!
    @IBOutlet weak var removeLandMark: UIButton!
    @IBOutlet weak var widthconstForvisitsLbl: NSLayoutConstraint!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }
    
}
