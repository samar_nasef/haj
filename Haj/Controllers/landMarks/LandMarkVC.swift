//
//  LandMarkVC.swift
//
//
//
//  Copyright © 2019 ITRoots. All rights reserved.
//

import UIKit

class LandMarkVC:  UITableViewController{
    
    private var currentPage = 0
    var tmparrlength = -1
    var editData = landMark(SID: -1, LayerSID: -1, LayerName: "", Latitude: 0, Longtitude: 0, PointTitle: "", Notes: "", SubFormNo: "", WCNo: -1, PointStatus: -1, SubType: "", StarsNo: -1, PointNo: -1, NearestLandmark: "", WorkDuringNormalDays: false, WorkDuringRamadan: false, WorkDuringFriday: false, WorkDuringPilgrimage: false, CreationDate: "",UploadedImage: "")
    
    var fbtn = UIButton()
    var fbtn2 = UIButton()
    var addOrEdit:String = ""
    var landmarkidForedit:Int = -1
    var label:UILabel = UILabel()
    var landMarksData : [landMark] = []
    var sv: UIView?
    var selectedLat:Double = 0
    var selectedLng:Double = 0
    @IBOutlet var landMarksTableView: UITableView!
    var refreshControl4 = UIRefreshControl()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        drawNotificationBell()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.drawNotificationBell),
                                               name: NSNotification.Name(rawValue: "reloadnotificationCount"),
                                               object: nil)
        
        
        
        if L102Language.currentAppleLanguage() == "ar"{
            floatingButton()
            langForApi =  1
            
        }else{
            
            floatingButtonen()
            langForApi =  2
           
        }
        
       
        label =  UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 21))
       
        
        refreshControl4.addTarget(self, action: #selector(loadVisits), for: UIControl.Event.valueChanged)
        landMarksTableView.addSubview(refreshControl4)
        landMarksTableView.delegate = self
        landMarksTableView.dataSource = self
        landMarksTableView.tableFooterView = UIView()
        
    
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return landMarksData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "landMarksCell", for: indexPath) as! landMarksTableViewCell
        let item = self.landMarksData[indexPath.row]
        cell.visit.text = item.PointTitle
        cell.layerName.text = item.LayerName
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
        dateFormatterGet.timeZone = TimeZone.current
        let estimatedDate = dateFormatterGet.date(from: item.CreationDate)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let estimatedDateString = dateFormatter.string(from: estimatedDate!)
        cell.date.text = estimatedDateString
        cell.map.tag = item.SID
        cell.map.addTarget(self, action: #selector(openlandMapx(_:)), for: .touchUpInside)
        
        if L102Language.currentAppleLanguage() == "ar"{
            cell.widthconstForvisitsLbl.constant = 45.33
            cell.visit.textAlignment = .right
            cell.lblvisitfordir.textAlignment = .right
            
        }else {
            cell.widthconstForvisitsLbl.constant = 40
            cell.visit.textAlignment = .left
            cell.lblvisitfordir.textAlignment = .left
        }
        
        cell.removeLandMark.tag = item.SID
        cell.removeLandMark.addTarget(self, action: #selector(removelandmark(_:)), for: .touchUpInside)
        cell.editLandMArk.tag = item.SID
        cell.editLandMArk.addTarget(self, action: #selector(editlandmark(_:)), for: .touchUpInside)
        
        return cell
    }
    @objc func openlandMapx(_ btn: UIButton){
        for item in landMarksData {
            if item.SID == btn.tag {
                selectedLat = item.Latitude!
                selectedLng = item.Longtitude!
                
                self.performSegue(withIdentifier: "openlandMap", sender: nil)
                
                
            }
        }
    }
    
    @objc func openNotification(sender: UIBarButtonItem){
       
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "notificationVc") as! notificationsViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
        
    }
    @objc func editlandmark(_ btn: UIButton){
        
        landmarkidForedit = btn.tag
        addOrEdit = "edit"
        for item in landMarksData {
            if item.SID == btn.tag {
                editData = item
                self.performSegue(withIdentifier: "newLandMarkseg", sender: nil)
                
                
            }
        }
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        _ = indexPath.row
       
        
    }
    
    @objc func removelandmark(_ btn: UIButton){
        
        let alert = UIAlertController(title:nil, message:  NSLocalizedString("confirmdeletemsg", comment: ""), preferredStyle: .alert)
        
        let paragraphStyle = NSMutableParagraphStyle()
        
        if L102Language.currentAppleLanguage() == "ar"{
            
            paragraphStyle.alignment = .right
        }else{
            paragraphStyle.alignment = .left
            
        }
        
        
        let messageText = NSMutableAttributedString(
            string: NSLocalizedString("confirmdeletemsg", comment: ""),
            attributes: [
                NSAttributedString.Key.paragraphStyle: paragraphStyle,
                NSAttributedString.Key.font : UIFont.preferredFont(forTextStyle: .body),
                NSAttributedString.Key.foregroundColor : UIColor.black
            ]
        )
        
        alert.setValue(messageText, forKey: "attributedMessage")
        alert.addAction(UIAlertAction(title: NSLocalizedString("delete", comment: ""), style: .default, handler: { (action) -> Void in
           
            
            if Reachability.isConnectedToNetwork() {
                self.sv = UIViewController.displaySpinner(onView: self.view)
                let userId = UserDefaults.standard.integer(forKey: "HajUserID")
                
                let jsonUrlString = removeLandMarkEndpoint+String(userId)+"&landmark="+String(btn.tag)+"&lang=\(String(langForApi))"
                
                guard let url = URL(string: jsonUrlString) else { return }
                URLSession.shared.dataTask(with: url) { (data, response, err) in
                    UIViewController.removeSpinner(spinner: self.sv!)
                    if (err != nil){
                        self.view.presentToast(msg: NSLocalizedString("serverErr", comment: "") )
                        return
                    }
                    if let status = response as? HTTPURLResponse {
                        
                        if status.statusCode != 200{
                            self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                            return
                        }
                    }
                    guard let data = data else { return }
                    do {
                        print(data)
                        let response = try JSONDecoder().decode(removeLandMarkRes.self, from: data)
                        
                        
                        if response.Status == 1 {
                            
                            DispatchQueue.main.async {
                                self.view.presentToast(msg: response.Message)
                                self.loadVisits()
                               
                                
                            }
                            
                            
                        }
                        else{
                            DispatchQueue.main.async {
                                self.view.presentToast(msg: response.Message)
                            }
                            
                        }
                    }
                        
                    catch _ {
                        
                        self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                    }
                }.resume()
            }
            else{
                self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
            }
            
            
            
            
            
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .default, handler: { (action) -> Void in
            
            
        }))
        
        self.present(alert, animated: true)
        
        
        
    }
    
    
    func floatingButton(){
        
        let height = UIScreen.main.bounds.height
        fbtn.frame = CGRect(x: 30, y: height-195, width: 100, height: 100)
        fbtn.setBackgroundImage(UIImage(named: "floatingButton"), for: .normal)
        fbtn.clipsToBounds = true
        fbtn.layer.cornerRadius = 50
        fbtn.addTarget(self,action: #selector(newlandMark), for: .touchUpInside)
        landMarksTableView.addSubview(fbtn)
        
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let  off = self.landMarksTableView.contentOffset.y
        let height = UIScreen.main.bounds.height
        fbtn.frame = CGRect(x: 30, y:   off + (height-195), width: fbtn.frame.size.width, height: fbtn.frame.size.height)
        let width  = UIScreen.main.bounds.width
        fbtn2.frame = CGRect(x: width-130, y:   off + (height-195), width: fbtn2.frame.size.width, height: fbtn2.frame.size.height)
    }
    
    
    func floatingButtonen(){
      
        let width  = UIScreen.main.bounds.width
        let height = UIScreen.main.bounds.height
        fbtn2.frame = CGRect(x: width-130 , y:height-195 , width: 100, height: 100)
        fbtn2.setBackgroundImage(UIImage(named: "floatingButton"), for: .normal)
        fbtn2.clipsToBounds = true
        fbtn2.layer.cornerRadius = 50
        fbtn2.addTarget(self,action: #selector(newlandMark), for: .touchUpInside)
        landMarksTableView.addSubview(fbtn2)
    }
    
    
    @objc func newlandMark(){
        addOrEdit = "add"
        self.performSegue(withIdentifier: "newLandMarkseg", sender: nil)
    }
    
    @objc func loadVisits() {
        currentPage = 0 ;
        if Reachability.isConnectedToNetwork() {
            sv = UIViewController.displaySpinner(onView: self.view)
            let userId = UserDefaults.standard.integer(forKey: "HajUserID")
            let jsonUrlString = listLandMarksEndpoint+String(userId)+"&lang=\(String(langForApi))"+"&page=\(currentPage)"
            
            guard let url = URL(string: jsonUrlString) else { return }
            URLSession.shared.dataTask(with: url) { (data, response, err) in
                UIViewController.removeSpinner(spinner: self.sv!)
                if (err != nil){
                    self.view.presentToast(msg: NSLocalizedString("serverErr", comment: "") )
                    return
                }
                if let status = response as? HTTPURLResponse {
                    print("request status \(status.statusCode)")
                    if status.statusCode != 200{
                        self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                        return
                    }
                }
                guard let data = data else { return }
                do {
                    print(data)
                    let response = try JSONDecoder().decode(listuserLandMarks.self, from: data)
                   
                  
                    DispatchQueue.main.async {
                        self.refreshControl4.endRefreshing()
                        self.label.removeFromSuperview()
                        if self.currentPage  == 0 {
                            self.landMarksData = []
                        }
                      
                        self.landMarksTableView.reloadData()
                        
                    }
                    if response.Status == 1 {
                      
                        
                        self.tmparrlength = response.LandmarksList.count
                        
                        if self.currentPage > 0{
                            for itemx in response.LandmarksList {
                                self.landMarksData.append(itemx)
                            }
                            
                        }else{
                            self.landMarksData = response.LandmarksList
                        }
                        
                        if self.landMarksData.count == 0 {
                            DispatchQueue.main.async {
                                
                                self.label.center = CGPoint(x: 0, y: 300)
                                self.label.tag  = 1
                                
                                self.label.textAlignment = .center
                                
                                self.label.center.x = self.view.center.x
                               
                                self.label.font = UIFont(name: "Cairo SemiBold", size: 16)
                                self.label.textColor = #colorLiteral(red: 0.4476690292, green: 0.4476690292, blue: 0.4476690292, alpha: 1)
                                
                                
                                self.label.text =    NSLocalizedString("noLandMarks", comment: "")
                                
                                self.view.addSubview(self.label)
                                
                              
                            }
                        }
                        else{
                            DispatchQueue.main.async {
                                
                                self.tmparrlength = response.LandmarksList.count
                                
                                if self.currentPage > 0{
                                    for itemx in response.LandmarksList {
                                        self.landMarksData.append(itemx)
                                    }
                                    
                                }else{
                                    self.landMarksData = response.LandmarksList
                                }
                                
                               
                                self.landMarksTableView.reloadData()
                            }
                        }
                    }
                    else{
                        
                        DispatchQueue.main.async {
                                                        
                            self.tmparrlength = response.LandmarksList.count
                            
                            if self.currentPage > 0{
                                for itemx in response.LandmarksList {
                                    self.landMarksData.append(itemx)
                                }
                                
                            }else{
                                self.landMarksData = response.LandmarksList
                            }
                            
                            if self.landMarksData.count == 0 {
                                
                                
                                self.label.center = CGPoint(x: 0, y: 300)
                                
                                self.label.textAlignment = .center
                                
                                self.label.center.x = self.view.center.x
                               
                                self.label.tag = 1
                                
                                self.label.font = UIFont(name: "Cairo SemiBold", size: 16)
                                self.label.textColor = #colorLiteral(red: 0.4476690292, green: 0.4476690292, blue: 0.4476690292, alpha: 1)
                               
                                self.label.text =    NSLocalizedString("noLandMarks", comment: "")
                                
                                self.view.addSubview(self.label)
                                
                            }
                            
                            
                        }
                        
                       
                    }
                }
                    
                catch _ {
                   
                    DispatchQueue.main.async {
                        self.refreshControl4.endRefreshing()
                        self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                    }
                    
                }
            }.resume()
        }
        else{
            DispatchQueue.main.async {
                self.refreshControl4.endRefreshing()
                self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
            }
        }
        
        
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        if segue.identifier == "newLandMarkseg" {
            if let destinationVC = segue.destination as? addEditViewController {
                
                destinationVC.editLandmarkId = landmarkidForedit
                
                destinationVC.from = addOrEdit
                destinationVC.editObj = editData
                
            }
            
            
            
        }
        else if segue.identifier == "openlandMap" {
            if let destinationVC = segue.destination as? openMapViewController{
                
                destinationVC.userLat = selectedLat
                destinationVC.userLong = selectedLng
                
            }
            
            
            
        }
        
    }
    
    
    @objc func loadVisits2(){
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
       
        self.loadVisits()
    }
    
    override  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
       
        if indexPath.row == landMarksData.count - 1{
            
            if self.tmparrlength  == 10 {
                currentPage = currentPage + 1;
                self.loadVisits22()
            }
        }
    }
    
    @objc func loadVisits22() {
        
        if Reachability.isConnectedToNetwork() {
            sv = UIViewController.displaySpinner(onView: self.view)
            let userId = UserDefaults.standard.integer(forKey: "HajUserID")
            let jsonUrlString = listLandMarksEndpoint+String(userId)+"&lang=\(String(langForApi))"+"&page=\(currentPage)"
            
            guard let url = URL(string: jsonUrlString) else { return }
            URLSession.shared.dataTask(with: url) { (data, response, err) in
                UIViewController.removeSpinner(spinner: self.sv!)
                if (err != nil){
                    self.view.presentToast(msg: NSLocalizedString("serverErr", comment: "") )
                    return
                }
                if let status = response as? HTTPURLResponse {
                    print("request status \(status.statusCode)")
                    if status.statusCode != 200{
                        self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                        return
                    }
                }
                guard let data = data else { return }
                do {
                    print(data)
                    let response = try JSONDecoder().decode(listuserLandMarks.self, from: data)
                    
                   
                    DispatchQueue.main.async {
                        self.refreshControl4.endRefreshing()
                        self.label.removeFromSuperview()
                        if self.currentPage  == 0 {
                            self.landMarksData = []
                        }
                       
                        self.landMarksTableView.reloadData()
                        
                    }
                    if response.Status == 1 {
                      
                        self.tmparrlength = response.LandmarksList.count
                        
                        if self.currentPage > 0{
                            for itemx in response.LandmarksList {
                                self.landMarksData.append(itemx)
                            }
                            
                        }else{
                            self.landMarksData = response.LandmarksList
                        }
                        
                        if self.landMarksData.count == 0 {
                            DispatchQueue.main.async {
                               
                                self.label.center = CGPoint(x: 0, y: 300)
                                self.label.tag  = 1
                                self.label.textAlignment = .center
                                self.label.center.x = self.view.center.x
                                
                                self.label.font = UIFont(name: "Cairo SemiBold", size: 16)
                                self.label.textColor = #colorLiteral(red: 0.4476690292, green: 0.4476690292, blue: 0.4476690292, alpha: 1)
                                self.label.text =    NSLocalizedString("noLandMarks", comment: "")
                                self.view.addSubview(self.label)
                                
                                
                                
                            }
                        }
                        else{
                            DispatchQueue.main.async {
                                self.landMarksTableView.reloadData()
                            }
                        }
                    }
                    else{
                       
                        DispatchQueue.main.async {
                            
                            
                            self.tmparrlength = response.LandmarksList.count
                            
                            if self.currentPage > 0{
                                for itemx in response.LandmarksList {
                                    self.landMarksData.append(itemx)
                                }
                                
                            }else{
                                self.landMarksData = response.LandmarksList
                            }
                            
                            if self.landMarksData.count == 0 {
                                self.label.center = CGPoint(x: 0, y: 300)
                                self.label.textAlignment = .center
                                self.label.center.x = self.view.center.x
                                self.label.tag = 1
                                self.label.font = UIFont(name: "Cairo SemiBold", size: 16)
                                self.label.textColor = #colorLiteral(red: 0.4476690292, green: 0.4476690292, blue: 0.4476690292, alpha: 1)
                                self.label.text =    NSLocalizedString("noLandMarks", comment: "")
                                self.view.addSubview(self.label)
                                
                                
                            }
                            
                        }
                        
                      
                    }
                }
                    
                catch _ {
                   
                    DispatchQueue.main.async {
                        self.refreshControl4.endRefreshing()
                        self.view.presentToast(msg: NSLocalizedString("serverErr", comment: ""))
                    }
                    
                }
            }.resume()
        }
        else{
            DispatchQueue.main.async {
                self.refreshControl4.endRefreshing()
                self.view.presentToast(msg: NSLocalizedString("internetConnectionErr", comment: ""))
            }
        }
        
        
        
    }
    
    @objc func drawNotificationBell(){
        let notificationButton = SSBadgeButton()
        notificationButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        notificationButton.setImage(UIImage(named: "notificationsButton-1")?.withRenderingMode(.alwaysTemplate), for: .normal)
        notificationButton.tintColor = .white
        
        if UserDefaults.standard.integer(forKey: "notificationCount") > 0 {
            
            notificationButton.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
            notificationButton.badge = String(UserDefaults.standard.integer(forKey: "notificationCount"))
        }
        
        notificationButton.addTarget(self, action: #selector(openNotification(sender:)), for: .touchUpInside)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: notificationButton)
    }
    
    
}
